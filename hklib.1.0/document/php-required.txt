- Version 5.6.11 or higher

For PHP-MYSQL Support
- Support MySQL Native Driver (mysqlnd)
    . On Ubuntu run:
        $ sudo apt-get update
        $ sudo apt-get install php5-mysqlnd

Check for mbstring enable
    ## in php.ini
    extension=php_mbstring.dll

    [mbstring]
    mbstring.language = all
    mbstring.internal_encoding = UTF-8
    mbstring.http_input = auto
    mbstring.http_output = UTF-8
    mbstring.encoding_translation = On
    mbstring.detect_order = UTF-8
    mbstring.substitute_character = none
    mbstring.func_overload = 0
    mbstring.strict_encoding = Off

    ## Install php mbstring support
    sudo apt-get install php7.0-mbstring

Ensure php allow file upload
    file_uploads = On