<?php
    return
    [
        'default-logger' => [
            'source'=>      '',
            'output'=>      '@system.root/logs/default.log',
            'pattern'=>     '%date (%from) %level > %message',
            'date_format'=> 'd/m/y H:i:s',
            'max'=>         1000,
            'backup'=>      1,
            'level'=>       'debug',
            'simplify'=>    true
        ],

        'hk-system-logger' => [
            'source'=>      '@system.root/src/',
            'output'=>      '@system.root/logs/hk.log',
            'pattern'=>     '%date (%from) %level > %message',
            'date_format'=> 'd/m/y H:i:s',
            'max'=>         500,
            'backup'=>      1,
            'level'=>       'debug',
            'simplify'=>    true
        ],

        'hk-db-connection' => [
            'source'=>      '@system.root/src/HK/DB',
            'output'=>      '@system.root/logs/db.log',
            'pattern'=>     '%date (%from) %level > %message',
            'date_format'=> 'd/m/y H:i:s',
            'max'=>         500,
            'backup'=>      2,
            'level'=>       'debug',
            'simplify'=> true
        ]
    ];
?>