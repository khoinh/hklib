<?php
    namespace HK\DB;

    abstract class Connection {
        abstract public function connect($server, $user, $password, $database, $port=null, $socket=null);
        abstract public function isReady();
        abstract public function close();
        abstract public function getType();
        abstract public function setDatabase($name);
        abstract public function query($query);
        abstract public function escapeString($str);
        abstract public function getDateFormat();
        abstract public function getDateTimeFormat();
        abstract public function getLastInsertId();
    }