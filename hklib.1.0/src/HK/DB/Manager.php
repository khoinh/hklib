<?php
    namespace HK\DB;

    use HK\Config as Config;

    class Manager {
        private static $connections = [];
        private static $config = null;

        public static function init($config = []) {
            Manager::$config = $config;
            Config::insertConfigValue(Manager::$config);
        }

        private static function createConnection($name) {
            if (!Manager::$connections) {
                Manager::$connections = [];
            }

            // Check if connection name exist in config list
            if (array_key_exists($name, Manager::$config)) {
                // Create new connection
                $config = Manager::$config[$name];
                return Manager::initConnection( $config["type"],
                                                $config["server"],
                                                $config["user"],
                                                $config["password"],
                                                $config["database"],
                                                $config["port"],
                                                $config["socket"]);
            }

            // If not exist, return null object
            return null;
        }

        private static function initConnection($type, $server, $user, $password, $database, $port, $socket) {
            $connection = null;

            // Check Database type and create related connection
            switch ($type) {
                case "MySQL":
                    // If this is an mysql connection
                    \HK\import("hk:DB/MySQL");
                    $connection = new MySQL\Connection();
                    $connection->connect($server, $user, $password, $database, $port, $socket);
                    break;
            }

            return $connection;
        }

        //--------------------------------------------------------------------------------------------------------------
        public static function connection($name) {
            if (!Manager::$connections || !array_key_exists($name, Manager::$connections))
                Manager::$connections[$name] = Manager::createConnection($name);

            return Manager::$connections[$name];
        }

        public static function close($name) {
            if (connection($name)) {
                connection($name)->close();
            }
        }

        public static function closeAll() {
            foreach (Manager::$connections as $connection) {
                $connection->close();
            }
        }
    }

    Manager::init(\HK\Context::databaseConfig());