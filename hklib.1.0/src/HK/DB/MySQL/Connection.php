<?php
    namespace HK\DB\MySQL;

    use HK\DB\SQLResult as SQL;

    class Connection extends \HK\DB\Connection {
        private $logger;
        private $server;
        private $user;
        private $password;
        private $database;
        private $port;
        private $socket;

        function __construct() {
            $this->connection = null;
            $this->logger = new \HK\Debug\Logger(__FILE__);
        }

        function __destruct() {
            $this->close();
        }

        public function connect($server, $user, $password, $database=null, $port=null, $socket=null) {
            $this->connection = \mysqli_connect($server, $user, $password, $database, $port, $socket);
            $this->server = $server;
            $this->user = $user;
            $this->password = $password;
            $this->database = $database;
            $this->port = $port;
            $this->socket = $socket;

            // Check connection
            if (mysqli_connect_errno() || !$this->connection) {
                $this->logger->error("[%server] Failed to connect to %type server: %error", array(
                        'server' =>     $server,
                        'type' =>       $this->getType(),
                        'error' =>      mysqli_connect_error()
                    ));
            }
        }

        public function isReady() {
            return $this->connection != null;
        }

        public function close() {
            if ($this->connection) {
                mysqli_close($this->connection);
                $this->connection = null;
            }
        }

        public function getType() {
            return "MySQL";
        }

        public function setDatabase($name) {
            if ($this->isReady()) {
                if (mysqli_select_db($this->connection, $name)) {
                    $this->database = $name;
                    return true;
                }
            }

            return false;
        }

        public function query($query) {
            $this->logger->info("[%server - %type] Querying: %query", array(
                    'server' =>     $this->server,
                    'type' =>       $this->getType(),
                    'query' =>      $query
                ));

            $result = null;

            if ($this->isReady()) {
                $result = mysqli_query($this->connection, $query);
                if (!$result) {
                    $this->logger->error("[%server - %type] Query error: %error", array(
                            'server' =>     $this->server,
                            'type' =>       $this->getType(),
                            'error' =>      mysqli_error($this->connection)
                        ));

                    return new SQLResult(SQL::$TYPE_FAILURE);
                } else if ($result === true) {
                    return new SQLResult(SQL::$TYPE_SUCCESS);
                } else {
                    return new SQLResult(SQL::$TYPE_VALUE, $result);
                }
            }

            return new SQLResult(SQL::$TYPE_FAILURE);
        }

        public function escapeString($str) {
            if ($this->isReady()) {
                return mysqli_real_escape_string($this->connection, $str);
            }

            \HK\Error::send("ER0200", array('name' => $this->getType()));
        }

        public function getDateFormat() {
            return 'Y-m-d';
        }

        public function getDateTimeFormat() {
            return 'Y-m-d H:i:s';
        }

        public function getLastInsertId() {
            return mysqli_insert_id($this->connection);
        }
    }