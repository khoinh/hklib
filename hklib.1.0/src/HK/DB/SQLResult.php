<?php
    namespace HK\DB;

    abstract class SQLResult {

        public static $TYPE_VALUE;
        public static $TYPE_SUCCESS;
        public static $TYPE_FAILURE;

        public static function init() {
            SQLResult::$TYPE_VALUE = 0;
            SQLResult::$TYPE_SUCCESS = 1;
            SQLResult::$TYPE_FAILURE = 2;
        }

        //--------------------------------------------------------------------------------------------------------------
        protected $type;

        public function getType() {
            return $this->type;
        }

        //--------------------------------------------------------------------------------------------------------------
        abstract public function getAll();
        abstract public function getRow($index);
        abstract public function numField();
        abstract public function numRow();
    }

    SQLResult::init();