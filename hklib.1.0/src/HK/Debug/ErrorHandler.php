<?php
    namespace HK\Debug;

    use HK\Config as Config;
    use HK\Util as Util;

    Class ErrorAndExceptionHandler {

        private $errorHandler;
        private $exceptionHandler;

        public function __construct() {
            error_reporting(E_ALL);
            $mode = Config::get("debug.error-handler.mode");

            if (!$mode || $mode == "hide")
                return;

            $this->errorHandler = new ErrorHandler($mode);
            $this->exceptionHandler = new ExceptionHandler($mode);

            set_error_handler(array($this->errorHandler, 'execute'), E_ALL);
            set_exception_handler(array($this->exceptionHandler, 'execute'));
        }

        public function display() {
            $this->isEnable = true;
        }

        public function log() {
            $this->isEnable = false;
        }
    }

    class ErrorHandler {

        private $mode;

        public function __construct($mode) {
            $this->mode = $mode;
        }

        public function execute($errorNo,
                                $errorMessage,
                                $errorFile,
                                $errorLine,
                                $errorContext) {
            switch ($errorNo) {
                case E_COMPILE_ERROR:
                case E_CORE_ERROR:
                case E_USER_ERROR:
                case E_ERROR:
                case E_PARSE:

                case E_USER_NOTICE:
                case E_NOTICE:
                    $this->handleError( $errorNo,
                                        $errorMessage,
                                        $errorFile,
                                        $errorLine,
                                        $errorContext);
                    exit();

                case E_COMPILE_WARNING:
                case E_USER_WARNING:
                case E_CORE_WARNING:
                case E_WARNING:

                case E_USER_DEPRECATED:
                case E_DEPRECATED:
                case E_STRICT:

                case E_RECOVERABLE_ERROR:
                    $this->handleWarning(   $errorNo,
                                            $errorMessage,
                                            $errorFile,
                                            $errorLine,
                                            $errorContext);
                    break;
            }
        }

        private function handleError(   $errorNo,
                                        $errorMessage,
                                        $errorFile,
                                        $errorLine,
                                        $errorContext) {
            // If system error mode is log. Use logger to
            // store error.
            if ($this->mode == "log") {
                $logger = new Logger($errorFile);
                $logger->error("code %code - line %line - %message",
                        array(
                            "code" => $errorNo,
                            "line" => $errorLine,
                            "message" => $errorMessage
                        ));
                return;
            }

            // Clean page content and display error
            ob_end_clean();
            $message =  Util::buildString("code %code %n %file %n line %line %n %message",
                        array(
                            "code" => $errorNo,
                            "line" => $errorLine,
                            "message" => $errorMessage,
                            "file" => $errorFile,
                            "n" => '</br>'
                        ));

            $context = array(
                    "header" =>     "AN ERROR HAS OCCUR",
                    "message" =>    $message,
                    "detail" =>     json_encode(debug_backtrace(), JSON_PRETTY_PRINT)
                );

            \HK\Template\present(new \HK\Template\Component("hk:ErrorScreen", $context));
        }

        private function handleWarning( $errorNo,
                                        $errorMessage,
                                        $errorFile,
                                        $errorLine,
                                        $errorContext) {
            $logger = new Logger($errorFile);
            $logger->warning("code %code - line %line - %message",
                    array(
                        "code" => $errorNo,
                        "line" => $errorLine,
                        "message" => $errorMessage
                    ));
        }
    }

    class ExceptionHandler {

        private $mode;

        public function __construct($mode) {
            $this->mode = $mode;
        }

        public function execute($exception) {
            $trace = $exception->getTrace();
            $message = $exception->getMessage();
            $code = $exception->getCode();
            $file = $exception->getFile();
            $line = $exception->getLine();

            // If system error mode is log. Use logger to
            // store error.
            if ($this->mode == "log") {
                $logger = new Logger($file);
                $logger->error("Exception code %code - line %line - %message",
                        array(
                            "code" => $code,
                            "line" => $line,
                            "message" => $message
                        ));
                return;
            }

            // Clean page content and display error
            ob_end_clean();
            $message =  Util::buildString("code %code %n %file %n line %line %n %message",
                        array(
                            "code" => $code,
                            "line" => $line,
                            "message" => $message,
                            "file" => $file,
                            "n" => '</br>'
                        ));

            $context = array(
                    "header" =>     "AN EXCEPTION HAS OCCUR",
                    "message" =>    $message,
                    "detail" =>     json_encode(debug_backtrace(), JSON_PRETTY_PRINT)
                );

            \HK\Template\present(new \HK\Template\Component("hk:ErrorScreen", $context));
        }
    }