<?php
    namespace HK\Debug;

    use HK\Config as Config;
    use HK\Context as Context;
    use HK\Util as Util;

    class Logger {

        //--------------------------------------------------------------------------
        // STATIC
        private static $isEnable;
        private static $configs;

        public static function init($configs = []) {
            Logger::$configs = $configs;
            Config::insertConfigValue(Logger::$configs);

            Logger::$isEnable = false;
            if (Config::get("debug.log.enable") == "true") {
                Logger::$isEnable = true;
            }
        }

        private static function chooseConfig($location) {
            $result = null;
            $lastRemain = "";
            $root = Context::env("document_root");

            foreach (Logger::$configs as $name => $config) {
                $splitResult = explode(
                    realpath($root.$config['source']),
                    realpath($location), 2);

                if (count($splitResult) < 2)
                    continue;

                $check = $splitResult[0];
                $remain = $splitResult[1];

                if ($check != "") {
                    continue;
                }

                if (!$result || strlen($lastRemain) > $remain) {
                    $result = $config;
                    $result['name'] = $name;
                    $lastRemain = $remain;
                }
            }

            return array($result, $lastRemain);
        }

        //--------------------------------------------------------------------------
        // MEMBER
        private $config;
        private $location;
        private $shortLocation;
        private $patternTokens;

        function __construct($location = "") {
            $this->location = $location;
            list($this->config, $this->shortLocation) = Logger::chooseConfig($location);
            $this->preparePattern();
        }

        private function preparePattern() {
            $this->patternTokens = [];
            $this->patternTokens['from'] = $this->shortLocation;
        }

        private function getOutputFile($level) {
            if ($this->isEnable($level)) {
                // Attemping to create new output file
                $root = Context::env("document_root");
                $outputFileUrl = $root.$this->config['output'];
                $outputFile = fopen($outputFileUrl, 'c+')
                    or die("Internal error code 001 [".$this->config['name']."]");

                // When successfully get output file
                if ($outputFile) {
                    // Check if this output file has suppass maximum number of log lines
                    // 1. Count number of log in output file
                    $this->numLog = 0;
                    while (!feof($outputFile)) {
                        fgets($outputFile);
                        $this->numLog++;
                    }

                    // 2. Check against maximum number of log lines
                    if ($this->numLog >= $this->config['max']) {
                        fclose($outputFile);

                        // Create and save backup
                        $this->createBackup();

                        // Return empty output file
                        return fopen($outputFileUrl, 'w+')
                            or die("Internal error code 001 [".$this->config['name']."]");
                    }

                    // Return output file
                    return $outputFile;
                }
            }

            // Return null if this log is not enabled
            return null;
        }

        private function createBackup() {
            $numBackup = $this->config['backup'];
            $root = Context::env("document_root");
            $outputFileUrl = $root.$this->config['output'];

            // Check for number of backup is larger than 0.
            // If not then we simply cleanup current output file content.
            if ($numBackup > 0) {
                // Check for current backup file existance
                $index = 1;
                while ($index <= $numBackup) {
                    if (!file_exists("$outputFileUrl.bk.$index")) {
                        break;
                    }
                    $index++;
                }

                // If index exceed max number of backup file. This mean that we should remove
                // the oldest backup and create new one.
                if ($index > $numBackup) {
                    // 1. Remove oldest backup file
                    unlink("$outputFileUrl.bk.1");

                    // 2. Rename all old backup file to index one
                    for ($index = 2; $index <= $numBackup; $index++) {
                        $prev = $index - 1;
                        rename("$outputFileUrl.bk.$index", "$outputFileUrl.bk.$prev");
                    }

                    // 3. Reset current index to the last backup index
                    $index = $numBackup;
                }

                // Now rename current output file to backup file
                rename("$outputFileUrl", "$outputFileUrl.bk.$index");
            }
        }

        private function prepareMessage($level, $message, $tokens) {
            // Combine message with tokens
            $message = Util::buildString($message, $tokens);

            // Build log pattern
            $pattern = $this->config['pattern'];
            $patternTokens = [];

            // 1. Update message
            $this->patternTokens['message'] = $message;

            // 2. Update level
            switch ($level) {
                case 2:
                    $this->patternTokens['level'] = "ERROR";
                    break;

                case 1:
                    $this->patternTokens['level'] = "WARNING";
                    break;

                case 0:
                    $this->patternTokens['level'] = "INFO";
                    break;
            }

            // 3. Update current time
            if (array_key_exists('date_format', $this->config)) {
                $dateFormat = $this->config['date_format'];
                $this->patternTokens['date'] = date($dateFormat);
            }

            // 4. Build and return log message
            $message = Util::buildString($pattern, $this->patternTokens);

            // Strip and simplify all white space in message to only one white space
            if (!array_key_exists("strip.simplify", $this->config) ||
                $this->config["strip.simplify"])
                return trim(preg_replace('/\s\s+/', ' ', $message));

            return $message;
        }

        private function outputMessage($level, $message, $tokens) {
            $outputFile = $this->getOutputFile($level);
            if ($outputFile) {
                $message = $this->prepareMessage($level, $message, $tokens);
                $logNumber = $this->numLog++;
                fwrite($outputFile, "$logNumber. $message\n");
                fclose($outputFile);
            }
        }

        private function isEnable($level) {
            if ($this->config && Logger::$isEnable) {
                switch ($this->config['level']) {
                    case 'debug':
                        return true;

                    case 'release':
                        return $level > 0;

                    case 'stable':
                        return $level > 1;
                }
            }

            return false;
        }

        //--------------------------------------------------------------------------
        public function error($message, $tokens = []) {
            $this->outputMessage(2, $message, $tokens);
        }

        public function warning($message, $tokens = []) {
            $this->outputMessage(1, $message, $tokens);
        }

        public function info($message, $tokens = []) {
            $this->outputMessage(0, $message, $tokens);
        }
    }
