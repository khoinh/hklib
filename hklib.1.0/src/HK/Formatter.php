<?php
    namespace HK;

    class Formatter {

        public function format($input, $type, $options = []) {
            switch (strtolower($type)) {
                case 'date':
                    return self::formatToDate($input, $options);

                case 'datetime':
                    return self::formatToDateTime($input, $options);

                case 'string':
                    return self::formatToString($input, $options);

                default:
                    // No change
                    return $input;
            }
        }

        public function jsonEncode($input, $options, $depth) {
            return json_encode($input, $options, $depth);
        }

        public function jsonDecode($input, $assoc, $depth, $options) {
            return json_decode($input, $assoc, $depth, $options);
        }

        private function formatToDate($input, $options, $errorFormatType = 'date') {
            if (empty($input)) {
                return null;
            }

            if (gettype($input) == 'string') {
                // Convert string to date
                $format = Config::get("default.format.date");
                if (isset($options['format'])) {
                    $format = $options['format'];
                }

                return \DateTime::createFromFormat($format, $input);
            } else if (gettype($input) == 'integer') {
                // Convert integer to date (In milisecond)
                return \DateTime::createFromFormat('u', $input);
            } else if ($input instanceof \DateTime) {
                return $input;
            }

            var_dump($input);
            exit;
            (new \HK\Error('ER0600', [
                    'formatType' => $errorFormatType,
                    'inputType' =>  gettype($input)
                ]))->send();
        }

        private function formatToDateTime($input, $options) {
            $options['format'] = Config::get("default.format.datetime");
            return self::formatToDate($input, $options, 'datetime');
        }

        private function formatToString($input, $options) {
            if ($input instanceof \DateTime) {
                // Convert Date object to string
                $format = Config::get("default.format.datetime");
                if (isset($options['format'])) {
                    $format = $options['format'];
                }

                return $input->format($format);
            } else if (gettype($input) == 'object' || gettype($input) == 'array') {
                // Print object and array content
                return print_r($input, true);
            } else if (gettype($input) == 'boolean') {
                return $input ? 'true' : 'false';
            } else if (gettype($input) == 'double') {
                // Convert and format float number to string
                $decimals = 0;
                $decPoint = '.';
                $thousandsSep = ',';

                if (isset($options['decimals'])) {
                    $decimals = $options['decimals'];
                }

                if (isset($options['decimal-point'])) {
                    $decPoint = $options['decimal-point'];
                }

                if (isset($options['thousands-seperator'])) {
                    $thousandsSep = $options['thousands-seperator'];
                }

                return number_format($input, $decimals, $decPoint, $thousandsSep);
            } else {
                return "".$input;
            }
        }
    }