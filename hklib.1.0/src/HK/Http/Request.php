<?php
    namespace HK\Http;

    class Request {
        //--------------------------------------------------------------------------------------------------------------
        //.Static
        static public function clean() {
            unset($_GET);
            unset($_POST);
            $_GET = [];
            $_POST = [];
        }

        static public function method() {
            return $_SERVER['REQUEST_METHOD'];
        }

        static public function get($name, $default = null) {
            if (!array_key_exists($name, $_GET)) {
                return $default;
            }

            return $_GET[$name];
        }

        static public function post($name, $default = null) {
            if (!array_key_exists($name, $_POST)) {
                return $default;
            }

            return $_POST[$name];
        }

        static public function getOrPost($name, $default = null) {
            if (array_key_exists($name, $_GET)) {
                return $_GET[$name];
            } else {
                return self::post($name, $default);
            }
        }

        static public function hasGet($name) {
            return array_key_exists($name, $_GET);
        }

        static public function hasPost($name) {
            return array_key_exists($name, $_POST);
        }

        static public function has($name) {
            return array_key_exists($name, $_GET) || array_key_exists($name, $_POST);
        }

        static function file($name) {
            if (array_key_exists($name, $_FILES))
                return $_FILES[$name];

            return null;
        }
    }