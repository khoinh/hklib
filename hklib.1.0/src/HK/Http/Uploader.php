<?php
    namespace HK\Http;

    class Uploader {

        static private $uploadDir;
        static private $maxUploadSize;
        static private $specialize;

        static public function init() {
            self::$uploadDir = \HK\Config::get('media.upload.dir');
            self::$maxUploadSize = \HK\Config::get('media.upload.maxsize');
            self::$specialize = \HK\Config::get('media.specialize');
        }

        static public function getUploadDir() {
            return self::$uploadDir;
        }

        static public function setUploadDir($dir) {
            self::$uploadDir = $dir;
        }

        static public function getMaxUploadSize() {
            return self::$maxUploadSize;
        }

        static public function setMaxUploadSize($size) {
            self::$maxUploadSize = $size;
        }

        static public function upload($name, $useUID = false) {
            if (Request::method() != 'POST') {
                return new \HK\Error('ER0010');
            }

            $request = Request::create();
            $file = $request->file($name);

            if ($file) {
                $fileName = $file['name'];
                $fileSize = $file['size'] / 1024 / 1024; // In MB
                $fileTmp = $file['tmp_name'];
                $fileType = $file['type'];

                $ext = explode('.', $fileName);
                $ext = end($ext);
                $fileExt = strtolower($ext);

                $supportedExt = \HK\Config::get('system.supported.extension');
                if (!in_array($fileExt, $supportedExt)) {
                    return new \HK\Error('ER0011', ['ext' => implode(', ', $supportedExt)]);
                }

                if ($fileSize > self::getMaxUploadSize()) {
                    return new \HK\Error('ER0012', ['size' => self::getMaxUploadSize()]);
                }

                $subDir = '';
                foreach (self::$specialize as $sub => $extList) {
                    if (in_array($fileExt, $extList)) {
                        $subDir = $sub;
                        break;
                    }
                }

                $root = \HK\Context::env("document_root");
                $uploadDir = $root.self::getUploadDir().$subDir;

                $uuid = uniqid('', true);
                $fileUploadFileName = "$uploadDir/$fileName";
                if ($useUID) {
                    $fileUploadFileName = "$uploadDir/$uuid";
                }

                if (!move_uploaded_file($fileTmp, $fileUploadFileName)) {
                    return new \HK\Error('ER0013');
                }

                return [$fileName, $subDir, $uuid];
            }

            return new \HK\Error('ER0009', ['name' => $name]);
        }
    }

    Uploader::init();