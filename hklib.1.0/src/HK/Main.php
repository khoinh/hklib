<?php
    namespace HK;

    function import(...$sources) {
        foreach ($sources as $source) {
            // Check and exit if we already import package
            if (Config::isImported($source))
                continue;

            // Get package name and url
            list($container, $package) = explode(":", $source);

            // Load container url from repository src
            $container = Config::get("repositories.src.$container");
            if ($container) {
                // Load Core.php file inside pacakge
                $root = Context::env("document_root");
                if (file_exists("$root$container$package/Core.php")) {
                    require_once "$root$container$package/Core.php";
                } else {
                    (new Error("ER0004", array(
                        'container' =>  $container,
                        'package' =>    $package
                    )))->send();
                }
            } else {
                (new Error("ER0003", array('container' =>  $container)))->send();
            }

            Config::imported($source);
        }
    }

    if (Config::get("shortcut.import"))
        require __DIR__."/GlobalShortcut.php";