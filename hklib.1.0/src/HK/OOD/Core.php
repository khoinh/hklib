<?php
    namespace HK\OOD;

    \HK\import( "hk:DB",
                "hk:OOD/Entities",
                "hk:OOD/Fields");

    require "ConnectionHandler.php";
    require "Manager.php";
    require "Unknown.php";
    require "Result.php";