<?php
    namespace HK\OOD\Entities;

    use HK\DB\SQLResult as SQLResult;

    use HK\OOD\Unknown as Unknown;
    use HK\OOD\Fields as Fields;
    use HK\OOD\Result as Result;

    use HK\Util as Util;


    abstract class BaseEntity {

        protected static $handlerMap;

        public static function assignHandler($handler, $classes) {
            if (!BaseEntity::$handlerMap) {
                BaseEntity::$handlerMap = [];
            }

            BaseEntity::$handlerMap[] = array(
                    'handler' => $handler,
                    'classes' => $classes
                );
        }

        public static function getHandler($entity) {
            return BaseEntity::findHandler($entity);
        }

        private static function findHandler($entity) {
            if (!BaseEntity::$handlerMap) {
                BaseEntity::$handlerMap = [];
            }

            foreach (BaseEntity::$handlerMap as $map) {
                $handler = $map['handler'];
                $classes = $map['classes'];

                if (in_array($entity, $classes)) {
                    return $handler;
                }
            }

            return null;
        }

        //--------------------------------------------------------------------------------------------------------------
        // Public Method
        protected $fields = [];
        protected $options = [];

        public function options($options = []) {
            $this->options = $options;
        }

        public function setOption($name, $value) {
            $this->options[$name] = $value;
        }

        public function getOption($name) {
            if (array_key_exists($name, $this->options))
                return $this->options[$name];

            return null;
        }

        public function getPrimaryKeys() {
            $result = [];
            foreach ($this->fields as $name => $field) {
                if ($field->isPrimaryKey())
                    $result[$name] = $field;
            }

            return $result;
        }

        public function __set($name, $value) {
            // If input is a field, Check if field already exist.
            // If not, add field to field list
            if ($value instanceof Fields\BaseField) {
                if (array_key_exists($name, $this->fields)) {
                    (new \HK\Error("ER0111", array(
                        'entity' => get_class($this),
                        'field' => $name
                    )))->send();
                }

                // Save to field list
                $this->fields[$name] = $value;
                $this->fields[$name]->setEntity($this);

                // Use the following method to set default column name to field
                $this->getColumnName($name);
            }

            // If input isn't a field, and entity does not have field name in field list
            // Output invalid variable error message.
            else if (!array_key_exists($name, $this->fields)) {
                (new \HK\Error("ER0112", array(
                    'entity' => get_class($this),
                    'field' => $name
                )))->send();
            }

            // If input isn't a field, and field name exist in field list
            // Set input as value to that field
            else {
                $this->fields[$name]->set($value);
            }
        }

        public function __get($name) {
            // Get field value if exist
            if (array_key_exists($name, $this->fields)) {
                return $this->fields[$name]->get();
            }

            // Output non existing field error if field not exist in field list
            (new \HK\Error("ER0113", array(
                'entity' => get_class($this),
                'field' => $name
            )))->send();
        }

        public function __isset($name) {
            // Check if field with name exist
            return isset($this->fields[$name]);
        }

        public function copy() {
            $entity = get_class($this);
            $result = new $entity();
            $result->cloneFrom($this);

            return $result;
        }

        public function cloneFrom($other) {
            $this->options($other->options);
            $this->fields = [];

            foreach ($other->fields as $name => $field) {
                $this->fields[$name] = $field->copy();
                $this->fields[$name]->setEntity($this);
            }
        }

        public function hasField($name) {
            return array_key_exists($name, $this->fields);
        }

        public function getField($name) {
            if (array_key_exists($name, $this->fields))
                return $this->fields[$name];

            return new \HK\Error("ER0110", array(
                'entity' => get_class($this),
                'field' => $name
            ));
        }

        public function getFieldList() {
            return $this->fields;
        }

        //--------------------------------------------------------------------------------------------------------------
        // Per-Entity SQL Method
        public function reload() {
            foreach ($this->fields as $name => $field) {
                if ($field->isPrimaryKey())
                    continue;

                $field->set(new Unknown);
            }

            $result = $this->select();
            if (count($result) <= 0) {
                return Result::asFailure(Result::$ERROR_NO_ENTITY_FOUND);
            }

            $this->cloneFrom($result[0]);

            return Result::asSuccess($this);
        }

        public function save() {
            if (!$this->handler())
                (new \HK\Error('ER0100'))->send();

            // Validate known fields value, required primary keys to be valid
            $primaryKeys = $this->getPrimaryKeys();
            if (count($primaryKeys) <= 0)
                return Result::asFailure(Result::$ERROR_SAVE_OR_REMOVE_WITH_NO_PRIMARY_KEYS,
                    array('entity' => get_class($this)));

            // Use primary key to query result.
            // All primary key need to be valid (Not unknown or any thing weird).
            $conditions = [];
            foreach ($primaryKeys as $name => $field) {
                $sqlQuery = $field->toSqlQuery($this->handler());
                if ($sqlQuery instanceof \HK\Error)
                    return Result::asFailure(Result::$ERROR_INVALID_FIELD_QUERY, array(
                        'entity' =>     get_class($this),
                        'field' =>      $name,
                        'helper' =>     $sqlQuery->getMsg()
                    ));

                $conditions[] = $sqlQuery;
            }

            // Combine condition into one
            $conditions = "WHERE ".implode(" AND ", $conditions);

            $tableName = $this->getTableName();
            list($knownFields, $unknownFields) = $this->classifyField();

            $assignments = [];
            foreach ($knownFields as $name => $field) {
                // Skip primary key as they will be used as condition.
                if ($field->isPrimaryKey())
                    continue;

                $sqlValue = $field->toSqlValue($this->handler(), true);
                if ($sqlValue instanceof \HK\Error)
                return Result::asFailure(Result::$ERROR_INVALID_FIELD_VALUE, array(
                    'entity' =>     get_class($this),
                    'field' =>      $name,
                    'helper' =>     $sqlValue->getMsg()
                ));

                $assignments[] = $sqlValue;
            }

            // Combine assignment into one
            if (count($assignments) <= 0)
                return Result::asFailure(Result::$ERROR_SAVE_WITH_NO_VALUE, array(
                    'entity' => get_class($this)
                ));
            $assignments = "SET ".implode(", ", $assignments);

            // Build sql query
            $sql = "UPDATE %tableName %assignments %conditions;";
            $sql = Util::buildString($sql, array(
                    'tableName' =>      $tableName,
                    'assignments' =>    $assignments,
                    'conditions' =>     $conditions
                ));

            // Use Connection handler to execute query
            $result = $this->handler()->query($sql);
            if ($result->getType() == SQLResult::$TYPE_FAILURE) {
                return Result::asFailure(Result::$ERROR_SQL_FAILURE);
            }

            return Result::asSuccess(true);
        }

        public function remove() {
            if (!$this->handler())
                (new \HK\Error('ER0100'))->send();

            // Validate known fields value, required primary keys to be valid
            $primaryKeys = $this->getPrimaryKeys();
            if (count($primaryKeys) <= 0)
                return Result::asFailure(Result::$ERROR_SAVE_OR_REMOVE_WITH_NO_PRIMARY_KEYS,
                    array('entity' => get_class($this)));

            // Use primary key to query result.
            // All primary key need to be valid (Not unknown or any thing weird).
            $conditions = [];
            foreach ($primaryKeys as $name => $field) {
                $sqlQuery = $field->toSqlQuery($this->handler());
                if ($sqlQuery instanceof \HK\Error)
                    return Result::asFailure(Result::$ERROR_INVALID_FIELD_QUERY, array(
                        'entity' =>     get_class($this),
                        'field' =>      $name,
                        'helper' =>     $sqlQuery->getMsg()
                    ));

                $conditions[] = $sqlQuery;
            }

            // Combine condition into one
            $conditions = "WHERE ".implode(" AND ", $conditions);

            $tableName = $this->getTableName();

            $sql = "DELETE FROM %tableName %conditions;";
            $sql = Util::buildString($sql, array(
                    'tableName' =>      $tableName,
                    'conditions' =>     $conditions
                ));

            // Use Connection handler to execute query
            $result = $this->handler()->query($sql);
            if ($result->getType() == SQLResult::$TYPE_FAILURE) {
                return Result::asFailure(Result::$ERROR_SQL_FAILURE);
            }

            return Result::asSuccess(true);
        }

        //--------------------------------------------------------------------------------------------------------------
        // Per-Table SQL Method
        public function select($options = []) {
            if (!$this->handler())
                (new \HK\Error('ER0100'))->send();

            $tableName = $this->getTableName();
            list($knownFields, $unknownFields) = $this->classifyField();

            $conditions = [];
            foreach ($knownFields as $name => $field) {
                $sqlQuery = $field->toSqlQuery($this->handler());
                if ($sqlQuery instanceof \HK\Error)
                    return Result::asFailure(Result::$ERROR_INVALID_FIELD_QUERY, array(
                        'entity' =>     get_class($this),
                        'field' =>      $name,
                        'helper' =>     $sqlQuery->getMsg()
                    ));

                $conditions[] = $sqlQuery;
            }

            // Combine condition into one
            if (count($conditions) <= 0)
                $conditions = "";
            else
                $conditions = "WHERE ".implode(" AND ", $conditions);

            $orderBy = '';
            if (array_key_exists('order-by', $options)) {
                $orderBy .= 'ORDER BY ';

                $order = [];
                foreach ($options['order-by'] as $sortOrder => $fieldName) {
                    $order[] = $this->getColumnName($fieldName).
                        (strtoupper($sortOrder) == 'DESC' ? ' DESC' :
                            (strtoupper($sortOrder) == 'ASC' ? ' ASC' : ''));
                }

                $orderBy .= implode(', ', $order);
            }

            // Build sql query
            $sql = "SELECT *
                    FROM %table
                    %conditions
                    %orderBy;";
            $sql = Util::buildString($sql, array(
                    'table' =>      $tableName,
                    'conditions' => $conditions,
                    'orderBy' =>    $orderBy
                ));

            // Use Connection handler to execute query
            $result = $this->handler()->query($sql);

            if ($result->getType() == SQLResult::$TYPE_FAILURE)
                return Result::asFailure(Result::$ERROR_SQL_FAILURE);

            return Result::asSuccess($this->sqlResultToEntity($result));
        }

        public function insert() {
            if (!$this->handler())
                (new \HK\Error('ER0100'))->send();

            $tableName = $this->getTableName();
            list($knownFields, $unknownFields) = $this->classifyField();
            if (count($knownFields) <= 0)
                return Result::asFailure(Result::$ERROR_INSERT_WITH_NO_VALUE, array(
                    'entity' => get_class($this)
                ));

            $valueList = [];
            $columnList = [];
            foreach ($knownFields as $name => $field) {
                $sqlValue = $field->toSqlValue($this->handler());
                if ($sqlValue instanceof \HK\Error)
                    return Result::asFailure(Result::$ERROR_INVALID_FIELD_QUERY, array(
                        'entity' =>     get_class($this),
                        'field' =>      $name,
                        'helper' =>     $sqlValue->getMsg()
                    ));

                $valueList[] = $sqlValue;
                $columnList[] = $name;
            }

            // Combine value and column list
            $valueList = implode(', ', $valueList);
            $columnList = implode(', ', $columnList);

            // Build sql query
            $sql = "INSERT INTO %tableName (%columnList) VALUES (%valueList);";
            $sql = Util::buildString($sql, array(
                    'tableName' =>      $tableName,
                    'columnList' =>     $columnList,
                    'valueList' =>    $valueList
                ));

            // Use Connection handler to execute query
            $result = $this->handler()->query($sql);

            if ($result->getType() == SQLResult::$TYPE_FAILURE)
                return Result::asFailure(Result::$ERROR_SQL_FAILURE);

            $primaryKeys = $this->getPrimaryKeys();
            $insertIds = array_map('trim', explode(',', $this->handler()->getLastInsertId()));
            if ($insertIds === false) {
                return Result::asFailure(Result::$ERROR_INSERT_SUCCESS_BUT_CANNOT_RELOAD);
            }

            $index = 0;
            foreach ($primaryKeys as $name => $field) {
                if ($field->isUnknown() && $insertIds[$index] !== 0) {
                    $this->$name = $insertIds[$index];
                }

                $index++;
            }

            return $this->reload();
        }

        public function delete() {
            if (!$this->handler())
                (new \HK\Error('ER0100'))->send();

            $tableName = $this->getTableName();
            list($knownFields, $unknownFields) = $this->classifyField();
            if (count($knownFields) <= 0)
                return Result::asFailure(Result::$ERROR_DELETE_WITH_NO_VALUE,
                    array('entity' => get_class($this)));

            // Use primary key to query result.
            // All primary key need to be valid (Not unknown or any thing weird).
            $conditions = [];
            foreach ($knownFields as $name => $field) {
                $sqlQuery = $field->toSqlQuery($this->handler());
                if ($sqlQuery instanceof \HK\Error)
                    return Result::asFailure(Result::$ERROR_INVALID_FIELD_QUERY, array(
                        'entity' =>     get_class($this),
                        'field' =>      $name,
                        'helper' =>     $sqlQuery->getMsg()
                    ));

                $conditions[] = $sqlQuery;
            }

            // Combine condition into one
            $conditions = "WHERE ".implode(" AND ", $conditions);

            $sql = "DELETE FROM %tableName %conditions;";
            $sql = Util::buildString($sql, array(
                    'tableName' =>      $tableName,
                    'conditions' =>     $conditions
                ));

            // Use Connection handler to execute query
            $result = $this->handler()->query($sql);
            if ($result->getType() == SQLResult::$TYPE_FAILURE) {
                return Result::asFailure(Result::$ERROR_SQL_FAILURE);
            }

            return Result::asSuccess(true);
        }

        public function clear() {
            if (!$this->handler())
                (new \HK\Error('ER0100'))->send();

            $tableName = $this->getTableName();
            $sql = "DELETE FROM %tableName;";
            $sql = Util::buildString($sql, array(
                    'tableName' =>      $tableName
                ));

            // Use Connection handler to execute query
            $result = $this->handler()->query($sql);
            if ($result->getType() == SQLResult::$TYPE_FAILURE) {
                return Result::asFailure(Result::$ERROR_SQL_FAILURE);
            }

            return Result::asSuccess(true);
        }

        public function updateTo($destEntity) {
            if (!$this->handler())
                (new \HK\Error('ER0100'))->send();

            if (get_class($this) != get_class($destEntity)) {
                return Result::asFailure(Result::$ERROR_UPDATE_SOURCE_AND_DEST_NOT_IDENTICAL,
                    array(
                        'source.entity' => get_class($this),
                        'dest.entity' => get_class($destEntity),
                    ));
            }

            $tableName = $this->getTableName();
            list($srcKnownFields, $srcUnknownFields) = $this->classifyField();
            if (count($srcKnownFields) <= 0)
                return Result::asFailure(Result::$ERROR_UPDATE_SOURCE_ENTITY_HAVE_NO_VALUE,
                    array(
                        'entity' => get_class($this)
                    ));

            list($destKnownFields, $destUnknownFields) = $destEntity->classifyField();
            if (count($destKnownFields) <= 0)
                return Result::asFailure(Result::$ERROR_UPDATE_DEST_ENTITY_HAVE_NO_VALUE,
                    array(
                        'entity' => get_class($destEntity)
                    ));

            $conditions = [];
            foreach ($srcKnownFields as $name => $field) {
                $sqlQuery = $field->toSqlQuery($this->handler());
                if ($sqlQuery instanceof \HK\Error)
                    return Result::asFailure(Result::$ERROR_INVALID_FIELD_QUERY, array(
                        'entity' =>     get_class($this),
                        'field' =>      $name,
                        'helper' =>     $sqlQuery->getMsg()
                    ));

                $conditions[] = $sqlQuery;
            }

            // Combine condition into one
            $conditions = "WHERE ".implode(" AND ", $conditions);

            $assignments = [];
            foreach ($destKnownFields as $name => $field) {
                // Skip primary key as they will be used as condition.
                if ($field->isPrimaryKey())
                    continue;

                $sqlValue = $field->toSqlValue($this->handler(), true);
                if ($sqlValue instanceof \HK\Error)
                return Result::asFailure(Result::$ERROR_INVALID_FIELD_VALUE, array(
                    'entity' =>     get_class($this),
                    'field' =>      $name,
                    'helper' =>     $sqlValue->getMsg()
                ));

                $assignments[] = $sqlValue;
            }

            // Combine assignment into one
            $assignments = "SET ".implode(", ", $assignments);

            // Build sql query
            $sql = "UPDATE %tableName %assignments %conditions;";
            $sql = Util::buildString($sql, array(
                    'tableName' =>      $tableName,
                    'assignments' =>    $assignments,
                    'conditions' =>     $conditions
                ));

            // Use Connection handler to execute query
            $result = $this->handler()->query($sql);
            if ($result->getType() == SQLResult::$TYPE_FAILURE) {
                return Result::asFailure(Result::$ERROR_SQL_FAILURE);
            }

            return Result::asSuccess(true);
        }

        //--------------------------------------------------------------------------------------------------------------
        protected function sqlResultToEntity($result) {
            $rows = $result->getAll();
            $resultList = [];
            foreach ($rows as $row) {
                $clone = $this->copy();
                foreach ($this->fields as $name => $field) {
                    $columnName = $this->getColumnName($name);
                    if (array_key_exists($columnName, $row)) {
                        // Get data and project it in clone entity
                        $data = $row[$columnName];
                        $clone->fields[$name]->set($data);
                    }
                }

                // Add new entity to result list
                $resultList[] = $clone;
            }

            return $resultList;
        }

        protected function getTableName() {
            // Check if option name exist
            // If yes, then replace table name with this option
            if (!$this->getOption('name')) {
                $entityName = Util::getClass($this);
                if (!$this->handler())
                    (new \HK\Error('ER0100'))->send();

                $this->setOption('name', $this->handler()->getTableName($entityName));
            }

            return $this->getOption('name');
        }

        protected function getColumnName($name) {
            if (!$this->fields[$name]->getOption('name')) {
                if (!$this->handler())
                    (new \HK\Error('ER0100'))->send();

                $this->fields[$name]->setOption('name', $this->handler()->getColumnName($name));
            }

            return $this->fields[$name]->getOption('name');
        }

        protected function handler() {
            return BaseEntity::getHandler(get_class($this));
        }

        protected function classifyField() {
            $result = array([], []);
            foreach ($this->fields as $name => $field) {
                // Seperate fields into known and unknown set
                if (!$field->isUnknown())
                    $result[0][$name] = $field;
                else
                    $result[1][$name] = $field;
            }

            return $result;
        }
    }