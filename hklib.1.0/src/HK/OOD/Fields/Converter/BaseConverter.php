<?php
    namespace HK\OOD\Fields\Converter;

    class BaseConverter {

        public static function convert($data, $options, $handler) {
            if ($data === null)
                return 'null';

            return $data;
        }
    }