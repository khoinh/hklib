<?php
    namespace HK\OOD\Fields\Converter;

    class DateConverter extends BaseConverter {

        public static function convert($data, $options, $handler) {
            $data = BaseConverter::convert($data, $options, $handler);
            if ($data == 'null')
                return $data;

            // Get Database date format
            $dbFormat = $handler->getConnection()->getDateTimeFormat();
            $result = \HK\Formatter::format($data, 'string', ['format' => $dbFormat]);
            return "'$result'";
        }
    }