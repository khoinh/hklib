<?php
    namespace HK\OOD\Fields\Converter;

    class StringConverter extends BaseConverter {

        public static function convert($data, $options, $handler) {
            $data = BaseConverter::convert($data, $options, $handler);
            if ($data == 'null')
                return $data;

            if (gettype($data) == 'array' || gettype($data) == 'object') {
                $data = print_r($data, true);
            } else if (gettype($data) != 'string') {
                $data = (string)$data;
            }

            $data = $handler->escapeString($data);
            if ($data instanceof \HK\Error)
                return $data;

            return "'$data'";
        }
    }