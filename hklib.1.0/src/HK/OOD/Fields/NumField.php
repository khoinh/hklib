<?php
    namespace HK\OOD\Fields;

    class NumField extends BaseField {

        public function __construct($options = []) {
            $this->setDefaultOptions(array(
                'nullable'
            ));

            $this->init($options);

            $this->setValueValidator('\HK\OOD\Fields\Validator\NumberValidator');
        }
    }