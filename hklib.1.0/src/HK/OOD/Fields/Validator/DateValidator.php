<?php
    namespace HK\OOD\Fields\Validator;

    use HK\OOD\Unknown as Unknown;
    use HK\Error as Error;

    class DateValidator extends BaseValidator {

        public static function validate($data, $options) {
            $check = BaseValidator::validate($data, $options);
            if ($check !== true)
                return $check;

            if ($data != null) {
                // Try to convert data to datetime object (If fail, formatter will throw an error)
                \HK\Formatter::format($data, 'datetime', ['format' => $options['format']]);
            }

            return true;
        }
    }