<?php
    namespace HK\OOD\Fields\Validator;

    use HK\OOD\Unknown as Unknown;
    use HK\Error as Error;

    class NumberValidator extends BaseValidator {

        public static function validate($data, $options) {
            $check = BaseValidator::validate($data, $options);
            if ($check !== true)
                return $check;

            if ($data === true) {
                $data = 1;
            } else if ($data === false) {
                $data = 0;
            }

            if (!is_numeric($data))
                return new Error("ER0303");

            return true;
        }
    }