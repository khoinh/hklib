<?php
    namespace HK\OOD\MySQL;

    class ConnectionHandler extends \HK\OOD\ConnectionHandler {
        public function init($connection, $config) {
        }

        public function query($sql) {
            if ($this->connection->isReady())
                return $this->connection->query($sql);
            else
                (new \HK\Error("ER0105"))->send();
        }
    }