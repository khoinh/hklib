<?php
    namespace HK\OOD;

    class Result {

        public static $ERROR_INVALID_FIELD_VALUE;
        public static $ERROR_INVALID_FIELD_QUERY;
        public static $ERROR_SQL_FAILURE;
        public static $ERROR_NO_ENTITY_FOUND;
        public static $ERROR_SAVE_WITH_NO_VALUE;
        public static $ERROR_INSERT_WITH_NO_VALUE;
        public static $ERROR_DELETE_WITH_NO_VALUE;
        public static $ERROR_SAVE_OR_REMOVE_WITH_NO_PRIMARY_KEYS;
        public static $ERROR_UPDATE_SOURCE_AND_DEST_NOT_IDENTICAL;
        public static $ERROR_UPDATE_SOURCE_ENTITY_HAVE_NO_VALUE;
        public static $ERROR_UPDATE_DEST_ENTITY_HAVE_NO_VALUE;
        public static $ERROR_INSERT_SUCCESS_BUT_CANNOT_RELOAD;

        private static $errorMap;

        public static function init() {
            Result::$ERROR_INVALID_FIELD_VALUE = 0;
            Result::$ERROR_INVALID_FIELD_QUERY = 1;
            Result::$ERROR_SQL_FAILURE = 2;
            Result::$ERROR_NO_ENTITY_FOUND = 3;
            Result::$ERROR_SAVE_WITH_NO_VALUE = 4;
            Result::$ERROR_INSERT_WITH_NO_VALUE = 5;
            Result::$ERROR_DELETE_WITH_NO_VALUE = 6;
            Result::$ERROR_SAVE_OR_REMOVE_WITH_NO_PRIMARY_KEYS = 7;
            Result::$ERROR_UPDATE_SOURCE_AND_DEST_NOT_IDENTICAL = 8;
            Result::$ERROR_UPDATE_SOURCE_ENTITY_HAVE_NO_VALUE = 9;
            Result::$ERROR_UPDATE_DEST_ENTITY_HAVE_NO_VALUE = 10;
            Result::$ERROR_INSERT_SUCCESS_BUT_CANNOT_RELOAD = 11;

            Result::$errorMap = array(
                    Result::$ERROR_INVALID_FIELD_VALUE =>                   "ER0101",
                    Result::$ERROR_INVALID_FIELD_QUERY =>                   "ER0108",
                    Result::$ERROR_SQL_FAILURE =>                           "ER0114",
                    Result::$ERROR_NO_ENTITY_FOUND =>                       "ER0115",
                    Result::$ERROR_SAVE_OR_REMOVE_WITH_NO_PRIMARY_KEYS =>   "ER0119",
                    Result::$ERROR_SAVE_WITH_NO_VALUE =>                    "ER0120",
                    Result::$ERROR_INSERT_WITH_NO_VALUE =>                  "ER0121",
                    Result::$ERROR_DELETE_WITH_NO_VALUE =>                  "ER0122",
                    Result::$ERROR_UPDATE_SOURCE_AND_DEST_NOT_IDENTICAL =>  "ER0123",
                    Result::$ERROR_UPDATE_SOURCE_ENTITY_HAVE_NO_VALUE =>    "ER0124",
                    Result::$ERROR_UPDATE_DEST_ENTITY_HAVE_NO_VALUE =>      "ER0125",
                    Result::$ERROR_INSERT_SUCCESS_BUT_CANNOT_RELOAD =>      "ER0126"
                );
        }

        public static function asFailure($code, $tokens = []) {
            (new \HK\Error(self::getErrorCode($code), $tokens))->send();
            return null;
        }

        public static function asSuccess($value) {
            return $value;
        }

        private static function getErrorCode($code) {
            if (array_key_exists($code, Result::$errorMap))
                return Result::$errorMap[$code];

            return "ER0000";
        }
    }

    Result::init();