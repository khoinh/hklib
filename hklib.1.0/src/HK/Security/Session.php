<?php
    namespace HK\Security;

    use HK\Error;

    class Session {

        private static $started;

        static function init() {
            self::$started = false;
        }

        static function start($sessionName = 'default_secure_session') {
            if (self::$started) {
                self::destroy();
            }

            // Session access through secure connection only
            $secure = false;

            // This stops JavaScript being able to access the session id.
            $httponly = true;

             // Forces sessions to only use cookies.
            if (ini_set('session.use_only_cookies', 1) === FALSE) {
                return new Error('ER0500');
            }

            // Gets current cookies params.
            $cookieParams = session_get_cookie_params();
            session_set_cookie_params($cookieParams["lifetime"],
                $cookieParams["path"],
                $cookieParams["domain"],
                $secure,
                $httponly);

            // Sets the session name to the one set above.
            session_name($sessionName);
            session_start();
            self::$started = true;

            // regenerated the session, delete the old one.
            session_regenerate_id(true);
        }

        static function destroy() {
            if (!self::$started) {
                self::start();
            }

            session_destroy();
            self::$started = false;
        }

        static function clean() {
            if (!self::$started) {
                self::start();
            }

            session_unset();
        }

        static function set($name, $value) {
            // PHP 5.4 and above
            if (session_status() == PHP_SESSION_NONE) {
                self::start();
            }

            // PHP bellow 5.4
            // if(session_id() == '') {
            //     self::start();
            // }

            $_SESSION[$name] = $value;
        }

        static function get($name, $default = null) {
            // PHP 5.4 and above
            if (session_status() == PHP_SESSION_NONE) {
                return null;
            }

            // PHP bellow 5.4
            // if(session_id() == '') {
            //     return null;
            // }

            if (!array_key_exists($name, $_SESSION))
                return $default;

            return $_SESSION[$name];
        }
    }

    Session::init();