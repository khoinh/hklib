<?php
    namespace HK\Template;

    use HK\Error as Error;

    class Component {
        protected $template;
        protected $context;

        public function __construct($template = null, $context = []) {
            $this->template = $template;
            $this->context = $context;
        }

        public function setTemplate($template) {
            $this->template = $template;
            return $this;
        }

        public function getTemplate() {
            return $this->template;
        }

        public function setContext($context) {
            $this->context = $context;
            return $this;
        }

        public function addContext($name, $value) {
            $this->context[$name] = $value;
            return $this;
        }

        public function getContext() {
            return $this->context;
        }

        public function updateContext($newContext = []) {
            if (empty($this->context)) {
                $this->context = $newContext;
                return $this;
            }

            $this->context = \HK\Util::mergeArray($this->context, $newContext);
            return $this;
        }

        public function beforeBuild() {}
        public function afterBuild() {}
    }