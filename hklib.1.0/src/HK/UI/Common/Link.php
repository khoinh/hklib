<?php
    namespace HK\UI\Common;

    use HK\UI\Component;

    class Link extends Component {

        public function __construct($url = '#', $title = '') {
            parent::__construct('a');
            $this->setUrl($url);
            $this->setTitle($title);
        }

        public function setUrl($input) {
            if ($input instanceof \HK\Resources) {
                $this->setAttribute('href', $input->link());
            } else {
                $this->setAttribute('href', $input);
            }

            return $this;
        }

        public function getUrl() {
            return $this->getAttribute('href');
        }

        public function setTitle($title) {
            $this->setContent($title);
            return $this;
        }

        public function getTitle() {
            return $this->getContent();
        }
    }