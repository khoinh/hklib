<?php
    namespace HK\UI\Common;

    use HK\UI\Component;

    class ListView extends \HK\UI\Component {

        public function __construct() {
            parent::__construct('ul');

            $this->addClass('no-marker')
                ->addClass('no-spacing');

            $this->disableMarker()
                ->disableSpacing()
                ->disableInline();

            $this->setContent([]);
        }

        public function add($name) {
            $this->content += [$name => new Component('li')];
            return $this->content[$name];
        }

        public function get($name) {
            return $this->content[$name];
        }

        public function remove($name) {
            unset($this->cpntent[$name]);
            return $this;
        }

        public function enableMarker() {
            $this->removeClass('no-marker');
            return $this;
        }

        public function disableMarker() {
            $this->addClass('no-marker');
            return $this;
        }

        public function enableSpacing() {
            $this->removeClass('no-spacing');
            return $this;
        }

        public function disableSpacing() {
            $this->addClass('no-spacing');
            return $this;
        }

        public function enableInline() {
            $this->addClass('inline');
            return $this;
        }

        public function disableInline() {
            $this->removeClass('inline');
            return $this;
        }
    }