<?php
    namespace HK\UI\Form\Action;

    use HK\Http\Request;
    use HK\Http\Response;
    use HK\Error;

    class BaseHandler {

        private $form;

        public function __construct() {
            $this->form = null;
        }

        public function setForm($form) {
            if (!$form instanceof \HK\UI\Form\BaseForm) {
                (new Error('ER0407'))->send();
            }

            $this->form = $form;
            return $this;
        }

        public function getForm() {
            return $this->form;
        }

        //--------------------------------------------------------------------------------------------------------------
        // Process data
        public function process($submitCode) {
            $this->beforeProcess($submitCode);
            $this->handleProcess($submitCode);
            $this->afterProcess($submitCode);
        }

        protected function beforeProcess($submitCode) {

        }

        protected function afterProcess($submitCode) {

        }

        protected function handleProcess($submitCode) {
            if ($this->form) {
                $layout = $this->form->getLayoutBuilder();
                $callback = $this->form->getCallback();
                $method = $this->form->getMethod();     // post()
                $hasMethod = "has".ucfirst($method);    // hasPost()

                // Update field value (From Request)
                $fieldList = $layout->getFieldList();
                foreach ($fieldList as $name => $field) {
                    if (Request::$hasMethod($field->getName())) {
                        // Normal field
                        $data = Request::$method($field->getName());
                        $field->setValue($data);
                    } else {
                        // Multipart file field
                        $file = Request::file($field->getName());
                        if ($file) {
                            $field->setValue($file);
                        }
                    }
                }

                // If form was submited, validate field value
                if ($submitCode !== null) {
                    $numError = $this->validate();
                    if ($numError <= 0) {
                        $callback->trigger('success', $this->prepareParameters(), $submitCode);
                    } else {
                        $callback->trigger('failure', $this->prepareParameters(), $numError, $submitCode);
                    }
                } else {
                    $callback->trigger('init-empty-form');
                }
            }
        }

        protected function prepareParameters() {
            if ($this->form) {
                $result = new \HK\Parameters();

                $fieldList = $this->form->getLayoutBuilder()->getFieldList();
                foreach ($fieldList as $name => $field) {
                    $result->$name = $field->getValue();
                }

                return $result;
            }

            return null;
        }

        //--------------------------------------------------------------------------------------------------------------
        // Validate data
        public function validate() {
            $this->beforeValidate();
            $numError = $this->handleValidate();
            $this->afterValidate($numError);

            return $numError;
        }

        protected function beforeValidate() {

        }

        protected function afterValidate($numError) {

        }

        protected function handleValidate() {
            if ($this->form) {
                $layout = $this->form->getLayoutBuilder();
                $fieldList = $layout->getFieldList();

                $errorFound = 0;
                foreach ($fieldList as $name => $field) {
                    if (!$field->validate())
                        $errorFound++;
                }

                return $errorFound;
            }

            return 0;
        }
    }