<?php
    namespace HK\UI\Form\Fields;

    use HK\Error;

    class BaseField extends \HK\UI\Component {

        protected $data;
        protected $name;
        protected $helperMessage;
        protected $error;
        protected $validOptionList;
        protected $label;
        protected $callback;

        public function __construct($name) {
            parent::__construct();

            $this->data = null;
            $this->helperMessage = null;
            $this->error = null;
            $this->label = null;

            $this->setTag('input', false)
                ->setName($name)
                ->setValidOptions([
                    'required' =>           false,
                    'set-data-to' =>        'attr-value',
                    'entity-equivalent' =>  true
                ])
                ->setCallback(new \HK\Handler\Callback($this));

            $this->setTemplate('hk:UI/Form/Fields/BaseField');
        }

        public function getCallback() {
            return $this->callback;
        }

        public function setCallback($callback) {
            $this->callback = $callback;
            return $this;
        }

        public function getLabel() {
            return $this->label;
        }

        public function setLabel($label) {
            $this->label = $label;
            return $this;
        }

        public function getName() {
            return $this->name;
        }

        public function setName($name) {
            $this->name = $name;
            return $this;
        }

        public function getValidOptions() {
            return $this->validOptionList;
        }

        protected function setValidOptions($options) {
            $this->validOptionList = $options;
            return $this;
        }

        protected function updateValidOptions($options) {
            $this->validOptionList = array_merge($this->validOptionList, $options);
            return $this;
        }

        protected function removeValidOptions($options) {
            $this->validOptionList = array_diff($this->validOptionList, $options);
            return $this;
        }

        protected function clearValidOptions() {
            $this->validOptionList = [];
            return $this;
        }

        public function getOption($name) {
            if (array_key_exists($name, $this->validOptionList)) {
                return $this->validOptionList[$name];
            }

            return null;
        }

        public function setOption($name, $value) {
            if (!array_key_exists($name, $this->validOptionList)) {
                (new Error('ER0412', array(
                    'name' => $name,
                    'list' => implode('; ', array_keys($this->validOptionList))
                )))->send();
            }

            $this->checkOption($name, $value);
            $this->validOptionList[$name] = $value;
            return $this;
        }

        protected function checkOption($name, $value) {
            if ($name == 'set-data-to' && !in_array($value, ['attr-value', 'inner-html'])) {
                (new Error('ER0415', array(
                    'name' =>   $name,
                    'list' =>   implode('; ', ['attr-value', 'inner-html']),
                    'value' =>  print_r($value, true)
                )))->send();
            } else if ($name == 'required' && !is_bool($value)) {
                (new Error('ER0415', array(
                    'name' =>   $name,
                    'list' =>   implode('; ', ['boolean']),
                    'type' =>   gettype($value)
                )))->send();
            }
        }

        public function getValue() {
            return $this->data;
        }

        public function setValue($data) {
            $oldData = $this->data;
            $this->callback->trigger('before-set-data', $oldData, $data);
            $this->data = $data;
            $this->callback->trigger('after-set-data', $oldData, $data);
            return $this;
        }

        public function getHelperMessage() {
            return $this->helperMessage;
        }

        public function setHelperMessage($helperMessage) {
            $this->helperMessage = $helperMessage;
            return $this;
        }

        public function hasError() {
            return $this->error !== null;
        }

        public function getError() {
            return $this->error;
        }

        public function setError($name, $tokens = []) {
            if (FieldErrors::has($name)) {
                $this->callback->trigger('before-set-error', $name, $tokens);
                $this->error = $name;
                $this->helperMessage = \HK\Util::buildString(FieldErrors::get($name), $tokens);
                $this->callback->trigger('after-set-error', $name, $tokens);
            } else {
                (new \HK\Error('ER0409', array(
                    'type' => $type,
                    'list' => implode(', ', FieldErrors::getAvailableErrorType())
                )))->send();
            }

            return $this;
        }

        public function clearError() {
            $this->error = null;
            $this->helperMessage = null;
            return $this;
        }

        public function beforeBuild() {
            $this->setAttribute('name', $this->name);
            if ($this->getOption('set-data-to') == 'attr-value') {
                $this->setAttribute('value', $this->data);
            } else if ($this->getOption('set-data-to') == 'inner-html') {
                $this->setContent($this->data);
            }

            parent::beforeBuild();
            $this->updateContext([
                'required' =>       $this->getOption('required'),
                'label' =>          $this->label,
                'helperMessage' =>  ($this->hasError() ? $this->helperMessage : null)
            ]);
        }

        public function validate() {
            $this->beforeValidate();
            $isSuccess = $this->performValidate();
            $this->afterValidate();

            return $isSuccess;
        }

        protected function beforeValidate() {

        }

        protected function afterValidate() {

        }

        public function isEmpty() {
            return empty($this->data);
        }

        protected function performValidate() {
            if ($this->getOption('required') && $this->isEmpty()) {
                $this->setError('empty-data');
                return false;
            }

            return true;
        }
    }