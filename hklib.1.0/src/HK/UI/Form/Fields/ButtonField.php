<?php
    namespace HK\UI\Form\Fields;

    class ButtonField extends SubmitAbleField {

        public function __construct($name = '') {
            parent::__construct($name);
            $this->addClass('btn');
        }

        public function beforeBuild() {
            if ($this->getOption('as-submit')) {
                $this->setAttribute('type', 'submit');
            } else {
                $this->setAttribute('type', 'button');
            }

            parent::beforeBuild();
        }
    }