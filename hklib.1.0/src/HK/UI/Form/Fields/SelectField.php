<?php
    namespace HK\UI\Form\Fields;

    use \HK\Util;
    use \HK\Error;

    class SelectField extends BaseField {

        public function __construct($name) {
            parent::__construct($name);

            $this->setTag('select')
                ->updateValidOptions([
                    'default' =>        0,
                    'option-list' =>    []]
                );
        }

        protected function checkOption($name, $value) {
            parent::checkOption($name, $value);

            if ($name == 'option-list' && gettype($value) != 'array') {
                (new Error('ER0414', [
                    'name' =>           $name,
                    'required.type' =>  'array',
                    'type' =>           gettype($value)
                ]))->send();
            }
        }

        public function getValue() {
            if ($this->data == null && $this->getOption('required')) {
                $this->data = $this->getOption('default');
            }

            return $this->data;
        }

        public function isEmpty() {
            return $this->data == null || $this->data == '';
        }

        protected function performValidate() {
            if (parent::performValidate()) {
                if ($this->getOption('required') &&
                    !array_key_exists($this->getValue(), $this->getOption('option-list'))) {
                    $this->setError('option-not-exist');
                    return false;
                }
            }

            return true;
        }

        protected function createOptionItem($value, $name) {
            $option = (new \HK\UI\Component())
                ->setTag('option')
                ->setAttribute('value', $value)
                ->setContent($name);

            $fieldValue = $this->getValue();
            if (($fieldValue !== null && $fieldValue == $value)) {
                $option->setAttribute('selected', 'selected');
            }

            return $option;
        }

        public function beforeBuild() {
            $optionListComponent = new \HK\Template\ComponentList();
            $optionList = $this->getOption('option-list');

            if (!$this->getOption('required')) {
                $optionListComponent->add('', $this->createOptionItem('', ''));
            }

            foreach ($optionList as $value => $name) {
                $optionListComponent->add($value, $this->createOptionItem($value, $name));
            }

            $this->setContent($optionListComponent);

            parent::beforeBuild();
        }
    }