<?php
    namespace HK\UI\Form\Fields;

    use HK\Error;

    class SubmitAbleField extends BaseField {

        public function __construct($name) {
            parent::__construct($name);

            $this->updateValidOptions([
                'as-submit' =>          true,
                'entity-equivalent' =>  false
            ]);
        }
    }