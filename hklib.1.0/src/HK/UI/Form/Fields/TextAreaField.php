<?php
    namespace HK\UI\Form\Fields;

    use \HK\Util;
    use \HK\Error;

    class TextAreaField extends TextField {

        public function __construct($name) {
            parent::__construct($name);
            $this->setTag('textarea')
                ->setOption('set-data-to', 'inner-html');
        }
    }