<?php
    namespace HK\UI\Form\Layout;

    use HK\UI\Form\Fields\BaseField;

    abstract class BaseBuilder {

        protected $layout;
        protected $fieldList;
        protected $messages;

        public function __construct() {
            $this->layout = null;
            $this->fieldList = [];
            $this->clearAllMesages();
        }

        public function build() {
            $this->buildLayout();
            return $this->layout;
        }

        protected function setLayout($layout) {
            $this->layout = $layout;
            return $this;
        }

        public function getFieldList() {
            return $this->fieldList;
        }

        public function getField($name) {
            if (array_key_exists($name, $this->fieldList)) {
                return $this->fieldList[$name];
            }

            return null;
        }

        public function addMessage($type, $message) {
            if (isset($this->messages[$type])) {
                $this->messages[$type][] = $message;
            }

            return $this;
        }

        public function clearAllMesages() {
            $this->messages = [
                'info' =>       [],
                'error' =>      [],
                'warning' =>    []
            ];
            return $this;
        }

        public function getAllMessages() {
            return $this->messages;
        }

        protected function checkField($field) {
            if (!$field instanceof BaseField) {
                (new Error('ER0406'))->send();
            }

            $name = $field->getName();
            if (empty($name)) {
                (new Error('ER0410'))->send();
            }
        }

        public function addField($field) {
            $this->checkField($field);
            $this->beforeAddField($field);
            $this->fieldList[$field->getName()] = $field;
            $this->afterAddField($field);
            return $field;
        }

        protected function beforeAddField($field) {}
        protected function afterAddField($field) {}

        abstract protected function buildLayout();
    }