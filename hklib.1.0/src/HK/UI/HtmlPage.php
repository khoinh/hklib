<?php
    namespace HK\UI;

    use HK\Context as Context;
    use HK\Template\ComponentList as ComponentList;
    use HK\UI\StyleSheet;
    use HK\UI\JavaScript;

    class HtmlPage extends \HK\Template\Component {

        private $charset;
        private $keywords;
        private $description;
        private $title;
        private $body;
        private $jsSrcList;
        private $cssSrcList;

        public function __construct() {
            parent::__construct();
            $this->setTemplate('hk:UI/HtmlPage');

            $this->charset = Context::charset();
            $this->cssSrcList = Context::getCssFiles();
            $this->jsSrcList = Context::getJavaScripts();
            $this->keywords = null;
            $this->description = null;
            $this->title = null;
            $this->body = null;
        }

        public function setCharset($charset) {
            $this->charset = $charset;
            return $this;
        }

        public function getCharset() {
            return $this->charset;
        }

        public function setKeywords($keywords) {
            $this->keywords = $keywords;
            return $this;
        }

        public function getKeywords() {
            return $this->keywords;
        }

        public function setDescription($description) {
            $this->description = $description;
            return $this;
        }

        public function getDescription() {
            return $this->description;
        }

        public function setTitle($title) {
            $this->title = $title;
            return $this;
        }

        public function getTitle() {
            return $this->title;
        }

        public function setBody($body) {
            $this->body = $body;
            return $this;
        }

        public function getBody() {
            return $this->body;
        }

        public function addCss($cssFile) {
            $this->cssSrcList[] = $cssFile;
        }

        public function addJs($jsFile) {
            $this->jsSrcList[] = $jsFile;
        }

        public function beforeBuild() {
            parent::beforeBuild();
            $this->updateContext([
                'charset' =>        $this->charset,
                'keywords' =>       $this->keywords,
                'description' =>    $this->description,
                'title' =>          $this->title,
                'cssSrcList' =>     $this->cssSrcList,
                'jsSrcList' =>      $this->jsSrcList,
                'body' =>           $this->body,
            ]);
        }
    }