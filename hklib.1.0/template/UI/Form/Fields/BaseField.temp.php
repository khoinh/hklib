<div class='form-content input-wrapper <?= $label !== null ? 'has-label' : '' ?>'>
    <?php if ($label !== null) { ?>
        <span class='form-content input-label'>
            <?= $label ?>
            <?php if ($required) {?>
                <b class='required-marker'>*</b>
            <?php } ?>
        </span>
    <?php } ?>
    <span class='form-content input-col'>
        <?php require __DIR__."/../../Component.temp.php"; ?>
        <?php if (isset($helperMessage)) { ?>
            <div class='form-content input-helper'><?= $helperMessage ?></div>
        <?php } ?>
    </span>
</div>