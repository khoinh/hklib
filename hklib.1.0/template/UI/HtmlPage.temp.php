<!DOCTYPE HTML>
<html>
<head>
    <meta charset="<?= $charset ?>">
    <meta content="text/html; charset=<?= $charset ?>" http-equiv="content-type">
    <meta name="keywords" content="<?= $keywords ?>">
    <meta name="description" content="<?= $description ?>">
    <title><?= $title ?></title>

    <?php foreach ($jsSrcList->list() as $jsSrcFile) { ?>
        <script type='text/javascript' src='<?= $jsSrcFile ?>'></script>
    <?php } ?>

    <?php foreach ($cssSrcList->list() as $cssSrcFile) { ?>
        <link rel='stylesheet' href='<?= $cssSrcFile ?>'>
    <?php } ?>
</head>
<body>
    <?= present($body) ?>
</body>
</html>