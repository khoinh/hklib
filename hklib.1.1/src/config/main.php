<?php
    $config = \HK\Config();
    $config->framework = (object) [
        'root' =>       __DIR__."/../../",
        'languages' =>  ['en'],
        'extensions' => [
            // 'bmp', 'png', 'jpg', 'icon', 'jpeg', 'gif', // Image
            // 'txt', 'doc', 'pdf', 'excel', 'docx',       // Document
            // 'ini', 'json'                               // Config
        ]
    ];

    $config->app = (object) [
        'root' =>   '',     // Application root folder (Contain application source)
    ];

    $config->backend = (object) [
        'repo' => (object) [
            'src' => [
                'hk' =>     '@framework.root/src/hk',
                'vendor' => '@framework.root/src/vendor'
            ],

            'template' => [
                'hk' =>     '@framework.root/template'
            ]
        ],
    ];

    $config->frontend = (object) [
        'assets' => $_SERVER['DOCUMENT_ROOT']."/assets"
    ];


    return
    [

        //----------------------------------------------------------------------------------------------------------
        // Application configuration
        'application' => [
            'root' =>       '',
            'assets' =>     '',                 // Application public assets folder (Contain css, javascript, images,...)
            'default' =>    [
                'themes' => ''                  // Default application themes
            ]
        ],

        //----------------------------------------------------------------------------------------------------------
        // System repository (fast way to access and seperate source file, resources and other data]
        'repositories' => [

            // Source file container (PHP)
            'src' => [
                'hk' =>     '@system.root/src/HK/',
                'vendor' => '@system.root/src/Vendor'
            ],

            // Web template container
            'template' => [
                'hk' =>     '@system.root/template/'
            ],

            // Client side script (Normally javascript)
            'script' => [
            ],

            // Theme and style container
            'theme' => [
            ],

            // Resources and file container
            'res' => [
                'hk' =>     '@system.root/res/'
            ]
        ],

        //----------------------------------------------------------------------------------------------------------
        // Upload and Download media configuration
        'media' => [
            'upload' => [
                'dir' =>        '', // Upload directory
                'maxsize' =>    10, // Maximum upload file size in MB
            ],

            'download' => [
                'dir' =>        '@media.upload.dir' // Download directory
            ],

            // Specialize each file with extension into sub directory
            'specialize' => [
                // 'image' =>      ['bmp', 'png', 'jpg', 'icon', 'jpeg', 'gif'],
                // 'document' =>   ['txt', 'doc', 'pdf', 'excel', 'docx']
            ]
        ],

        //----------------------------------------------------------------------------------------------------------
        // System autoload source and library
        'autoload' => [
            'src' =>    ['hk:OOD'],
            'script' => []
        ],

        //----------------------------------------------------------------------------------------------------------
        // System configuration
        'debug' => [
            'log' => [
                'enable' => true
            ],

            'error-handler' => [
                'mode' => 'display'
            ]
        ],

        'message-dictionary' => [
            'error' =>      ['hk:error_dict.ini'],
            'warning' =>    ['hk:warning_dict.ini'],
            'form' => [
                'error' =>      ['hk:form_error.ini'],
                'warning' =>    ['hk:form_warning.ini'],
            ]
        ],

        //----------------------------------------------------------------------------------------------------------
        // System default configuration
        'default' => [
            'timezone' =>   'UTC',

            // Default format
            'format' =>     [
                'date' =>       'Y-m-d',
                'datetime' =>   '@default.format.date H:i:s'
            ],

            // Language and encoding
            'language' =>   'en',
            'charset' =>    'UTF-8'
        ],

        //----------------------------------------------------------------------------------------------------------
        // Enable system shortcut. Disable this if there are any conflict with your own library
        'shortcut' => [
            'template' =>   true,     // System template shortcut is in package HK\Template\GlobalShortcut
            'debug' =>      true,     // System debug shortcut is in package HK\Debug\GlobalShortcut
            'import' =>     true
        ]
    ];
?>