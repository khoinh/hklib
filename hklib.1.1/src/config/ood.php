<?php
    return
    [
        'naming-rules' => [
            'default' => [
                'entity' => [
                    'format' => '%name',
                    'apply' =>  []
                ],

                'field' =>  [
                    'format' => '%name',
                    'apply' =>  ['ucfirst']
                ]
            ]
        ],

        'managers' => [
            // 'OODName' => [
            //     'connection' =>     '',
            //     'entities' =>       '@application.root/src/OOD/Entities',
            //     'naming-rule' =>    'default'
            // ]
        ]
    ];
?>