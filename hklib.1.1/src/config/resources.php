<?php
    return
    [
        'caching' => [
            'enable' => false,
            'file' =>   'resource-map.cache'
        ],

        'img' => [
            'hints' => [
                // 'icon' =>       'low',
                // 'background' => 'high'
            ]
        ],

        'doc' => [
            'hints' => [
                'config' => 'ini',
                'data' =>   'json'
            ]
        ],

        'media' => [
            'hints' => [

            ]
        ]
    ];
?>