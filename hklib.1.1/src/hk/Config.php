<?php
    namespace HK;

    class Config {
        //////////////
        // Variable
        private static $data;
        private static $imported;

        ////////////
        // Method
        public static function init($config = []) {
            self::$data = $config;
            self::$imported = [];
            self::insertConfigValue(self::$data);
        }

        public static function update($config = []) {
            self::$data = \HK\Util::mergeArray(self::$data, $config);
            self::insertConfigValue(self::$data);
        }

        //--------------------------------------------------------------------------------
        public static function insertConfigValue(&$data) {
            // Check for @key in config
            // Replace it with valid value
            foreach ($data as $name => $value) {
                if (is_array($value)) {
                    self::insertConfigValue($data[$name]);
                } else if (is_string($value)) {
                    preg_match_all("/@[a-zA-Z0-9\_\.]*/", $value, $matches);
                    if (count($matches) > 0) {
                        // Replace @key with value get in config
                        foreach ($matches[0] as $match) {
                            $key = substr($match, 1);
                            $configValue = self::get($key);
                            if ($configValue !== null) {
                                $data[$name] = str_replace($match, $configValue, $value);
                            }
                        }
                    }
                }
            }
        }

        //--------------------------------------------------------------------------------
        public static function get($key) {
            $path = explode(".", $key);
            $current = self::$data;

            foreach ($path as $key) {
                if (!array_key_exists($key, $current))
                    return null;

                $current = $current[$key];
            }

            return $current;
        }

        public static function isImported($package) {
            return in_array($package, self::$imported);
        }

        public static function imported($package) {
            self::$imported[] = $package;
        }
    }