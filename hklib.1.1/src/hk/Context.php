<?php
    namespace HK;

    class Context {

        //////////////
        // Variable
        private static $language;
        private static $charset;
        private static $data;
        private static $database;
        private static $ood;
        private static $themes;
        private static $javascripts;

        ////////////
        // Method
        public static function init() {
            self::$data = [];
            self::resetDetault();
            self::$database = [];
            self::$ood = [];
            self::$themes = [];
            self::$javascripts = [];
        }

        public static function env($name, $value = null) {
            if ($value != null)
                self::setEnv($name, $value);

            return self::tryAndGetEnv($name);
        }

        public static function get($key) {
            if (array_key_exists($key, self::$data))
                return self::$data[$key];

            return null;
        }

        public static function set($key, $value) {
            self::$data[$key] = $value;
        }

        public static function language($language = null) {
            if ($language !== null && in_array($language, Config::get("supported.languages")))
                self::$language = $language;

            return self::$language;
        }

        public static function charset($charset = null) {
            if ($charset !== null)
                self::$charset = $charset;

            return self::$charset;
        }

        public static function databaseConfig($config = null) {
            if ($config !== null) {
                self::$database = $config;
            }

            return self::$database;
        }

        public static function oodConfig($config = null) {
            if ($config !== null) {
                self::$ood = $config;
            }

            return self::$ood;
        }

        public static function resetDetault() {
            self::$language = Config::get("default.language");
            self::$charset = Config::get("default.charset");
        }

        public static function addJavaScripts(...$scripts) {
            self::$javascripts = array_merge(self::$javascripts, $scripts);
        }

        public static function getJavaScripts() {
            $result = [];
            foreach (self::$javascripts as $source) {
                $source = trim($source);
                if (empty($source)) {
                    continue;
                }

                $result += self::getFileWith($source, "script", "js");
            }

            return $result;
        }

        public static function addThemes(...$themes) {
            self::$themes = array_merge(self::$themes, $themes);
        }

        public static function getCssFiles() {
            $result = [];
            foreach (self::$themes as $theme) {
                $theme = trim($theme);
                if (empty($theme)) {
                    continue;
                }

                $result += self::getFileWith($theme, "theme", "css");
            }

            return $result;
        }

        //--------------------------------------------------------------------------------------------------------------
        private static function getFileWith($direction, $containerName, $extension) {
            $result = [];
            $root = self::env("document_root");

            // Get package name and url
            list($container, $package) = explode(":", $direction);

            // Load container url from repository src
            $container = Config::get("repositories.$containerName.$container");
            if ($container) {
                // Check and load all css file
                $dir = scandir("$root$container$package");
                foreach ($dir as $file) {
                    // Only load file with extension
                    if (Util::getExtension($file) == $extension)
                        $result[] = "$container$package/$file";
                }
            }

            return $result;
        }

        private static function getEnv($name) {
            // Try with raw name
            if (array_key_exists($name, $_ENV))
                return $_ENV[$name];

            if (array_key_exists($name, $_SERVER))
                return $_SERVER[$name];

            return null;
        }

        private static function tryAndGetEnv($name) {
            $result = self::getEnv($name);

            if (!$result) {
                // Try with lower case
                $name = strtolower($name);
                $result = self::getEnv($name);
            }

            if (!$result) {
                // Try with uppercase
                $name = strtoupper($name);
                $result = self::getEnv($name);
            }

            return $result;
        }

        private static function setEnv($name, $value) {
            putenv("$name=$value");
        }
    }

    Context::init();