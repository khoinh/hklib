<?php
    namespace HK\DB\MySQL;

    use HK\DB\SQLResult as SQL;

    class SQLResult extends SQL {

        private $raw;
        private $data;
        private $rowCount;
        private $fieldCount;

        public function __construct($type, $data = null) {
            $this->type = $type;
            $this->raw = $data;
            $this->data = null;
            $this->rowCount = 0;
            $this->fieldCount = 0;

            if ($this->raw && $this->type == SQL::$TYPE_VALUE) {
                $this->data = $this->raw->fetch_all(MYSQLI_ASSOC);
                $this->rowCount = $this->raw->num_rows;
                $this->fieldCount = $this->raw->field_count;
            }
        }

        public function getAll() {
            if ($this->type == SQL::$TYPE_VALUE)
                return $this->data;

            return $this->type == SQL::$TYPE_SUCCESS;
        }

        public function getRow($index) {
            if ($this->type == SQL::$TYPE_VALUE) {
                if ($this->data && $index < $this->rowCount) {
                    return $this->data[$index];
                }

                return null;
            }

            return $this->type == SQL::$TYPE_SUCCESS;
        }

        public function numField() {
            return $this->fieldCount;
        }

        public function numRow() {
            return $this->rowCount;
        }
    }