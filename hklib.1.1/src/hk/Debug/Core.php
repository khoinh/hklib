<?php
    namespace HK\Debug;

    use HK\Config as Config;
    use HK\Context as Context;

    require "Logger.php";
    require "Output.php";
    require "ErrorHandler.php";

    // If shortcut enable
    if (Config::get("shortcut.debug"))
        require __DIR__."/GlobalShortcut.php";

    // Set system error and exception handler
    Context::set('system.error.handler', new ErrorAndExceptionHandler());
