<?php
    function out(...$args) {
        call_user_func_array('HK\Debug\Output::out', $args);
    }

    function outLn(...$args) {
        call_user_func_array('HK\Debug\Output::outLn', $args);
    }

    function outFormat(...$args) {
        call_user_func_array('HK\Debug\Output::outFormat', $args);
    }

    function outFormatLn(...$args) {
        call_user_func_array('HK\Debug\Output::outFormatLn', $args);
    }

    function outFormatAll(...$args) {
        call_user_func_array('HK\Debug\Output::outFormatAll', $args);
    }

    function outFormatAllLn(...$args) {
        call_user_func_array('HK\Debug\Output::outFormatAllLn', $args);
    }