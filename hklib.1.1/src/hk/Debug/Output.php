<?php
    namespace HK\Debug;

    use HK\Util as Util;

    class Output {
        public static function out(...$output) {
            array_unshift($output, "");
            call_user_func_array("HK\Debug\Output::outWithDelimiter", $output);
        }

        public static function outLn(...$output) {
            array_unshift($output, "</br>");
            call_user_func_array("HK\Debug\Output::outWithDelimiter", $output);
        }

        public static function outFormat($string, ...$tokens) {
            print(Util::buildString($string, $tokens));
        }

        public static function outFormatLn($string, ...$tokens) {
            print(Util::buildString($string, $tokens));
            print "</br>";
        }

        public static function outFormatAll(...$output) {
            foreach ($output as $out)
                call_user_func_array("HK\Debug\Output::outFormat", $out);
        }

        public static function outFormatAllLn($string, ...$tokens) {
            foreach ($output as $out)
                call_user_func_array("HK\Debug\Output::outFormatLn", $out);
        }

        public static function outWithDelimiter($delimiter, ...$output) {
            foreach ($output as $out) {
                if (gettype($out) == 'array' || gettype($out) == 'object')
                    print_r($out);
                else
                    print $out;

                print $delimiter;
            }
        }
    }