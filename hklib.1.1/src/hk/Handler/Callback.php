<?php
    namespace HK\Handler;

    class Callback {

        private $handlers;
        private $caller;

        public function __construct($caller = null) {
            $this->handlers = [];
            $this->caller = $caller;
        }

        public function bind($name, $callback) {
            if ($this->has($name)) {
                $this->handlers[$name]['callbacks'][] = $callback;
            } else {
                $this->handlers[$name] = [
                    'callbacks' =>  [$callback],
                    'enable' =>     true
                ];
            }
        }

        public function has($name) {
            return array_key_exists($name, $this->handlers);
        }

        public function disable($name) {
            if ($this->has($name)) {
                $this->handlers[$name]['enable'] = false;
            }

            return $this;
        }

        public function enable($name) {
            if ($this->has($name)) {
                $this->handlers[$name]['enable'] = true;
            }

            return $this;
        }

        public function trigger($name, ...$attrs) {
            if ($this->has($name)) {
                $handler = $this->handlers[$name];
                if ($handler['enable']) {
                    foreach ($handler['callbacks'] as $callback) {
                        if (gettype($this->caller) == 'object') {
                            return call_user_func_array([$this->caller, $callback], $attrs);
                        } else {
                            return call_user_func_array($callback, $attrs);
                        }
                    }
                }
            }

            return null;
        }
    }