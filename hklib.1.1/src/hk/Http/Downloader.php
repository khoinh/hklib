<?php
    namespace HK\Http;

    class Downloader {

        static private $downloadDir;

        static public function init() {
            self::$downloadDir = \HK\Config::get('media.upload.dir');
        }

        static public function getDownloadDir() {
            return self::$downloadDir;
        }

        static public function setDownloadDir($dir) {
            self::$downloadDir = $dir;
        }

        static public function download($fileName, $subDir = '') {
            $root = \HK\Context::env("document_root").\HK\Config::get('application.root');
            $fileDir = $root.self::getUploadDir()."$subDir/$fileName";

            if (file_exists($fileDir)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($fileDir).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($fileDir));

                $result = readfile($fileDir);
                if ($result === false) {
                    return new \HK\Error('ER0015');
                }

                return true;
            } else {
                return new \HK\Error('ER0014', ['file' => $fileDir]);
            }
        }

        static public function downloadAndQuit($fileName, $subDir = '') {
            $result = self::download($fileName, $subDir);
            if ($result === true) {
                exit();
            } else {
                return $result;
            }
        }
    }

    Downloader::init();