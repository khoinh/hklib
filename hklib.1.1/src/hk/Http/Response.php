<?php
    namespace HK\Http;

    class Response {

        public static function create($data = '', $arrayFormat = 'json') {
            return new Response($data, $arrayFormat);
        }

        //--------------------------------------------------------------------------------------------------------------
        //.Member
        private $data;
        private $arrayFormat;

        public function __construct($data, $arrayFormat) {
            $this->data = $data;
            $this->arrayFormat = $arrayFormat;
        }

        public function set($data) {
            $this->data = $data;
            return $this;
        }

        public function get() {
            return $this->data;
        }

        protected function beforeProcess() {
        }

        protected function afterProcess() {
        }

        protected function handleProcess() {
            if (gettype($this->data) == 'string') {
                print $this->data;
            } else if ($this->data instanceof \HK\Template\Component) {
                \present($this->data);
            } else if ($this->arrayFormat == 'json' && gettype($this->data) == 'array') {
                return json_encode($this->data);
            } else {
                var_dump($this->data);
            }
        }

        public function process() {
            $this->beforeProcess();
            $this->handleProcess();
            $this->afterProcess();
            return $this;
        }
    }