<?php
    namespace HK\OOD;

    use HK\OOD\Manager as Manager;
    use HK\Config as Config;
    use HK\Util as Util;

    abstract class ConnectionHandler {
        protected $config;
        protected $connection;

        public function setup($connection, $config) {
            $this->config = $config;
            $this->connection = $connection;
            $this->loadEntities($config['entities']);
            $this->init($connection, $config);
        }

        abstract public function init($connection, $config);
        abstract public function query($sql);

        public function getLastInsertId() {
            return $this->connection->getLastInsertId();
        }

        protected function loadEntities($folder) {
            $root = \HK\Context::env("document_root");
            $dir = "$root$folder";
            if (!is_dir($dir))
                (new \HK\Error("ER0104", array('folder' =>  $folder)))->send();

            $numClass = count(get_declared_classes());

            $files = scandir($dir);
            foreach ($files as $file) {
                if (is_dir("$dir/$file"))
                    continue;

                require_once "$dir/$file";
            }

            if ($numClass < count(get_declared_classes())) {
                list($skip, $entityClasses) = array_chunk(get_declared_classes(), $numClass);
                Entities\BaseEntity::assignHandler($this, $entityClasses);
            }
        }

        public function getTableName($entityName) {
            $rule = Manager::getNamingRule($this->config['naming-rule'])['entity'];
            return $this->getName($entityName, $rule);
        }

        public function getColumnName($fieldName) {
            $rule = Manager::getNamingRule($this->config['naming-rule'])['field'];
            return $this->getName($fieldName, $rule);
        }

        public function escapeString($string) {
            if ($this->connection->isReady())
                return $this->connection->escapeString($string);

            return null;
        }

        protected function getName($name, $rule) {
            $applyList = $rule['apply'];
            foreach ($applyList as $method)
                $name = $method($name);

            return Util::buildString($rule['format'], array('name' => $name));
        }

        public function getConnection() {
            return $this->connection;
        }
    }