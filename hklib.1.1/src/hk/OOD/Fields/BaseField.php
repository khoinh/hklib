<?php
    namespace HK\OOD\Fields;

    use HK\OOD\Unknown as Unknown;
    use HK\OOD\Result as Result;
    use HK\Util as Util;

    class BaseField {

        protected $data;
        protected $entity;
        protected $options;
        protected $required;
        protected $valueValidator;
        protected $valueConverter;
        protected $queryValidator;
        protected $queryConverter;

        //--------------------------------------------------------------------------------------------------------------
        public function init($options) {
            if ($this->options)
                $this->options = \HK\Util::mergeArray($options, $this->options);
            else
                $this->options = $options;

            $this->checkReqriredOption();

            // At first when init, all data will be set to unknown
            $this->data = new Unknown;

            $this->setValueValidator('\HK\OOD\Fields\Validator\BaseValidator');
            $this->setQueryValidator('\HK\OOD\Fields\Validator\QueryValidator');

            $this->setValueConverter('\HK\OOD\Fields\Converter\BaseConverter');
            $this->setQueryConverter('\HK\OOD\Fields\Converter\QueryConverter');

            $this->entity = null;
        }

        public function copy() {
            $field = get_class($this);
            $result = new $field($this->options);
            $result->data = $this->data;
            $result->entity = $this->entity;
            return $result;
        }

        public function setEntity($entity) {
            $this->entity = $entity;
        }

        public function getEntity() {
            return $this->entity;
        }

        public function setValueValidator($validator) {
            $this->valueValidator = $validator;
        }

        public function setValueConverter($converter) {
            $this->valueConverter = $converter;
        }

        public function setQueryValidator($validator) {
            $this->queryValidator = $validator;
        }

        public function setQueryConverter($converter) {
            $this->queryConverter = $converter;
        }

        public function isPrimaryKey() {
            return $this->getOption('primary-key');
        }

        public function setDefaultOptions($options) {
            $this->options = $options;
        }

        public function isValidValue() {
            return call_user_func($this->valueValidator."::validate",
                $this->data, $this->options);
        }

        public function isValidQuery() {
            return call_user_func($this->queryValidator."::validate",
                $this->data, $this->options, $this->valueValidator);
        }

        public function isUnknown() {
            return $this->data instanceof Unknown;
        }

        public function getOption($name) {
            if (array_key_exists($name, $this->options))
                return $this->options[$name];
            else if (in_array($name, $this->options))
                return true;
            else
                return false;
        }

        public function setOption($name, $value) {
            $this->options[$name] = $value;
        }

        public function get() {
            return $this->data;
        }

        public function set($data) {
            $this->data = $data;
        }

        public function toSqlValue($handler, $includeColumnName = false) {
            // TODO: add name to options
            $check = $this->isValidValue();
            if ($check !== true)
                return $check;

            $result = call_user_func($this->valueConverter."::convert", $this->data, $this->options, $handler);
            if ($includeColumnName) {
                $result = $this->getOption('name').'='.$result;
            }

            return $result;
        }

        public function toSqlQuery($handler) {
            $check = $this->isValidQuery();
            if ($check !== true)
                return $check;

            return call_user_func($this->queryConverter."::convert",
                $this->data, $this->options, $handler, $this->valueConverter);
        }

        //--------------------------------------------------------------------------------------------------------------
        protected function setRequiredOption(...$options) {
            $this->required = $options;
        }

        protected function checkReqriredOption() {
            if ($this->required) {
                foreach ($this->required as $option) {
                    // Missing required option error
                    if (!$this->getOption($option))
                        (new \HK\Error("ER0107", array(
                            'field' =>  get_class($this),
                            'option' => $option
                        )))->send();
                }
            }
        }
    }