<?php
    namespace HK\OOD\Fields;

    class CharField extends BaseField {

        public function __construct($options = []) {
            $this->setDefaultOptions(array(
                'nullable'
            ));

            $this->init($options);

            $this->setValueConverter('\HK\OOD\Fields\Converter\StringConverter');
        }
    }