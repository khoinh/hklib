<?php
    namespace HK\OOD\Fields\Converter;

    class ManyToOneConverter extends BaseConverter {

        public static function convert($data, $options, $handler) {
            // Create an temporary enity (for testing perpos only)
            $entity = new $options['entity']();

            // Check if reference field exist inside entity
            $reference = $options['reference'];
            $field = $entity->getField($reference);
            if ($field instanceof Error)
                return $field;

            // Set field data using this data
            $field->set($data);
            return $field->toSqlValue($handler);
        }
    }