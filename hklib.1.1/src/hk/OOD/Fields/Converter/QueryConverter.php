<?php
    namespace HK\OOD\Fields\Converter;

    class QueryConverter {

        public static function convert( $data,
                                        $options,
                                        $handler,
                                        $dataConverter = "HK\OOD\Fields\Converter\BaseConverter",
                                        $condition = '=',
                                        $logic = true) {
            $name = $options['name'];

            if (gettype($data) != 'array') {
                $result = call_user_func($dataConverter.'::convert', $data, $options, $handler);
                if ($result instanceof \HK\Error)
                    return $result;
                return "$name $condition $result";
            } else {
                $result = [];
                foreach ($data as $condition => $value) {
                    if (gettype($condition) == 'integer')
                        $condition = '=';

                    $item = QueryConverter::convert($value,
                                                    $options,
                                                    $handler,
                                                    $dataConverter,
                                                    $condition,
                                                    !$logic);
                    if ($item instanceof \HK\Error)
                        return $item;

                    $result[] = $item;
                }

                return '('.implode($logic ? ' OR ' : ' AND ', $result).')';
            }
        }
    }