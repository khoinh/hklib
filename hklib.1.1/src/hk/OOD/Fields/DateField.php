<?php
    namespace HK\OOD\Fields;

    use HK\Config as Config;

    class DateField extends BaseField {

        public function __construct($options = []) {
            $this->setDefaultOptions(array(
                'nullable',
                'format' => Config::get('default.format.datetime')
            ));

            $this->setRequiredOption('format');

            $this->init($options);

            $this->setValueValidator('\HK\OOD\Fields\Validator\DateValidator');
            $this->setValueConverter('\HK\OOD\Fields\Converter\DateConverter');
        }

        public function set($data) {
            if (gettype($data) == 'array' || $data instanceof \HK\OOD\Unknown) {
                $this->data = $data;
            } else {
                $this->data = \HK\Formatter::format($data, 'datetime');
            }
        }
    }