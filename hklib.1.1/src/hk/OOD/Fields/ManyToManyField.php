<?php
    namespace HK\OOD\Fields;

    use HK\OOD\Unknown as Unknown;
    use HK\OOD\Result as Result;

    class ManyToManyField extends BaseField {

        public function __construct($options = []) {
            $this->setDefaultOptions(array(
                'nullable'
            ));

            $this->setRequiredOption('entity', 'mapping');

            $this->init($options);

            if (!is_subclass_of($this->getOption('entity'), '\HK\OOD\Entities\BaseEntity')) {
                (new \HK\Error("ER0109", array(
                    'field' =>  get_class($this),
                    'entity' => $this->getOption('entity')
                )))->send();
            }

            if (!is_subclass_of($this->getOption('mapping'), '\HK\OOD\Entities\BaseEntity')) {
                (new \HK\Error("ER0117", array(
                    'field' =>  get_class($this),
                    'entity' => $this->getOption('mapping')
                )))->send();
            }
        }

        public function get() {
            $mapping = $this->getOption('mapping');
            $entity = $this->getOption('entity');
            $mapping = new $mapping();

            if (!$this->getOption('reference-field') || !$this->getOption('target-field')) {
                $fields = $mapping->getFieldList();
                $referenceField = null;
                $targetField = null;
                foreach ($fields as $name => $field) {
                    if ($field instanceof \HK\OOD\Fields\ManyToOneField){
                        if ($field->getOption('entity') == '\\'.get_class($this->getEntity())) {
                            $referenceField = $name;
                            continue;
                        } else if ($field->getOption('entity') == $entity) {
                            $targetField = $name;
                            continue;
                        }
                    }
                }

                if ($referenceField === null || $targetField === null) {
                    (new \HK\Error("ER0118", array(
                        'mapping.entity' => $this->getOption('mapping'),
                        'this.entity' =>    get_class($this->getEntity()),
                        'target.entity' =>  $this->getOption('entity')
                    )))->send();
                }

                $this->setOption('reference-field', $referenceField);
                $this->setOption('target-field', $targetField);
            }

            $referenceField = $this->getOption('reference-field');
            $targetField = $this->getOption('target-field');

            $reference = $mapping->getField($referenceField)->getOption('reference');
            $parent = $this->getEntity();
            $mapping->$referenceField = $parent->$reference;
            return $mapping->select();

            $mappingList = $mappingListResult->value;
            $entityList = [];
            foreach ($mappingList as $mapping) {
                $entityList[] = $mapping->$targetField;
            }

            return $entityList;
        }

        public function set($data) {
        }
    }