<?php
    namespace HK\OOD\Fields;

    use HK\OOD\Unknown as Unknown;

    class ManyToOneField extends BaseField {

        private $loadedEntity = null;

        public function __construct($options = []) {
            $this->setDefaultOptions(array(
                'nullable'
            ));

            $this->setRequiredOption('entity', 'reference');

            $this->init($options);

            $this->setValueValidator('\HK\OOD\Fields\Validator\ManyToOneValidator');
            $this->setValueConverter('\HK\OOD\Fields\Converter\ManyToOneConverter');

            if (!is_subclass_of($this->getOption('entity'), '\HK\OOD\Entities\BaseEntity')) {
                (new \HK\Error("ER0109", array(
                    'field' =>  get_class($this),
                    'entity' => $this->getOption('entity')
                )))->send();
            }
        }

        public function get() {
            if ($this->data != null && !$this->isUnknown()) {
                $entity = $this->getOption('entity');
                $field = $this->getOption('reference');

                if ($this->loadedEntity && $this->data == $this->loadedEntity->$field) {
                    return $this->loadedEntity;
                }

                $entity = new $entity();
                $entity->$field = $this->data;
                $result = $entity->select();

                if ($result && count($result) > 0) {
                    $this->loadedEntity = $result[0];
                    return $entity;
                } else {
                    $this->loadedEntity = null;
                    return new Unknown();
                }
            }

            return $this->data;
        }

        public function set($data) {
            if ($data instanceof \HK\OOD\Entities\BaseEntity) {
                $entity = $this->getOption('entity');
                if (!$data instanceof $entity) {
                    return new \HK\Error("ER0305", array(
                        'false.entity' =>  get_class($data),
                        'entity' => $this->getOption('entity')
                    ));
                }

                $field = $this->getOption('reference');
                $this->data = $data->$field;
            } else {
                $this->data = $data;
            }
        }
    }