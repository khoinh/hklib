<?php
    namespace HK\OOD\Fields;

    use HK\OOD\Unknown as Unknown;

    class OneToManyField extends BaseField {

        public function __construct($options = []) {
            $this->setDefaultOptions(array(
                'nullable'
            ));

            $this->setRequiredOption('entity');

            $this->init($options);

            if (!is_subclass_of($this->getOption('entity'), '\HK\OOD\Entities\BaseEntity')) {
                (new \HK\Error("ER0109", array(
                    'field' =>  get_class($this),
                    'entity' => $this->getOption('entity')
                )))->send();
            }
        }

        public function get() {
            $entity = $this->getOption('entity');
            $entity = new $entity();

            if (!$entity->getOption('reference-field')) {
                $fields = $entity->getFieldList();
                $referenceField = null;
                foreach ($fields as $name => $field) {
                    if ($field instanceof \HK\OOD\Fields\ManyToOneField &&
                        $field->getOption('entity') == '\\'.get_class($this->getEntity())) {
                        $referenceField = $name;
                        break;
                    }
                }

                if ($referenceField === null) {
                    (new \HK\Error("ER0116", array(
                        'otm.entity' => get_class($this->getEntity()),
                        'mto.entity' => $this->getOption('entity')
                    )))->send();
                }

                $this->setOption('reference-field', $referenceField);
            }

            $referenceField = $this->getOption('reference-field');
            $reference = $entity->getField($referenceField)->getOption('reference');

            $parent = $this->getEntity();
            $entity->$referenceField = $parent->$reference;

            return $entity->select()->value;
        }

        public function set($data) {
        }
    }