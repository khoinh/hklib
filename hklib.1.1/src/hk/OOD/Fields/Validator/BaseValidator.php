<?php
    namespace HK\OOD\Fields\Validator;

    use HK\OOD\Unknown as Unknown;
    use HK\Error as Error;

    class BaseValidator {

        public static function validate($data, $options) {
            if ($data instanceof Unknown)
                return new Error("ER0300");

            // Check for nullable
            $nullable = true;
            if (array_key_exists('nullable', $options))
                $nullable = $options['nullable'];

            if (array_key_exists('primary-key', $options))
                $nullable &= !$options['primary-key'];

            if ($nullable === false && $this->data == null)
                return new Error("ER0301");

            return true;
        }
    }