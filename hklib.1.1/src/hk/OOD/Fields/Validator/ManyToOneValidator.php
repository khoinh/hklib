<?php
    namespace HK\OOD\Fields\Validator;

    use HK\OOD\Unknown as Unknown;
    use HK\Error as Error;

    class ManyToOneValidator extends BaseValidator {

        public static function validate($data, $options) {
            $check = BaseValidator::validate($data, $options);
            if ($check !== true)
                return $check;

            // Create an temporary enity (for testing perpos only)
            $entity = new $options['entity']();

            // Check if reference field exist inside entity
            $reference = $options['reference'];
            $field = $entity->getField($reference);
            if ($field instanceof Error)
                return $field;

            // Set field data using this data
            $field->set($data);

            // Now validate this field value
            return $field->isValidValue();
        }
    }