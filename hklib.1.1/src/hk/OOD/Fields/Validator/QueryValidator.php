<?php
    namespace HK\OOD\Fields\Validator;

    use HK\Error as Error;

    class QueryValidator extends BaseValidator {

        public static $CONDITION_LIST;

        public static function init() {
            QueryValidator::$CONDITION_LIST = ['=', '>', '<', '>=', '<=', 'like', '!=', 'is'];
        }

        public static function validate($data, $options, $valueValidator = "\HK\ODD\Fields\Validator\BaseValidator") {
            if (gettype($data) == 'array') {
                foreach ($data as $condition => $value) {
                    if (gettype($condition) != 'integer' &&
                        !in_array($condition, QueryValidator::$CONDITION_LIST))
                        return new Error("ER0302", [
                            'conditions' => implode(', ', QueryValidator::$CONDITION_LIST)
                        ]);

                    $check = QueryValidator::validate($value, $options, $valueValidator);
                    if ($check !== true)
                        return $check;
                }
            } else {
                $result = call_user_func($valueValidator.'::validate', $data, $options);
                if ($result !== true)
                    return $result;
            }

            return true;
        }
    }

    QueryValidator::init();