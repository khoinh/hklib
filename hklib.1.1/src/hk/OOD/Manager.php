<?php
    namespace HK\OOD;

    use HK\Config as Config;
    use HK\DB\Manager as DBManager;

    class Manager {
        private static $handlers;
        private static $config;

        public static function init($config = []) {
            Manager::$config = $config;
            Config::insertConfigValue(Manager::$config);

            foreach (self::$config['managers'] as $name => $manager) {
                self::createHandler($name);
            }
        }

        private static function createHandler($name) {
            if (Manager::$handlers == null) {
                Manager::$handlers = [];
            }

            // Check if connection name exist in config list
            if (array_key_exists($name, Manager::$config['managers'])) {
                // Create new connection
                $config = Manager::$config['managers'][$name];
                return Manager::initHandler($config);
            }

            // If not exist, return null object
            return null;
        }

        private static function initHandler($config) {
            $handler = null;
            $connection = DBManager::connection($config['connection']);
            if (!$connection)
                return null;

            // Check Database type and create related connection
            switch ($connection->getType()) {
                case "MySQL":
                    // If this is an mysql connection
                    \HK\import("hk:OOD/MySQL");
                    $handler = new MySQL\ConnectionHandler();
                    $handler->setup($connection, $config);
                    break;
            }

            return $handler;
        }

        //--------------------------------------------------------------------------------------------------------------
        public static function getHandler($name) {
            if (!Manager::$handlers || !array_key_exists($name, Manager::$handlers))
                Manager::$handlers[$name] = Manager::createHandler($name);

            return Manager::$handlers[$name];
        }

        public static function clear() {
            Manager::$handlers = [];
        }

        public static function getNamingRule($name) {
            return Manager::$config['naming-rules'][$name];
        }
    }

    Manager::init(\HK\Context::oodConfig());