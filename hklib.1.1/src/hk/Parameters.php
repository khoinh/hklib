<?php
    namespace HK;

    use HK\Error;

    class Parameters {

        protected $data;

        public function __construct() {
            $this->data = [];
        }

        public function __set($name, $value) {
            if (array_key_exists($name, $this->data)) {
                (new Error('ER0007', array('name' => $name)))->send();
            }

            $this->data[$name] = $value;
        }

        public function __get($name) {
            if (!array_key_exists($name, $this->data)) {
                (new Error('ER0008', array('name' => $name)))->send();
            }

            return $this->data[$name];
        }

        public function __isset($name) {
            return array_key_exists($name, $this->data);
        }

        public function recursiveInit($data) {
            foreach ($data as $key => $value) {
                if (gettype($value) == 'array') {
                    $this->data[$key] = new Parameters();
                    $this->data[$key]->recursiveInit($value);
                } else {
                    $this->data[$key] = $value;
                }
            }
        }

        public function list() {
            return $this->data;
        }

        public function size() {
            return count($this->data);
        }
    }