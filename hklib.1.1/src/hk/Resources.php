<?php
    namespace HK;

    class Resources {

        private static $config;

        public static function init($config = []) {
            Resources::$config = $config;
        }

        public static function img($name, ...$hints) {
            return Resources::construct($name, "img", $hints);
        }

        public static function media($name, ...$hints) {
            return Resources::construct($name, "media", $hints);
        }

        public static function doc($name, ...$hints) {
            return Resources::construct($name, "doc", $hints);
        }

        public static function at($file) {
            list($container, $url) = explode(":", $file);
            $container = Config::get("repositories.res.$container");

            $root = Context::env("document_root");
            $url = "$root$container/$url";
            if (is_file($url))
                return new Resources($url);

            return null;
        }

        public static function all($name) {
            $result = [];

            list($containers, $package) = Resources::getRepositories($name);
            $root = Context::env("document_root");

            foreach ($containers as $name => $container) {
                $all = Util::findFiles("$root$container", $package);

                foreach ($all as $url)
                    $result[] = new Resources($url);
            }


            return $result;
        }

        private static function construct($name, $type, $hints) {
            // List all posible container
            list($containers, $package) = Resources::getRepositories($name);

            // loop through all container and find the best url fit
            foreach ($containers as $name => $container) {
                $root = Context::env("document_root");

                $all = [];
                if (Resources::$config['caching']['enable'])
                    $all = Resources::loadFilesFromCache("$name:$package", $type, $hints);
                else
                    $all = Util::findFiles("$root$container", $package);

                // File not exist in this container. Find in the next container
                if (count($all) <= 0)
                    continue;

                // Combine all hint to choose the best posible file url
                $urlHints = [];
                foreach ($hints as $hint) {
                    if (array_key_exists($hint, Resources::$config[$type]['hints']))
                        $urlHints[] = Resources::$config[$type]['hints'][$hint];
                }

                if (Context::language()) {
                    array_push($urlHints, Context::language());
                }

                array_push($urlHints, $type);

                // The higher the score the better.
                $scoredUrlList = [];
                foreach ($all as $url)
                    $scoredUrlList[] = array(
                        'score' => 0,
                        'url' => $url
                    );

                $bestUrl = $scoredUrlList[0]['url'];
                $max = 0;

                // Each hint exist in file path, that file score will increase to one
                foreach ($urlHints as $hint) {
                    foreach ($scoredUrlList as $item) {
                        if (strpos($item['url'], $hint."/") !== false) {
                            $item['score'] += 1;
                            if ($item['score'] > $max) {
                                $max = $item['score'];
                                $bestUrl = $item['url'];
                            }
                        }
                    }
                }

                // The highest score url will be choose
                return new Resources($bestUrl);
            }

            // No file found
            return null;
        }

        private static function loadFilesFromCache($name) {
            $cacheFileName = Resources::$config['caching']['file'];
            $allFile = [];

            list($container, $package) = explode(":", $name);
            $container = Config::get("repositories.res.$container");
            if ($container) {
                $root = Context::env("document_root");
                if (!is_file("$root$container$cacheFileName")) {
                    $allFile = Util::listFileAt("$root$container");
                    $result = file_put_contents("$root$container$cacheFileName", implode("\n", $allFile));

                    if ($result === false) {
                        (new Error("ER0001", array(
                            'file' =>       $cacheFileName,
                            'container' =>  $container
                        )))->send();
                    }
                } else {
                    $allFile = explode("\n", file_get_contents("$root$container$cacheFileName"));
                    if ($allFile === false) {
                        (new Error("ER0002", array(
                            'file' =>       $cacheFileName,
                            'container' =>  $container
                        )))->send();
                    }
                }
            }

            // Filter and only get url with name
            $allFile = preg_grep('/'.$package.'$/', $allFile);

            return $allFile;
        }

        private static function getRepositories($name) {
            if (strpos($name, ":") === false)
                return array(Config::get("repositories.res"), $name);

            list($containerName, $package) = explode(":", $name);
            $container = Config::get("repositories.res.$containerName");
            if ($container)
                $container = array($containerName => $container);
            else
                $container = [];

            return array($container, $package);
        }

        //--------------------------------------------------------------------------------------------------------------
        private $url;
        private $link;

        public function __construct($url) {
            $this->url = $url;

            $root = Context::env("document_root");
            $this->link = str_replace($root, "", $this->url);
        }

        public function link() {
            return $this->link;
        }

        public function path() {
            return $this->url;
        }

        public function ext() {
            return Util::getExtension($this->url);
        }

        public function asText() {
            return file_get_contents($this->url);
        }

        public function asFile($mode) {
            return fopen($this->url, $mode);
        }

        public function asIni($processSections = false, $scannerMode = INI_SCANNER_NORMAL) {
            return parse_ini_file($this->url, $processSections, $scannerMode);
        }

        public function asJson() {
            return json_decode($this->asText());
        }
    }