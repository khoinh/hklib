<?php
    namespace HK\Security;

    class Util {

        /**
         * Sanitizes the url (Like output from the PHP_SELF server variable)
         * This is to preventing cross site scripting attack
         * @param  [type] $url [description]
         * @return [type]      [description]
         */
        static public function sanitizeUrl($url) {
            if (empty($url)) {
                return $url;
            }

            $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);

            $strip = array('%0d', '%0a', '%0D', '%0A');
            $url = (string) $url;

            $count = 1;
            while ($count) {
                $url = str_replace($strip, '', $url, $count);
            }

            $url = str_replace(';//', '://', $url);

            $url = htmlentities($url);

            $url = str_replace('&amp;', '&#038;', $url);
            $url = str_replace("'", '&#039;', $url);

            if ($url[0] !== '/') {
                // We're only interested in relative links from $_SERVER['PHP_SELF']
                return '';
            } else {
                return $url;
            }
        }
    }