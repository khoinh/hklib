<?php
    namespace HK;

    class BaseError {

        protected $code;
        protected $msg;
        protected $tokens;
        protected $type;

        public function update($newTokens) {
            $this->tokens = array_merge($this->tokens, $newTokens);
        }

        public function getMsg() {
            return '('.$this->code.') '.Util::buildString($this->msg, $this->tokens);
        }

        public function send() {
            trigger_error($this->getMsg(), $this->type);
        }

        public function getCode() {
            return $this->code;
        }
    }

    class Error extends BaseError {

        private static $msgDict;

        public static function init() {
            Error::$msgDict = [];
            $errorMsgDictList = Config::get('message-dictionary.error');

            foreach ($errorMsgDictList as $dict) {
                $dict = Resources::doc($dict, 'config')->asIni();
                if ($dict)
                    Error::$msgDict = array_merge(Error::$msgDict, $dict);
            }
        }

        public function __construct($code, $tokens = []) {
            $this->type = E_USER_ERROR;

            if (!array_key_exists($code, Error::$msgDict))
                $this->msg = "System error, please contact your administrator.";
            else
                $this->msg = Error::$msgDict[$code];

            $this->tokens = $tokens;
            $this->code = $code;
        }
    }

    class Warning extends BaseError {

        private static $msgDict;

        public function init() {
            Warning::$msgDict = [];
            $errorMsgDictList = Config::get('message-dictionary.warning');

            foreach ($errorMsgDictList as $dict) {
                $dict = Resources::doc($dict, 'config')->asIni();
                if ($dict)
                    Warning::$msgDict = array_merge(Warning::$msgDict, $dict);
            }
        }

        public function __construct($code, $tokens = []) {
            $this->type = E_USER_WARNING;

            if (array_key_exists($code, Error::$msgDict))
                $this->msg = "($code) System warning.";
            else
                $this->msg = Error::$msgDict[$code];

            $this->tokens = $tokens;
            $this->code = $code;
        }
    }

    Error::init();
    Warning::init();