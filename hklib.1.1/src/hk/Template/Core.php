<?php
    namespace HK\Template;

    use HK\Config as Config;
    use HK\Error as Error;

    require "Component.php";

    // If shortcut is enable
    if (Config::get("shortcut.template"))
        require __DIR__."/GlobalShortcut.php";


    //--------------------------------------------------------------------------------------------------------------------------------------
    function present_template($template, $context) {
        if (empty($template)) {
            return;
        }

        // Get package name and url
        list($container, $package) = explode(":", $template);
        $context = $context;

        // Load container url from repository src
        $container = Config::get("repositories.template.$container");
        if ($container) {
            // Load Core.php file inside pacakge
            $root = \HK\Context::env("document_root");
            if (file_exists("$root$container$package.temp.php")) {
                $context_tmp = [];
                foreach ($context as $key => $value) {
                    // Store previous value into temporary variable
                    if (isset($$key)) {
                        $context_tmp[$key] = $$key;
                    }

                    // Check if value is an array, then translate it into parameters
                    if (gettype($value) == 'array') {
                        $$key = new \HK\Parameters();
                        $$key->recursiveInit($value);
                    } else {
                        // Set new value
                        $$key = $value;
                    }
                }

                // Require template
                require "$root$container$package.temp.php";

                // Reset value
                foreach ($context as $key => $value) {
                    if (isset($context_tmp[$key])) {
                        $$key = $context_tmp[$key];
                    } else {
                        unset($$key);
                    }
                }
            }
        } else {
            (new \HK\Error("ER0005", array(
                'template' =>   $name,
                'dir' =>        "$root$container$package"
            )))->send();
        }
    }

    function present($inputData, $context = null) {
        if (gettype($context) == 'array' && gettype($inputData) == 'str' && strpos($inputData, ':') !== false) {
            // Raw present template
            present_template($inputData, $context);
        } else if ($inputData instanceof Component) {
            // Build component
            $inputData->beforeBuild();
            present_template($inputData->getTemplate(), $inputData->getContext());
            $inputData->afterBuild();
        }

        // Loop through input list and present each of them
        else if ($inputData instanceof \HK\Parameters) {
            foreach ($inputData->list() as $value) {
                present($value);
            }
        } else if (gettype($inputData) == 'array') {
            foreach ($inputData as $value) {
                present($value);
            }
        }

        // Or just return them raw
        return $inputData;
    }