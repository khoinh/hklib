<?php
    function present($inputData, $context = []) {
        \HK\Template\present($inputData, $context);
    }

    function present_template($template, $context = []) {
        \HK\Template\present_template($template, $context);
    }