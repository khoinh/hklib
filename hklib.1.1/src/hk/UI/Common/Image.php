<?php
    namespace HK\UI\Common;

    use HK\UI\Component as Component;

    class Image extends Component {

        private $width = null;
        private $height = null;

        public function __construct() {
            parent::__construct('img');
        }

        public function setSource($source) {
            $this->setAttribute('src', $source);
            return $this;
        }

        public function getSource() {
            return $this->getAttribute('src');
        }

        public function setAlt($alt) {
            $this->setAttribute('alt', $alt);
            return $this;
        }

        public function getAlt() {
            return $this->getAttribute('alt');
        }

        public function setWidth($width) {
            if ($width !== null) {
                $this->width = $width;
                $this->setAttribute('width', $this->width."px");
            }

            return $this;
        }

        public function getWidth() {
            return $this->width;
        }

        public function setHeight($height) {
            if ($height !== null) {
                $this->height = $height;
                $this->setAttribute('height', $this->height."px");
            }

            return $this;
        }

        public function getHeight() {
            return $this->height;
        }
    }