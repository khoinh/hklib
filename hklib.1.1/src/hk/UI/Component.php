<?php
    namespace HK\UI;

    class Component extends \HK\Template\Component {

        protected $id;
        protected $attr;
        protected $tag;
        protected $content;
        protected $style;
        protected $script;
        protected $classes;
        protected $hasClosingTag;

        public function __construct($tag = 'div', $hasClosingTag = true) {
            parent::__construct();
            $this->setTag($tag, $hasClosingTag);
            $this->setId(uniqid('hk-ui-'));
            $this->content = null;
            $this->attr = [];
            $this->classes = [];

            $this->setTemplate('hk:UI/Component');
        }

        public function getId() {
            return $this->id;
        }

        public function setId($newId) {
            $this->id = $newId;
            return $this;
        }

        public function getTag() {
            return $this->tag;
        }

        public function setTag($tag, $hasClosingTag = true) {
            $this->tag = $tag;
            $this->hasClosingTag = $hasClosingTag;
            return $this;
        }

        public function getAttribute($name) {
            if (array_key_exists($name, $this->attr))
                return $this->attr[$name];
            return null;
        }

        public function setAttribute($name, $value) {
            $this->attr[$name] = $value;
            return $this;
        }

        public function getContent() {
            return $this->content;
        }

        public function setContent($content) {
            $this->content = $content;
            return $this;
        }

        public function addClass(...$nameList) {
            foreach ($nameList as $name) {
                // Check duplicate before add class
                if (($key = array_search($name, $this->classes)) === false) {
                    $this->classes[] = $name;
                }
            }

            return $this;
        }

        public function removeClass(...$nameList) {
            foreach ($nameList as $name) {
                if (($key = array_search($name, $this->classes)) !== false) {
                    unset($this->classes[$key]);
                }
            }

            return $this;
        }

        public function clearClass() {
            $this->classes = [];
            return $this;
        }

        public function hasClosingTag() {
            return $this->hasClosingTag;
        }

        public function beforeBuild() {
            parent::beforeBuild();

            // Update attributes
            $attr = [];
            foreach ($this->attr as $name => $value) {
                // Skip Class Attribute. As we use classes to store class name instead.
                if ($name == 'class') {
                    continue;
                }

                $attr[] = "$name='$value'";
            }

            if (!empty($this->classes))
                $attr[] = "class='".implode(' ', $this->classes)."'";

            $this->updateContext([
                'id' =>         $this->id,
                'tag' =>        $this->tag,
                'content' =>    $this->content,
                'attr' =>       implode(' ', $attr),
                'hasClosingTag' => $this->hasClosingTag
            ]);
        }
    }