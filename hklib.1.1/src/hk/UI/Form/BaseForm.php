<?php
    namespace HK\UI\Form;

    use HK\UI\Form\Fields\BaseField;
    use HK\UI\Form\Action\BaseAction;
    use HK\Parameter;
    use HK\Error;
    use HK\UI\Component;
    use HK\Parameters;
    use HK\HTTP\Request;
    use HK\UI\Form\Layout\StackLayout\Builder as StackLayoutBuilder;

    class BaseForm extends Component {

        private $handler;
        private $callback;
        private $layoutBuilder;
        private $fieldList = [];
        private $submitChecker = [];

        public function __construct() {
            parent::__construct();

            $this->setTag('form')
                ->setAttribute('name', 'form')
                ->setMethod('post')
                ->setAction($_SERVER['REQUEST_URI'])
                ->setActionHandler(new BaseHandler())
                ->setCallback(new \HK\Handler\Callback($this))
                ->setLayoutBuilder(new StackLayoutBuilder());

            $this->getCallback()->bind('failure', 'onFailure');
        }

        public function onFailure($parameters, $numError, $submitCode) {
            $this->getLayoutBuilder()->addMessage('error', "There are $numError error in form . Process terminate!");
        }

        public function getMethod() {
            return strtolower($this->getAttribute('method'));
        }

        public function setMethod($method = 'post') {
            $method = strtolower($method);
            if ($method == 'get') {
                $this->setAttribute('method', 'get');
            } else {
                $this->setAttribute('method', 'post');
            }

            return $this;
        }

        public function addMessage($type, $message) {
            $this->getLayoutBuilder()->addMessage($type, $message);
            return $this;
        }

        public function clearAllMesages() {
            $this->getLayoutBuilder()->clearAllMesages();
            return $this;
        }

        public function getAllMessages() {
            return $this->getLayoutBuilder()->getAllMessages();
        }

        public function setLayoutBuilder($layoutBuilder) {
            $this->layoutBuilder = $layoutBuilder;
            return $this;
        }

        public function getLayoutBuilder() {
            return $this->layoutBuilder;
        }

        public function getCallback() {
            return $this->callback;
        }

        public function setCallback($callback) {
            $this->callback = $callback;
            return $this;
        }

        public function setAction($action) {
            // To prevent cross site attack
            $action = \HK\Security\Util::sanitizeUrl($action);
            $this->setAttribute('action', $action);
            return $this;
        }

        public function getAction() {
            return $this->getAttribute('action', $action);
        }

        public function setActionHandler($handler) {
            if (!$handler instanceof BaseAction) {
                (new Error('ER0408'))->send();
            }

            $this->handler = $handler;
            $this->handler->setForm($this);
            return $this;
        }

        public function getActionHandler() {
            return $this->handler;
        }

        public function enableUpload() {
            $this->setAttribute('enctype', 'multipart/form-data');
            return $this;
        }

        public function buildLayout($builder) {
            // This is where you use layout builder to build field layout
        }

        public function getFieldList() {
            return $this->layoutBuilder->getFieldList();
        }

        public function beforeBuild() {
            $this->buildLayout($this->getLayoutBuilder());
            $this->process();
            $this->setContent($this->getLayoutBuilder()->build());

            parent::beforeBuild();
        }

        public function checkFormSubmited() {
            $fieldList = $this->layoutBuilder->getFieldList();
            $submitFieldName = [];
            foreach ($fieldList as $field) {
                if ($field instanceof \HK\UI\Form\Fields\SubmitAbleField &&
                    $field->getOption('as-submit')) {
                    // This field is an submit able field and it submit flag was enable
                    $submitFieldName[] = $field->getName();
                }
            }

            foreach ($submitFieldName as $submitCode) {
                if (Request::has($submitCode)) {
                    return $submitCode;
                }
            }

            return null;
        }

        public function process() {
            $this->handler->process($this->checkFormSubmited());
        }
    }