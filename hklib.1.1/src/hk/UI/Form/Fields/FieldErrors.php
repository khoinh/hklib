<?php
    namespace HK\UI\Form\Fields;

    class FieldErrors {

        private static $errorList;

        public static function init() {
            self::$errorList = [];
            $errorMsgDictList = \HK\Config::get('message-dictionary.form.error');

            foreach ($errorMsgDictList as $dict) {
                $dict = \HK\Resources::doc($dict, 'config')->asIni();
                if ($dict)
                    self::$errorList = array_merge(self::$errorList, $dict);
            }
        }

        public static function get($name) {
            if (!array_key_exists($name, self::$errorList)) {
                (new \HK\Error('ER0409', array('type' => $name)))->send();
            }

            return self::$errorList[$name];
        }

        public static function has($name) {
            return array_key_exists($name, self::$errorList);
        }
    }

    FieldErrors::init();