<?php
    namespace HK\UI\Form\Fields;

    use \HK\Util;
    use \HK\Error;

    class RadioField extends SelectField {

        public function __construct($name) {
            parent::__construct($name);

            $this->setTag('div')->addClass('radio-group-wrapper');
        }

        protected function createOptionItem($value, $name) {
            $option = (new \HK\UI\Component())
                ->setTag('input')
                ->setAttribute('type', 'radio')
                ->setAttribute('name', $this->getName())
                ->setAttribute('value', $value)
                ->setContent($name);

            $fieldValue = $this->getValue();
            if (($fieldValue !== null && $fieldValue == $value)) {
                $option->setAttribute('checked', 'checked');
            }

            return $option;
        }
    }