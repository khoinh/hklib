<?php
    namespace HK\UI\Form\Fields;

    use \HK\Util;
    use \HK\Error;

    class TextField extends BaseField {

        public function __construct($name) {
            parent::__construct($name);

            $this->updateValidOptions([
                'max-size' =>       null,
                'min-size' =>       null,
                'as-password' =>    false
            ]);
        }

        protected function checkOption($name, $value) {
            parent::checkOption($name, $value);

            if (($name == 'max-size' || $name == 'min-size') && !is_int($value) && $value !== null) {
                (new Error('ER0411', array('name' => $name)))->send();
            }
        }

        public function isEmpty() {
            return $this->data === null || strlen($this->data) <= 0;
        }

        protected function performValidate() {
            if (parent::performValidate()) {
                if (!Util::canBeString($this->getValue())) {
                    $this->setError('value-is-not-string');
                    return false;
                }

                $maxSize = $this->getOption('max-size');
                if ($maxSize && Util::strlen($this->getValue()) > $maxSize) {
                    $this->setError('max-value-size-exceed');
                    return false;
                }

                $minSize = $this->getOption('min-size');
                if ($minSize && Util::strlen($this->getValue()) < $minSize) {
                    $this->setError('min-value-size-not-meet');
                    return false;
                }

                return true;
            }

            return false;
        }

        public function beforeBuild() {
            if ($this->getOption('as-password')) {
                $this->setAttribute('type', 'password');
            } else {
                $this->setAttribute('type', 'text');
            }

            parent::beforeBuild();
        }
    }