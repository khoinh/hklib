<?php
    namespace HK\UI\Form\Fields;

    use \HK\Util;
    use \HK\Error;

    class UploadField extends BaseField {

        public function __construct($name) {
            parent::__construct($name);

            $this->addClass('upload-file');
        }

        public function updateContext() {
            $this->setAttribute('type', 'file');
            parent::updateContext();
        }
    }