<?php
    namespace HK\UI\Form\Layout\StackLayout;

    class Builder extends \HK\UI\Form\Layout\BaseBuilder {

        private $rowList;
        private $numRow;

        public function __construct() {
            parent::__construct();
            $this->rowList = [];
            $this->numRow = 0;
        }

        public function addRow() {
            $this->rowList[$this->numRow++] = [];
            return $this;
        }

        public function getNumRow() {
            return $this->numRow;
        }

        public function afterAddField($field) {
            if ($this->numRow > 0) {
                $this->rowList[$this->numRow - 1][] = $field;
            }
        }

        public function addComponent($component, $index = null) {
            if ($index !== null && array_key_exists($index, $this->rowList)) {
                $this->rowList[$index][] = $component;
            } else if ($this->numRow > 0) {
                $this->rowList[$this->numRow - 1][] = $component;
            }

            return $component;
        }

        public function buildLayout() {
            $newLayout = $this->buildMessageLayout();

            foreach ($this->rowList as $rowIndex => $row) {
                $rowComponent = new \HK\UI\Component();
                $rowComponent->setTag('div')
                    ->addClass('form-row');

                $rowLayout = [];
                foreach ($row as $column) {
                    $rowLayout[] = $column;
                }

                $rowComponent->setContent($rowLayout);
                $newLayout->add($rowIndex, $rowComponent);
            }

            $this->setLayout($newLayout);
        }

        private function buildMessageLayout() {
            $messageList = [];
            $messages = $this->getAllMessages();
            $messageCount = 0;
            foreach ($messages as $type => $msgList) {
                $messageDialog = (new \HK\UI\Component())
                    ->addClass('msg-dialog-type-'.$type);

                $messageDialogContent = (new \HK\UI\Common\ListView())
                    ->disableMarker();
                foreach ($msgList as $key => $msg) {
                    if (empty($msg)) {
                        continue;
                    }

                    $messageCount++;
                    $messageDialogContent->add($key)->setContent($msg);
                }

                $messageDialog->setContent($messageDialogContent);
                $messageList[$type] = $messageDialog;
            }

            if ($messageCount > 0) {
                $messageWrapper = (new \HK\UI\Component())
                    ->addClass('form-msg-wrapper')
                    ->setContent($messageList);
                $container->add('form-msg-list', $messageWrapper);
            }

            return $container;
        }
    }