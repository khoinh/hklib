<?php
    namespace HK;

    class Util {
        /**
         * This method is used to dynamically build string from source.
         * If source string contain %tokens, it will find and replace it
         * with corresponding token value in token list.
         * - Example if source string is "Say %token1 World". Tokens list is
         * ('token1' => 'Hello') then output result would be "Say Hello World".
         * @param  String   $str:       Source string
         * @param  Array    $tokens:    A List of tokens
         * @return String   String result build from source and token
         */
        public static function buildString($str, $tokens) {
            $result = "";
            $key = null;

            $str = str_replace("\\\\", "#001#", $str);
            $str = str_replace("\%", "#002#", $str);

            $strlen = strlen($str);
            for ($index = 0; $index < $strlen; $index++) {
                $char = $str[$index];
                if ($char == '%') {
                    $key = "";
                } else if ($key !== null) {
                    if (($char >= 'a' && $char <= 'z') ||
                        ($char >= 'A' && $char <= 'Z') ||
                        ($char >= '0' && $char <= '9') ||
                        $char == '.' || $char == '_') {
                        $key .= $char;
                    } else {
                        // Remove '.' character at the end of key
                        if (substr($key, -1) == '.')
                            $key = substr($key, 0, -1);

                        $result = Util::applyToken($result, $key, $tokens).$char;
                        $key = null;
                    }

                } else {
                    $result .= $char;
                }
            }

            if ($key !== null) {
                $result = Util::applyToken($result, $key, $tokens);
            }

            $result = str_replace("#001#", "\\\\", $result);
            $result = str_replace("#002#", "%", $result);

            return $result;
        }

        public static function isStringInteger($input) {
            return intval($input).'' === $input.'';
        }

        public static function getClass($object) {
            $result = explode('\\', get_class($object));
            return end($result);
        }

        public static function getExtension($file) {
            $result = explode(".", $file);
            return end($result);
        }

        public static function findFiles($url, $name) {
            $result = [];

            $dirList = scandir($url);

            foreach ($dirList as $dir) {
                if ($dir == "." || $dir == "..")
                    continue;

                if (is_file("$url/$dir")) {
                    if ($dir ==  $name)
                        $result[] = $url.'/'.$dir;
                } else
                    $result = array_merge($result, Util::findFiles("$url/$dir", $name));
            }

            return $result;
        }

        public static function listFileAt($url) {
            $result = [];

            $dirList = scandir($url);

            foreach ($dirList as $dir) {
                if ($dir == "." || $dir == "..")
                    continue;

                if (is_file("$url/$dir"))
                    $result[] = $url.'/'.$dir;
                else
                    $result = array_merge($result, Util::listFileAt("$url/$dir"));
            }

            return $result;
        }

        public static function strlen($string) {
            return mb_strlen($string, Config::get('default.charset'));
        }

        public static function canBeString($data) {
            return $data === null || is_scalar($data) || is_callable([$data, '__toString']);
        }

        public static function redirect($url) {
            header("Location: $url");
            die();
        }

        public static function mergeArray($base = [], $update = []) {
            foreach ($update as $key => $value) {
                // Append sequence key value
                if (is_int($key)) {
                    $base[] = $value;
                }

                // If key already exist in base and both base and update's value is an array
                else if (array_key_exists($key, $base) &&
                    gettype($base[$key]) == 'array' &&
                    gettype($value) == 'array') {
                    // update base recursively
                    $base[$key] = self::mergeArray($base[$key], $value);
                }

                // Or else, update base with new value
                else {
                    $base[$key] = $value;
                }
            }

            return $base;
        }

        //--------------------------------------------------------------------------------------------------------------
        private static function applyToken($result, $key, $tokens) {
            if (Util::isStringInteger($key)) {
                $key = intval($key);
            }

            if (array_key_exists($key, $tokens)) {
                $result .= $tokens[$key];
            } else {
                $result .= "%$key";
            }

            return $result;
        }
    }