<?php
    require "HK/Core.php";

    class HKFrameworkLoader {

        public static function load($mainConfig = [], $loggerConfig = [], $resourcesConfig = [], $databaseConfig = [], $oodConfig = []) {
            // Main configuration
            $defaultConfig = require __DIR__."/Config/main.php";
            $mainConfig = HK\Util::mergeArray($defaultConfig, $mainConfig);

            require __DIR__."/HK/Config.php";
            HK\Config::init($mainConfig);

            // Application context (Default language, charset, env...)
            require __DIR__."/HK/Context.php";

            // Save database configuration
            $defaultConfig = require __DIR__."/Config/database.php";
            $databaseConfig = HK\Util::mergeArray($defaultConfig, $databaseConfig);
            HK\Context::databaseConfig($databaseConfig);

            // Save OOD Configuration
            $defaultConfig = require __DIR__."/Config/ood.php";
            $oodConfig = HK\Util::mergeArray($defaultConfig, $oodConfig);
            HK\Context::oodConfig($oodConfig);

            // Resources configuration
            $defaultConfig = require __DIR__."/Config/resources.php";
            $resourcesConfig = HK\Util::mergeArray($defaultConfig, $resourcesConfig);

            require __DIR__."/HK/Resources.php";
            HK\Resources::init($resourcesConfig);

            // Additional library
            require __DIR__."/HK/Parameters.php";

            // Logger and debug code
            require __DIR__."/HK/SystemError.php";

            // Main function
            require __DIR__."/HK/Main.php";

            // Import default package
            HK\import(  "hk:Debug",
                        "hk:Template");

            $defaultConfig = require __DIR__."/Config/logger.php";
            $loggerConfig = HK\Util::mergeArray($defaultConfig, $loggerConfig);
            HK\Debug\Logger::init($loggerConfig);

            // Auto load source code
            call_user_func_array("\HK\import", HK\Config::get("autoload.src"));
            call_user_func_array("\HK\Context::addJavaScripts", HK\Config::get("autoload.script"));

            // Load default theme
            HK\Context::addThemes(HK\Config::get("application.default.themes"));

            // Setup default time zone
            date_default_timezone_set(HK\Config::get('default.timezone'));
        }
    }