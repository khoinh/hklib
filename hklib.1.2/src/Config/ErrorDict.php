<?php
// This is used to translate error code to a message. Can be used to hide error message (Display error code only)
return [
    'ER000000' => 'System error! Please contact the administrator.'
];