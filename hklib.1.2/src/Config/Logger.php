<?php
return [
    // Logger name
    'default' => [
        // Use this to determine which log will show the message.
        // The closer the source file to the log command itself, the higher the priority.
        'source'=> '',

        // Log output location
        'output'=> __DIR__.'/../../logs/default.log',

        // Log output message
        // %1 is the date log number
        // %2 is the date log output
        // %3 is the url which log happen
        // %4 is the level of the message (Error, Warning, Info)
        // %5 is the message itself
        'pattern'=> '%1$04d. %2$s (%3$s) %4$s > %5$s',

        // Date time format
        'date_format'=> 'd/m/Y H:i:s',

        // Maximum number of log in one file
        'max'=> 1000,

        // Maximum number of log backup file
        'backup'=> 1,

        // There are three level: debug, release, stable.
        // debug level output all message including error, warning and info message.
        // release level output only error and warning message.
        // stable level only output error message.
        'level' => 'debug'
    ],
];