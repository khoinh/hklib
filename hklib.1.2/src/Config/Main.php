<?php
return [
    // List of package in the application. Us this to quickly import a directory or a file in the package
    // Example: \HK\import('hk:DB') will import DB directory inside hk package
    'packages' => [
        'hk' => __DIR__.'/../HK',                   // Main package
        'hk-template' => __DIR__.'/../Template',    // Included default template
        'hk-vendor' => __DIR__.'/../Vendor',        // Possible vendor packages
    ],

    // Defined how to import packages and when
    'autoload' => [
        'startup' => ['hk:Main'],   // A list of package that will always be imported when the framework load

        // A chain autoload is autoloading with condition
        // Basically it is "if package A is import then import package B"
        // It use regex match to determent the package
        'chain' => [
            '^hk:JOD$' =>       ['hk:DB'],
            '^hk:OOD$' =>       ['hk:DB'],
            '^hk:UI($|\/.*)' => ['hk:Template', 'hk:UI/Common']
        ]
    ],

    'default_context' => require(__DIR__.'/DefaultContext.php'),

    // Default system format
    'formats' => [
        'date' => 'd/m/Y',
        'datetime' => 'd/m/Y H:i:s',
    ],

    // 'debugger' and 'logger' setting only matter if you import hk:Debug package
    'debugger' => [
        // If mode is display, error will only appear in the website
        // If mode is log, then error will be saved into log file only
        // If mode is log_and_display, then error will be appear on the web and save in log file
        'mode' => 'log_and_display',

        // An error code dictionary (See 'ErrorDict.php' in Config folder)
        'error_dict' => require(__DIR__.'/ErrorDict.php'),

        // If display mode is active, we need a template to generate error page
        // Here we have a default template for you
        'templates' => [
            'error' => 'hk-template:Debug/error.temp.php',          // When an error is triggered
            'exception' => 'hk-template:Debug/exception.temp.php'   // When an exception was throwed
        ]
    ],

    'logger' => [
        'enable' => true,                           // Enable logger here
        'configs' => require(__DIR__.'/Logger.php') // This contain a list of logger's config to be used (See 'Logger.php' in Config folder)
    ],

    'database' => [
        'handlers' => [],                               // List of database handlers
        'configs' => require(__DIR__.'/Database.php'),  // List of database setting and how to access them (See 'DataBase.php' in Config folder)
    ],

    // Object Oriented Database (An Custom ORM system)
    'ood' => [
        'safe_delete' => true,                      // If true, it will prevent delete all query (DELETE FROM table)
        'safe_update' => false,                     // If true, it will prevent update all data of the table (UPDATE table SET ... WHERE true)
        'configs' => require(__DIR__.'/OOD.php'),   // List of each ood handler, contain the database handler, entity namespace, and more (See 'OOD.php' in Config folder)
        'query_builders' => [                       // List of query builders
            'hk:OOD/QueryBuilders/MySQL'
        ]
    ],

    // Json Oriented Databse (An simple array base database system).
    'jod' => [
        'safe_delete' => true,                      // If true, it will prevent delete all query (DELETE FROM table)
        'safe_update' => false,                     // If true, it will prevent update all data of the table (UPDATE table SET ... WHERE true)
        'configs' => require(__DIR__.'/JOD.php'),   // List of each jod handler (See 'JOD.php' in Config folder)
        'query_builders' => [                       // List of query builders
            'hk:JOD/QueryBuilders/MySQL'
        ]
    ],

    // Template configuration
    'template' => [
        'context' => [
            'assetRoot' => '',                      // Root of application public asset (Css, Javascript files,...)
            'hkAssetRoot' => __DIR__.'/../Assets',  // Default asset root of hk library
            'assetVersion' => '01'                  // Asset version (Use to add after asset file link, update it to renew cache)
        ]
    ],
];