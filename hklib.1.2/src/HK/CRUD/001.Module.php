<?php
namespace HK\CRUD;

class Module extends \HK\Main\Callback {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    protected $createHandler;
    protected $updateHandler;
    protected $deleteHandler;
    protected $readHandler;


    ////////////
    // METHOD
    public function setCreateHandler($createHandler) {
        $this->createHandler = $createHandler;
        return $this;
    }

    public function setUpdateHandler($updateHandler) {
        $this->updateHandler = $updateHandler;
        return $this;
    }

    public function setDeleteHandler($deleteHandler) {
        $this->deleteHandler = $deleteHandler;
        return $this;
    }

    public function setReadHandler($readHandler) {
        $this->readHandler = $readHandler;
        return $this;
    }

    public function getCreateHandler() {
        return $this->createHandler;
    }

    public function getUpdateHandler() {
        return $this->updateHandler;
    }

    public function getDeleteHandler() {
        return $this->deleteHandler;
    }

    public function getReadHandler() {
        return $this->readHandler;
    }

    public function onCreate($args) {
        return $this->createHandler->execute($args);
    }

    public function onUpdate($args) {
        return $this->updateHandler->execute($args);
    }

    public function onDelete($args) {
        return $this->deleteHandler->execute($args);
    }

    public function onRead($args) {
        return $this->readHandler->execute($args);
    }
}