<?php
namespace HK\CRUD;

abstract class Handler extends \HK\Main\Callback {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    protected $module;
    protected $validator;


    ////////////
    // METHOD
    function __construct($module) {
        $this->module = $module;
        $this->validator = null;
    }

    public function setModule($module) {
        $this->module = $module;
        return $this;
    }

    public function getModule() {
        return $this->module;
    }

    public function getValidator() {
        return $this->validator;
    }

    public function setValidator($validator) {
        $this->validator = $validator;
        return $this;
    }

    public function beforeExecute($args) {
        $validate = true;
        if ($this->validator) {
            $validate = $this->validator->validate($args);
        }

        return [
            'errors' => $validate === true ? null : $validate,
            'value' =>  $args
        ];
    }


    /////////////////////
    // ABSTRACT METHOD
    public abstract function onExecute($args);
}