<?php
namespace HK\CRUD;

abstract class Validator extends \HK\Main\Callback {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    /////////////////////
    // ABSTRACT METHOD
    public abstract function onValidate($args);
}