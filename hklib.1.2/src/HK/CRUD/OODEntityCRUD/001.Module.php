<?php
namespace HK\CRUD\OODEntityCRUD;

class Module extends \HK\CRUD\Module {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    protected $entityClassName;


    ////////////
    // METHOD
    function __construct($entity) {
        $this->createHandler = new CreateHandler($this);
        $this->updateHandler = new UpdateHandler($this);
        $this->deleteHandler = new DeleteHandler($this);
        $this->readHandler = new ReadHandler($this);

        if (gettype($entity) == 'string') {
            $this->entityClassName = $entity;
        } else {
            $this->entityClassName = get_class($entity);
        }
    }

    public function createEntity($args) {
        $entity = new $entityClassName();
        if ($args) {
            foreach ($args as $key => $value) {
                if ($entity->hasField($key)) {
                    $entity->$key = $value;
                }
            }
        }

        return $entity;
    }

    public function beforeCreate($args) {
        return $this->createEntity($args);
    }

    public function beforeDelete($args) {
        return $this->createEntity($args);
    }

    public function beforeRead($args) {
        return $this->createEntity($args);
    }

    public function beforeUpdate($args) {
        return [
            'from' => $this->createEntity($args['from']),
            'to' => $this->createEntity($args['to']),
        ];
    }
}