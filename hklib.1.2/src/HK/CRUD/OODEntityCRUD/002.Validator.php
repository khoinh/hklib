<?php
namespace HK\CRUD\OODEntityCRUD;

class Validator extends \HK\CRUD\Validator {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    /////////////////////
    // ABSTRACT METHOD
    public function onValidate($entity) {
        if ($entity->isUnknown()) {
            return false;
        }

        $fields = $entity->getFields();
        $errors = [];
        foreach ($fields as $name => $field) {
            $errors[$name] = [];
            if ($field->getOption('required') && ($field->isUnknown() || empty($field->getValue()))) {
                $errors[$name][] = 'Empty required field';
            }

            if ($field->getOption('not-null') && !$field->isUnknown()&& $field->getValue() === null) {
                $errors[$name][] = 'Field not nullable';
            }
        }

        return empty($errors) ? true : $errors;
    }
}