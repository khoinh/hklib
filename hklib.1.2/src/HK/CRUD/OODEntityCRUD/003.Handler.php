<?php
namespace HK\CRUD\OODEntityCRUD;

class Handler extends \HK\CRUD\Handler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    public function __construct($module) {
        parent::__construct($module);
        $this->setValidator(new Validator());
    }

    public function afterExecute($args) {

    }
}