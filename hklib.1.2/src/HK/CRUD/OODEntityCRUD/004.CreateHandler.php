<?php
namespace HK\CRUD\OODEntityCRUD;

class CreateHandler extends Handler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    public function onExecute($args) {
        $entity = $args['value'];
        $errors = $args['errors'];
        if ($errors === null) {
            $entity = $entity->insert();
        }

        return [
            'errors' => $errors,
            'value' =>  $entity
        ];
    }
}