<?php
namespace HK\CRUD\OODEntityCRUD;

class UpdateHandler extends Handler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    public function beforeExecute($args) {
        $validate = parent::beforeExecute($args['to']);
        return [
            'errors' => $validate['errors'],
            'value' => $args
        ];
    }

    public function onExecute($entity) {
        $from = $args['value']['from'];
        $to = $args['value']['to'];
        $errors = $args['errors'];
        if ($errors === null) {
            $to = $from->update($to);
        }

        return [
            'errors' => $errors,
            'value' =>  $to
        ];
    }
}