<?php
namespace HK\CRUD\OODEntityCRUD;

class DeleteHandler extends Handler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct($module) {
        parent::__construct($module);
        $this->setValidator(null);
    }

    public function onExecute($args) {
        $entity = $args['value'];
        $errors = $args['errors'];
        if ($errors === null) {
            $entity->delete();
        }

        return [
            'errors' => $errors,
            'value' =>  $entity
        ];
    }
}