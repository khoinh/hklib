<?php
namespace HK\CRUD\OODEntityCRUD;

class ReadHandler extends DeleteHandler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    public function onExecute($args) {
        $entity = $args['value'];
        $errors = $args['errors'];
        $list = [];
        if ($errors === null) {
            $list = $entity->read();
        }

        return [
            'errors' => $errors,
            'value' =>  $entity,
            'list' =>   $list
        ];
    }
}