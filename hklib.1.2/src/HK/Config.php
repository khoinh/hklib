<?php
namespace HK;

class Config {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private static $main;


    ////////////
    // METHOD
    public static function init($config = []) {
        self::$main = \HK\Util::deepMergeArray(include(__DIR__.'/../Config/Main.php'), $config);
        \HK\Context::init();
    }

    public static function get(...$keys) {
        $current = self::$main;
        foreach ($keys as $key) {
            if (isset($current[$key])) {
                $current = $current[$key];
            } else {
                return null;
            }
        }

        return $current;
    }
}