<?php
namespace HK;

class Context {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private static $variables;


    ////////////
    // METHOD
    public static function init() {
        self::$variables = \HK\Config::get('default_context');

        // Autoload chain package cache
        self::set('hk.chain.packages.pattern.cache', \HK\Config::get('autoload', 'chain'));

        // Create default global context for template
        self::set('hk.template.global_context', \HK\Config::get('template', 'context'));
    }

    public static function get($key, $default = null) {
        if (!isset(self::$variables[$key])) {
            self::$variables[$key] = $default;
        }

        return self::$variables[$key];
    }

    public static function &getRef($key, $default = null) {
        if (!isset(self::$variables[$key])) {
            self::$variables[$key] = $default;
        }

        return self::$variables[$key];
    }

    public static function set($key, $value) {
        self::$variables[$key] = $value;
    }

    public static function print() {
        var_dump(self::$variables);
    }
}