<?php
namespace HK;

/**
 * THIS IS THE MAIN ENTRANCE TO THE LIBRARY.
 * To use this library, just need to required this file and use init to setup the library
 */

require_once 'Util.php';
require_once 'Config.php';
require_once 'Constant.php';
require_once 'Context.php';

/**
 * Use this function to import (Required once) a file, a folder, or a package
 * @param  [type] $packages [description]
 * @return [type]           [description]
 */
function import(...$packages) {
    $chainPackagesPattern = &\HK\Context::getRef('hk.chain.packages.pattern.cache');
    $importedPackages = &\HK\Context::getRef('hk.imported.packages');
    foreach ($packages as $package) {
        // Check if this package is imported
        if (in_array($package, $importedPackages)) {
            continue;
        }

        // Add this package to imported list
        $importedPackages[] = $package;

        // First import chain packages
        foreach ($chainPackagesPattern as $pattern => $chainPackages) {
            // If package match the pattern
            if (preg_match("/$pattern/", $package)) {
                // Add pattern to imported chain packages
                unset($chainPackagesPattern[$pattern]);

                // Then load each required packages
                foreach ($chainPackages as $chainPackage) {
                    import($chainPackage);
                }
            }
        }

        // Finnaly, import the package
        $result = Util::getPackagesUrl($package);
        foreach ($result as $url) {
            require_once($url);
        }
    }
}

function init($config = []) {
    // Initialize configuration
    \HK\Config::init($config);

    // Load preload packages
    $preload = \HK\Config::get('autoload', 'startup');
    foreach ($preload as $dir) {
        import($dir);
    }
}