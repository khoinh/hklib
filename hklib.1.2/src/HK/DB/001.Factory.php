<?php
namespace HK\DB;

abstract class Factory {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    /////////////////////
    // ABSTRACT METHOD

    abstract public function createDatabaseHandler($setting);
}