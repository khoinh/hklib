<?php
namespace HK\DB;

abstract class Handler {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private static $settings;
    private static $databaseList;
    private static $databaseFactoryList;


    ////////////
    // METHOD
    public static function init() {
        // Import all database handler
        $dbHandlers = \HK\Config::get('database', 'handlers');
        foreach ($dbHandlers as $dir) {
            \HK\import($dir);
        }

        // Setup
        self::$settings = \HK\Config::get('database', 'configs');
        self::$databaseList = [];

        // Use database factory to create database handler
        foreach (self::$settings as $name => $setting) {
            if (!isset(self::$databaseFactoryList[$setting['type']])) {
                throw new \Exception("Error when init database. Unknown database type[".$setting['type']."]");
            } else {
                $factory = self::$databaseFactoryList[$setting['type']];
                self::$databaseList[$name] = $factory->createDatabaseHandler($setting);
            }
        }
    }

    public static function addDatabaseFactory($name, $factory) {
        self::$databaseFactoryList[$name] = $factory;
    }

    public static function get($name) {
        if (isset(self::$databaseList[$name])) {
            return self::$databaseList[$name];
        }

        throw new \Exception("Unable to find database handler name: $name");
    }

    public static function has($name) {
        return isset(self::$databaseList[$name]);
    }

    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////////////
    // ABSRACT METHOD
    abstract public function connect($setting);
    abstract public function execute($args);
    abstract public function close();
    abstract public function getType();
    abstract public function isReady();
}

Handler::init();