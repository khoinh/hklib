<?php
namespace HK\DB;

class Result {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    protected $isSuccess;
    protected $error;
    protected $value;


    ////////////
    // METHOD
    function __construct($value) {
        if (isset($value)) {
            $this->isSuccess = true;
            $this->value = $value;
            return;
        }

        $this->isSuccess = false;
        $this->value = null;
        $this->error = null;
    }

    public function isSuccess() {
        return $this->isSuccess;
    }

    public function value() {
        return $this->value;
    }

    public function setError($message) {
        $this->error = $message;
        return $this;
    }

    public function getError() {
        return $this->error;
    }
}