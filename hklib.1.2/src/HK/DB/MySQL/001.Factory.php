<?php
namespace HK\DB\MySQL;

class Factory extends \HK\DB\Factory {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    public static function init() {
        \HK\DB\Handler::addDatabaseFactory('mysql', new Factory());
    }

    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct() {
    }

    public function createDatabaseHandler($settings) {
        return (new Handler())->connect($settings);
    }
}

Factory::init();