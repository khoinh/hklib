<?php
namespace HK\DB\MySQL;

use \HK\DB\Result as Result;

class Handler extends \HK\DB\Handler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private $connection;
    private $isReady;
    private $type;


    ////////////
    // METHOD
    function __construct() {
        $this->isReady = false;
    }

    function __destruct() {
        $this->close();
    }

    public function connect($settings) {
        $this->type = $settings['type'];
        $this->connection = new \mysqli($settings['server'], $settings['username'], $settings['password'], $settings['database'], $settings['port'], $settings['socket']);
        if ($this->connection->connect_error) {
            throw new \Exception('Error: Unable to create database handler from settings >> '.print_r($settings, true));
        }

        $this->isReady = true;
        return $this;
    }

    public function close() {
        if ($this->connection && $this->isReady) {
            $this->connection->close();
        }
    }

    public function isReady() {
        return $this->isReady && $this->connection;
    }

    public function execute($args) {
        if (!$this->isReady()) {
            throw new \Exception('Error: Call query on an invalid database connection');
        }

        $result = $this->connection->query($args);
        if ($result === TRUE) {
            return new Result($this->connection->insert_id);
        } else if ($result === FALSE) {
            return (new Result())->setError($this->connection->error);
        } else {
            return new Result($result);
        }
    }

    public function getType() {
        return $this->type;
    }
}