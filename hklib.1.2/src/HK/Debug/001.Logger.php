<?php
namespace HK\Debug;

class Logger {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private static $isEnable;
    private static $settingList;
    private static $loggerList;
    private static $first;



    ////////////
    // METHOD
    public static function init($configs = []) {
        // Create a list of logger setting. Which will be used to create the logger itself.
        self::$isEnable = \HK\Config::get('logger', 'enable');
        $settingList = \HK\Config::get('logger', 'configs');

        foreach ($settingList as $name => $setting) {
            self::$settingList[$setting['source']] = [
                'name' => $name,
                'setting' => $setting
            ];

            if (!self::$first) {
                self::$first = $setting['source'];
            }
        }
    }

    public static function get($location) {
        // Check logger for the location existed
        if (isset(self::$loggerList[$location])) {
            return self::$loggerList[$location];
        }

        // Create logger using setting that match location
        $result = null;
        $defaultLogger = null;
        foreach (self::$settingList as $source => $setting) {
            if (empty($source)) {
                $defaultLogger = new Logger($setting['name'], $setting['setting'], $location);
            } else {
                $check = explode($source, $location);
                if ($check != "" && (count($check) != 2 || $check[0] != "")) {
                    continue;
                } else if (!$result || count($source) > count($result->getSourceDir())) {
                    $result = new Logger($setting['name'], $setting['setting'], $location);
                    break;
                }
            }
        }

        if ($result) {
            return $result;
        }

        // Use logger with empty source location as default logger
        if ($defaultLogger) {
            return $defaultLogger;
        }

        // Cant get any logger? Return the first logger.
        if (self::$first) {
            $setting = self::$settingList[self::$first];
            self::$loggerList[$location] = new Logger($setting['name'], $setting['setting'], $location);
            return self::$loggerList[$location];
        }

        // Return an disabled logger version. Which wont be able to log anything.
        return new Logger();
    }

    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private $setting;
    private $name;
    private $numLog;
    private $outputFile;
    private $location;


    //////////////
    // METHOD

    function __construct($name, $setting, $location) {
        if (!$setting) {
            $this->setting = null;
            return;
        }

        $this->location = $location;
        $this->setting = $setting;
        $this->name = $name;
        $this->getOutputFile();
    }

    function __destruct() {
        if ($this->outputFile) {
            fclose($this->outputFile);
        }
    }

    public function getSourceDir() {
        return $this->setting['source'];
    }

    public function error($message, ...$tokens) {
        $this->outputMessage(2, $message, $tokens);
    }

    public function warning($message, ...$tokens) {
        $this->outputMessage(1, $message, $tokens);
    }

    public function info($message, ...$tokens) {
        $this->outputMessage(0, $message, $tokens);
    }

    private function outputMessage($level, $message, $tokens) {
        if (!$this->isEnable($level)) {
            return;
        }

        $outputFile = $this->getOutputFile();
        if ($outputFile) {
            $message = $this->prepareMessage($level, $message, $tokens);
            fwrite($outputFile, "$message\n");
        }
    }

    private function getOutputFile() {
        if ($this->outputFile && $this->numLog < $this->setting['max']) {
            return $this->outputFile;
        } else {
            // Reset output file
            $this->outputFile = null;
            $this->numLog = 0;

            // Attemping to create new output file
            $url = $this->setting['output'];
            $this->outputFile = fopen($url, 'c+')
                or die("Unable to create log file for logger(".$this->name.")");

            // When successfully get output file
            if ($this->outputFile) {
                // Check if this output file has suppass maximum number of log lines
                // 1. Count number of log in output file
                while (!feof($this->outputFile)) {
                    fgets($this->outputFile);
                    $this->numLog++;
                }

                // 2. Check against maximum number of log lines
                if ($this->numLog >= $this->setting['max']) {
                    // If exceeds maximum number of log, backup the file and create a new empty one
                    fclose($this->outputFile);

                    // Create and save backup
                    $this->createBackup();

                    // Return empty output file
                    $this->outputFile = fopen($url, 'w+')
                        or die("Unable to create log file for logger(".$this->name.")");
                }
            }

            // Return output file
            return $this->outputFile;
        }
    }

    private function isEnable($level) {
        if (self::$isEnable && $this->setting) {
            switch ($this->setting['level']) {
                case 'debug':
                    return true;

                case 'release':
                    return $level > 0;

                case 'stable':
                    return $level > 1;
            }
        }

        return false;
    }

    private function createBackup() {
        $numBackup = $this->setting['backup'];
        $url = $this->setting['output'];

        // Check for number of backup is larger than 0.
        // If not then we simply cleanup current output file content.
        if ($numBackup > 0) {
            // Check for current backup file existance
            $index = 1;
            while ($index <= $numBackup) {
                if (!file_exists("$url.bk.$index")) {
                    break;
                }
                $index++;
            }

            // If index exceed max number of backup file. This mean that we should remove
            // the oldest backup and create new one.
            if ($index > $numBackup) {
                // 1. Remove oldest backup file
                unlink("$url.bk.1");

                // 2. Rename all old backup file to index one
                for ($index = 2; $index <= $numBackup; $index++) {
                    $prev = $index - 1;
                    rename("$url.bk.$index", "$url.bk.$prev");
                }

                // 3. Reset current index to the last backup index
                $index = $numBackup;
            }

            // Now rename current output file to backup file
            rename("$url", "$url.bk.$index");
        }
    }

    private function prepareMessage($level, $message, $tokens) {
        // Combine message with tokens
        $message = call_user_func_array('sprintf', array_merge([$message], $tokens));

        // Build log pattern
        $pattern = $this->setting['pattern'];
        $level = ($level == 0 ? 'INFO' : ($level == 1 ? 'WARNING' : 'ERROR'));
        $from = $this->location;
        $log = $this->numLog++;

        // 3. Update current time
        $dateFormat = 'd/m/y H:i:s';
        if (array_key_exists('date_format', $this->setting)) {
            $dateFormat = $this->setting['date_format'];
        }

        $date = new \DateTime();
        $date = $date->format($dateFormat);

        return sprintf($pattern, $log, $date, $from, $level, $message);
    }
}

Logger::init();