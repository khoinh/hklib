<?php
namespace HK\Debug;

Class ErrorHandler {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private static $handler;


    ////////////
    // METHOD
    public static function init() {
        // Enable all error reporting
        error_reporting(E_ALL);

        // If this play mode is active, include template library to generate error page
        $mode = \HK\Config::get('debugger', 'mode');
        if (in_array($mode, ['display', 'log_and_display'])) {
            \HK\import('hk:Template');
        }

        // Create error and exception handler
        self::$handler = new ErrorHandler($mode);
        set_error_handler([self::$handler, 'handleError'], E_ALL);
        set_exception_handler([self::$handler, 'handleException']);
    }

    public static function getMsg() {
        return '('.$this->code.') '.Util::buildString($this->msg, $this->tokens);
    }

    public static function trigger($type, $errorCode, ...$tokens) {
        // Indicate error type
        $type = $type == "error" ? E_USER_ERROR : E_USER_WARNING;

        // Use errorCode to find and format error message
        $msg = \HK\Config::get('debugger', 'error_dict', $errorCode);
        if ($msg) {
            $msg = "[$errorCode] ".call_user_func_array('sprintf', array_merge([$msg], $tokens));
            trigger_error($msg, $type);
        } else {
            trigger_error($errorCode, $type);
        }
    }


    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private $mode = null;
    private $templates = [];


    ////////////
    // METHOD
    public function __construct($mode) {
        $this->mode = $mode;

        // Load message template
        $templates = \HK\Config::get('debugger', 'templates');
        foreach ($templates as $type => $template) {
            $this->templates[$type] = new \HK\Template\Component($template);
        }
    }

    public function handleError($errorNo,
                                $errorMessage,
                                $errorFile,
                                $errorLine,
                                $errorContext) {
        switch ($errorNo) {
            case E_COMPILE_ERROR:
            case E_CORE_ERROR:
            case E_USER_ERROR:
            case E_ERROR:
            case E_PARSE:

            case E_USER_NOTICE:
            case E_NOTICE:
                $this->show(    "error",
                                $errorNo,
                                $errorMessage,
                                $errorFile,
                                $errorLine);
                exit();

            case E_COMPILE_WARNING:
            case E_USER_WARNING:
            case E_CORE_WARNING:
            case E_WARNING:

            case E_USER_DEPRECATED:
            case E_DEPRECATED:
            case E_STRICT:

            case E_RECOVERABLE_ERROR:
                $this->show(    "warning",
                                $errorNo,
                                $errorMessage,
                                $errorFile,
                                $errorLine);
                break;
        }
    }

    public function handleException($exception) {
        $message = $exception->getMessage();
        $code = $exception->getCode();
        $file = $exception->getFile();
        $line = $exception->getLine();

        $this->show(    "exception",
                        999,
                        $message,
                        $file,
                        $line);
    }

    private function show(  $type,
                            $errorNo,
                            $errorMessage,
                            $errorFile,
                            $errorLine) {
        try {
            $head = $type == "error" ? "SYSTEM ERROR" : ($type == "exception" ? "EXCEPTION" : "WARNING");

            // Log error
            if ($this->mode == "log" || $this->mode == "log_and_display") {
                $logger = \HK\Debug\Logger::get($errorFile);
                $logger->error('%1$s >> code %2$d - line %3$d - %4$s', $head, $errorNo, $errorLine, $errorMessage);
            }

            // Display on screen (Only if error or exception occur)
            if (isset($this->templates[$type]) && ($this->mode == "display" || $this->mode == "log_and_display")) {
                $template = $this->templates[$type];
                $template->setContext([
                    'header' =>     $head,
                    'no' =>         $errorNo,
                    'line' =>       $errorLine,
                    'file' =>       $errorFile,
                    'message' =>    $errorMessage,
                    'stackTrace' => debug_backtrace()
                ]);

                $template->render();
                exit();
            }
        } catch(\Exception $e) {
            var_dump("SYSTEM ERROR: ".$e->getMessage());
            exit;
        }
    }
}

ErrorHandler::init();