<?php
namespace HK\Http;

class Request {
    //--------------------------------------------------------------------------------------------------------------
    // Static
    static public function method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    static public function get($name, $default = null) {
        if (array_key_exists($name, $_GET)) {
            return $_GET[$name];
        } else if (array_key_exists($name, $_POST)) {
            return $_POST[$name];
        }

        return $default;
    }

    static public function all() {
        return [
            'args' => array_merge($_GET, $_POST),
            'file' => $_FILES
        ];
    }

    static public function hasPost($key = null) {
        if ($key === null) {
            return !empty($_POST);
        } else {
            return isset($_POST[$key]);
        }
    }

    static public function hasGet($key = null) {
        if ($key === null) {
            return !empty($_GET);
        } else {
            return isset($_GET[$key]);
        }
    }

    static function file($name) {
        if (array_key_exists($name, $_FILES))
            return $_FILES[$name];

        return null;
    }

    static function uri() {
        return $_SERVER['REQUEST_URI'];
    }
}