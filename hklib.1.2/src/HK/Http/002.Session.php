<?php
namespace HK\Http;

class Session {
    //--------------------------------------------------------------------------------------------------------------
    // Static

    //////////////
    // VARIABLE
    private static $isStarted = false;


    ////////////
    // METHOD
    static public function start($config = []) {
        if (self::$isStarted) {
            throw new Exception("Error Session already started!");
        }

        self::$isStarted = session_start($config);
        return $isStarted;
    }

    static public function end() {
        self::$isStarted = false;
        return session_destroy();
    }

    static public function clear() {
        session_unset();
    }

    static public function get($key, $default = null) {
        if (!self::$isStarted) {
            throw new Exception("Error Session not yet started!");
        }

        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }

        return $default;
    }

    static public function set($key, $value) {
        if (!self::$isStarted) {
            throw new Exception("Error Session not yet started!");
        }

        $_SESSION[$key] = $value;
    }

    static public function has($key) {
        if (!self::$isStarted) {
            throw new Exception("Error Session not yet started!");
        }

        return isset($_SESSION[$key]);
    }
}