<?php
namespace HK\JOD;

abstract class QueryBuilderFactory {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    /////////////////////
    // ABSTRACT METHOD

    abstract public function createQueryBuilder($databaseHandler);
}