<?php
namespace HK\JOD;

abstract class QueryBuilder {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private static $builderFactorys;
    private static $builderCache;


    ////////////
    // METHOD
    public static function init() {
        // Import all query builder factory
        $builders = \HK\Config::get('jod', 'query_builders');
        foreach ($builders as $builder) {
            \HK\import($builder);
        }
    }

    public static function addBuilderFactory($type, $factory) {
        self::$builderFactorys[$type] = $factory;
    }

    public static function get($name) {
        // Check if cache exist, if yes, create builder using cache
        if (isset(self::$builderCache[$name])) {
            $factory = $builderCache[$name]['factory'];
            $handler = $builderCache[$name]['handler'];
            return $factory->createQueryBuilder($handler);
        } else {
            // Get config for JOD
            $config = \HK\Config::get('jod', 'configs', $name);
            if ($config) {
                $handler = \HK\DB\Handler::get($config['database_handler']);
                $type = $handler->getType();
                if (!isset(self::$builderFactorys[$type])) {
                    throw new \Exception("Error when init JOD's query builder. Unknown type $type");
                }

                // Get builder factory using database type
                $factory = self::$builderFactorys[$type];

                // Add to cache
                self::$builderCache[$name] = [
                    'factory' => $factory,
                    'handler' => $handler
                ];

                return $factory->createQueryBuilder($handler);
            }
        }

        // Unable to find any builder for JOD
        throw new \Exception("Unable to find query builder with JOD's name: $name");
    }

    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////////////
    // ABSRACT METHOD
    abstract public function setup($databaseHandler);
    abstract public function insert($row);
    abstract public function update($from, $to);
    abstract public function from($table = null);
    abstract public function find($conditions = [], $top = null);
    abstract public function first($conditions = []);
    abstract public function delete($conditions = []);
}

QueryBuilder::init();