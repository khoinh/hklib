<?php
namespace HK\JOD\QueryBuilders\MySQL;

class QueryBuilderFactory extends \HK\JOD\QueryBuilderFactory {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    public static function init() {
        \HK\JOD\QueryBuilder::addBuilderFactory('mysql', new QueryBuilderFactory());
    }

    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct() {
    }

    public function createQueryBuilder($databaseHandler) {
        return (new QueryBuilder())->setup($databaseHandler);
    }
}

QueryBuilderFactory::init();