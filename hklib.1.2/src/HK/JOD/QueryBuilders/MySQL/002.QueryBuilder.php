<?php
namespace HK\JOD\QueryBuilders\MySQL;

class QueryBuilder extends \HK\JOD\QueryBuilder {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // CONSTANT
    const COMP_LIST = ['=', '>', '<', '>=', '<=', 'is', 'like'];

    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private $databaseHandler;
    protected $targetTable;


    ////////////
    // METHOD
    function __construct() {
        $this->databaseHandler = null;
        $this->targetTable = null;
    }

    public function setup($databaseHandler) {
        $this->databaseHandler = $databaseHandler;
        return $this;
    }

    public function from($table = null) {
        $this->targetTable = $table;
        return $this;
    }

    public function find($conditions = [], $limit = null) {
        // Get table name from entity
        $tableName = $this->targetTable;
        if (empty($tableName)) {
            throw new Exception("Error in JOD's Query builder. When find, empty target table name!");
        }

        if ($limit && gettype($limit) == 'integer' && $limit > 0) {
            $limit = "LIMIT $limit";
        } else {
            $limit = "";
        }

        // Select all
        if (empty($conditions)) {
            $query = "SELECT * FROM $tableName $limit";
        }

        // Generate query with condition
        else {
            $query = "SELECT * FROM $tableName WHERE ".$this->generateCondtionQuery('', $conditions)." $limit";
        }

        return $this->executeQuery($query);
    }

    public function first($conditions = []) {
        return $this->find($conditions, 1)[0];
    }

    public function insert($row) {
        // Get table name from entity
        $tableName = $this->targetTable;
        if (empty($tableName)) {
            throw new Exception("Error in JOD's Query builder. When find, empty target table name!");
        }

        // Parse insert values
        $columns = [];
        $values = [];
        foreach ($row as $column => $value) {
            $columns[] = $column;
            $values[] = $this->convertToSqlValue($value);
        }

        $columns = implode(', ', $columns);
        $values = implode(', ', $values);
        $query = "INSERT INTO $tableName ($columns) VALUES ($values)";
        return $this->executeQuery($query);
    }

    public function update($from, $to) {
        // Get table name from entity
        $tableName = $this->targetTable;
        if (empty($tableName)) {
            throw new Exception("Error in JOD's Query builder. When find, empty target table name!");
        }

        // From conditions
        $conditions = $from;

        // Get update value
        $values = [];
        foreach ($to as $key => $value) {
            $values[] = "$key = ".$this->convertToSqlValue($value);
        }

        // Update all
        if (empty($conditions)) {
            if (\HK\Config::get('jod', 'safe_update')) {
                throw new \Exception('JOD safe update mode active, can not update all data!');
            }

            $query = "UPDATE $tableName SET ".implode(', ', $values)." WHERE true";
        }

        // Generate query with condition
        else {
            $query = "UPDATE $tableName SET ".implode(', ', $values)." WHERE ".$this->generateCondtionQuery('', $conditions);
        }

        return $this->executeQuery($query);
    }

    public function delete($conditions = []) {
        // Get table name from entity
        $tableName = $this->targetTable;
        if (empty($tableName)) {
            throw new Exception("Error in JOD's Query builder. When find, empty target table name!");
        }

        // Delete all
        if (empty($conditions)) {
            if (\HK\Config::get('jod', 'safe_delete')) {
                throw new \Exception('JOD safe delete mode active, can not delete all data!');
            }

            $query = "DELETE FROM $tableName";
        }

        // Generate query with condition
        else {
            $query = "DELETE FROM $tableName WHERE ".$this->generateCondtionQuery('', $conditions);
        }

        return $this->executeQuery($query);
    }

    protected function executeQuery($query) {
        // Execute query and get result
        $result = $this->databaseHandler->execute($query);
        if (!$result->isSuccess()) {
            throw new \Exception($result->getError());
        }

        // Fetch result
        $value = $result->value();
        if (gettype($value) == 'integer' || gettype($value) == 'boolean') {
            return $value;
        } else {
            return $value->fetch_all(MYSQLI_ASSOC);
        }
    }

    protected function generateCondtionQuery($name, $conditions, $isOr = true, $comp = '=') {
        $query = [];
        foreach ($conditions as $key => $value) {
            // Get compare key (Or new column name)
            $newComp = $comp;
            $newName = $name;
            if (in_array($key, self::COMP_LIST)) {
                $newComp = $key;
            } else {
                $newName = $key;
            }

            if (gettype($value) == 'array') {
                // Continue to generate sql query using new value get from array
                $query[] = $this->generateCondtionQuery($newName, $value, !$isOr, $newComp);
            } else if ($value instanceof \DateTime) {
                return "$newName $newComp ".$this->convertToSqlValue($value);
            } else {
                return "$newName $newComp ".$this->convertToSqlValue($value);
            }
        }

        return '('.implode($isOr ? ' OR ' : ' AND ', $query).')';
    }

    protected function convertToSqlValue($value) {
        if ($value instanceof \DateTime) {
            return "'".$value->format('Y-m-d H:i:s')."'";
        } if (gettype($value) == 'array' || gettype($value) == 'object' || gettype($value) == 'resource') {
            return json_encode($value);
        } else if (gettype($value) == 'NULL') {
            return "NULL";
        } else if (gettype($value) == 'string') {
            return "'$value'";
        } else if (gettype($value) == 'boolean') {
            return $value ? '1' : '0';
        } else {
            return $value;
        }
    }
}