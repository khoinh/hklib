<?php
namespace HK\Main;

class Callback {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct() {}

    public function __call($name, $args) {
        $args = $this->beforeCall($args);
        $name = ucfirst($name);
        if (method_exists($this, "before$name")) {
            $args = call_user_func_array([$this, "before$name"], ['args' => $args]);
        }

        $result = call_user_func_array([$this, "on$name"], ['args' => $args]);
        if (method_exists($this, "after$name")) {
            $result = call_user_func_array([$this, "after$name"], ['result' => $result]);
        }

        return $this->afterCall($result);
    }

    public function beforeCall($args) {
        return $args;
    }

    public function afterCall($result) {
        return $result;
    }

    public function hasCallback($name) {
        $name = ucfirst($name);
        return method_exists($this, "on$name");
    }
}