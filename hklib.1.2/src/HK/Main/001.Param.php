<?php
namespace HK\Main;

class Param {
    //////////////
    // VARIABLE
    protected $data;


    ////////////
    // METHOD
    public function __construct() {
        $this->data = [];
    }

    public function init($data) {
        foreach ($data as $key => $value) {
            if (gettype($value) == 'array') {
                $value = (new Param())->init($value);
            }

            $this->data[$key] = $value;
        }

        return $this;
    }

    public function __set($name, $value) {
        $this->data[$name] = $value;
    }

    public function __get($name) {
        if (!array_key_exists($name, $this->data)) {
            return null;
        }

        return $this->data[$name];
    }

    public function __isset($name) {
        return array_key_exists($name, $this->data);
    }

    public function all() {
        return $this->data;
    }

    public function size() {
        return count($this->data);
    }
}