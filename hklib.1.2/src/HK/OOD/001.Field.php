<?php
namespace HK\OOD;

use HK\DB\QueryResult as QueryResult;


/**
 * A Field represent a column in database table
 */
class Field {
    //////////////
    // VARIABLE
    protected $value;   // Column value
    protected $options; // Options determine the field behavior
    protected $isUnknown;


    ////////////
    // METHOD
    function __construct($options = []) {
        $this->value = null;
        $this->options = $options;
        $this->isUnknown = true;
    }

    public function isUnknown() {
        return $this->isUnknown;
    }

    public function unknown() {
        $this->isUnknown = true;
        return $this;
    }

    public function getValue() {
        return $this->value;
    }

    public function setValue($value) {
        $this->value = $value;
        $this->isUnknown = false;

        if ($this->getOption('trim-value')) {
            $this->value = trim($this->value);
        }

        return $this;
    }

    public function getOptions() {
        return $this->options;
    }

    public function getOption($name) {
        if (isset($this->options[$name])) {
            return $this->options[$name];
        }

        return null;
    }

    public function setOption($name, $value) {
        if (!array_key_exists($name, $this->options)) {
            $this->options[$name] = $value;
        }

        return $this;
    }

    /**
     * Use this to get column name
     * @return [type] [description]
     */
    public function getName() {
        return $this->getOption('name');
    }

    public function copy($field) {
        $this->value = $field->value;
        $this->options = $field->options;
        $this->isUnknown = $field->isUnknown;
        return $this;
    }

    public function clone() {
        $className = get_class($this);
        $result = new $className();
        return $result->copy($this);
    }
}