<?php
namespace HK\OOD;

use HK\DB\QueryResult as QueryResult;

/**
 * Entity is basically an object representation of a table in database
 */
class Entity {
    //////////////
    // VARIABLE
    protected $fields = [];     // Field is a representation of column in table
    protected $options = [];    // Options defined entity behavior


    ////////////
    // METHOD
    function __construct() {
    }

    public function getOptions() {
        return $this->options;
    }

    public function getOption($name) {
        if (isset($this->options[$name])) {
            return $this->options[$name];
        }

        return null;
    }

    public function setOption($name, $value) {
        if (array_key_exists($name, $this->options)) {
            $this->options[$name] = $value;
        }

        return $this;
    }

    public function setOptions($options) {
        $this->options = $options;
        return $this;
    }

    public function getFields(...$options) {
        if (empty($options)) {
            return $this->fields;
        }

        $result = [];
        foreach ($this->fields as $name => $field) {
            $valid = true;
            foreach ($options as $option) {
                if (!$field->getOption($option)) {
                    $valid = false;
                    break;
                }
            }

            if ($valid) {
                $result[$name] = $field;
            }
        }

        return $result;
    }

    public function getField($name) {
        return $this->fields[$name];
    }

    public function hasField($name) {
        return isset($this->fields[$name]);
    }

    public function copy($entity) {
        $this->options = $entity->options;
        foreach ($this->fields as $name => $field) {
            if (isset($entity->$name)) {
                $field->copy($entity->getField($name));
            }
        }

        return $this;
    }

    public function clone() {
        $className = get_class($this);
        $result = new $className();
        return $result->copy($this);
    }

    public function unknown() {
        foreach ($this->fields as $field) {
            $field->unknown();
        }

        return $this;
    }

    /**
     * Use this function to set field for entity, should be when construct the entity
     */
    public function __set($name, $value) {
        // If input is a field, Check if field already exist.
        // If not, add field to field list
        if ($value instanceof Field) {
            if (array_key_exists($name, $this->fields)) {
                throw new \Exception("Error: In entity ".get_class($this).", when add new field (__set). Field name: $name already existed!");
            }

            // Save to field list
            $this->fields[$name] = $value;

            // Check if field has name, if not generate a default name
            if (empty($value->getOption('name'))) {
                // Generate default name using field name and uppercase first letter
                $this->fields[$name]->setOption('name', ucfirst($name));
            }
        }

        // If input value isn't a field, and entity does not have field name in field list
        // Output invalid variable error message.
        else if (!array_key_exists($name, $this->fields)) {
            throw new \Exception("Error: In entity ".get_class($this).", when set field value (__set). Field name: $name not existed!");
        }

        // If input value isn't a field, and field name exist in field list
        // Set input as value to that field
        else {
            $this->fields[$name]->setValue($value);
        }
    }

    public function __get($name) {
        // Get field value if exist
        if (array_key_exists($name, $this->fields)) {
            return $this->fields[$name]->getValue();
        }

        // Output non existing field error if field not exist in field list
        throw new \Exception("Error: In entity ".get_class($this).", when get field value (__get). Field name: $name not existed!");
    }

    public function __isset($name) {
        // Check if field with name exist
        if (!isset($this->fields[$name])) {
            throw new \Exception("Error: Trying to check field value. Field name $name not existed!");
        }

        return !$this->fields[$name]->isUnknown();
    }

    public function __unset($name) {
        if (!isset($this->fields[$name])) {
            throw new \Exception("Error: Trying to uset field value. Field name $name not existed!");
        }

        $this->fields[$name]->unknown();
    }

    public function getPrimaryKeys() {
        // Get all field with primary-key options
        $result = [];
        foreach ($this->fields as $name => $field) {
            if ($field->getOption('primary-key'))
                $result[$name] = $field;
        }

        return $result;
    }

    /**
     * Use this to get table name
     * @return [type] [description]
     */
    public function getName() {
        // Check if option name exist
        // If yes, then use this option as table name
        if (!empty($this->getOption('name'))) {
            return $this->getOption('name');
        }

        // If no, use class name as table name
        $name = \HK\Util::getClassName($this);
        $this->setOption('name', $name);
        return $name;
    }

    public function isUnknown() {
        foreach ($this->fields as $field) {
            if (!$field->isUnknown()) {
                return false;
            }
        }

        return true;
    }

    // QUERY METHOD ----------------------------------------------------------------------------------------------------------------------------------
    public function select() {
        return \HK\OOD\QueryBuilder::select($this);
    }

    public function insert() {
        return \HK\OOD\QueryBuilder::insert($this);
    }

    public function delete() {
        return \HK\OOD\QueryBuilder::delete($this);
    }

    public function update($to) {
        return \HK\OOD\QueryBuilder::update($this, $to);
    }

    public function reload() {
        return \HK\OOD\QueryBuilder::reload($this);
    }

    public function save() {
        return \HK\OOD\QueryBuilder::save($this);
    }

    public function remove() {
        return \HK\OOD\QueryBuilder::remove($this);
    }
}