<?php
namespace HK\OOD;

abstract class QueryBuilderFactory {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    /////////////////////
    // ABSTRACT METHOD

    abstract public function createQueryBuilder($databaseHandler);
}