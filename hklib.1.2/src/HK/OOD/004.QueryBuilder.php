<?php
namespace HK\OOD;

abstract class QueryBuilder {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private static $settings;
    private static $builders;
    private static $builderFactorys;
    private static $classToBuilderCache;


    ////////////
    // METHOD
    public static function init() {
        // Import all database handler
        $builders = \HK\Config::get('ood', 'query_builders');
        foreach ($builders as $builder) {
            \HK\import($builder);
        }

        // Setup
        self::$settings = \HK\Config::get('ood', 'configs');
        self::$classToBuilderCache = [];
        self::$builders = [];

        // Use database factory to create database handler
        foreach (self::$settings as $name => $setting) {
            $handler = \HK\DB\Handler::get($setting['database_handler']);
            $type = $handler->getType();
            if (!isset(self::$builderFactorys[$type])) {
                throw new \Exception("Error when init OOD's query builder. Unknown type $type");
            } if (isset(self::$builders[$setting['entity_namespace']])) {
                throw new \Exception("Error when create query builder. Namespace ".$setting['entity_namespace']." already used!");
            } else {
                $factory = self::$builderFactorys[$type];
                self::$builders[$setting['entity_namespace']] = $factory->createQueryBuilder($handler);
            }
        }
    }

    public static function addBuilderFactory($type, $factory) {
        self::$builderFactorys[$type] = $factory;
    }

    public static function get($namespace) {
        if (isset(self::$builders[$namespace])) {
            return self::$builders[$namespace];
        }

        throw new \Exception("Unable to find query builder with namespace: $namespace");
    }

    public static function has($namespace) {
        return isset(self::$builders[$namespace]);
    }

    protected static function chooseQueryBuilder($entity) {
        $queryBuilder = null;
        $className = get_class($entity);

        if (!isset(self::$classToBuilderCache[$className])) {
            // Find builder using entity's namespace
            $entityNamespace = \HK\Util::getNamespace($entity);
            $queryBuilder = self::get($entityNamespace);
            self::$classToBuilderCache[$className] = $queryBuilder;
        } else {
            // Find builder using entity's class name
            $queryBuilder = self::$classToBuilderCache[$className];
        }

        return $queryBuilder;
    }

    public static function select($entity) {
        return self::chooseQueryBuilder($entity)->selectEntity($entity);
    }

    public static function insert($entity) {
        return self::chooseQueryBuilder($entity)->insertEntity($entity);
    }

    public static function update($from, $to) {
        return self::chooseQueryBuilder($from)->updateEntity($from, $to);
    }

    public static function delete($entity) {
        return self::chooseQueryBuilder($entity)->deleteEntity($entity);
    }

    public static function reload($entity) {
        return self::chooseQueryBuilder($entity)->reloadEntity($entity);
    }

    public static function remove($entity) {
        return self::chooseQueryBuilder($entity)->removeEntity($entity);
    }

    public static function save($entity) {
        return self::chooseQueryBuilder($entity)->saveEntity($entity);
    }

    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////////////
    // ABSRACT METHOD
    abstract public function setup($databaseHandler);
    abstract public function selectEntity($entity);
    abstract public function insertEntity($entity);
    abstract public function updateEntity($from, $to);
    abstract public function deleteEntity($entity);
    abstract public function reloadEntity($entity);
    abstract public function removeEntity($entity);
    abstract public function saveEntity($entity);
}

QueryBuilder::init();