<?php
namespace HK\OOD\QueryBuilders\MySQL;

class QueryBuilderFactory extends \HK\OOD\QueryBuilderFactory {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    public static function init() {
        \HK\OOD\QueryBuilder::addBuilderFactory('mysql', new QueryBuilderFactory());
    }

    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct() {
    }

    public function createQueryBuilder($databaseHandler) {
        return (new QueryBuilder())->setup($databaseHandler);
    }
}

QueryBuilderFactory::init();