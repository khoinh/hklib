<?php
namespace HK\OOD\QueryBuilders\MySQL;

class QueryBuilder extends \HK\OOD\QueryBuilder {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // CONSTANT
    const COMP_LIST = ['=', '>', '<', '>=', '<=', 'is', 'like'];

    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private $databaseHandler;


    ////////////
    // METHOD
    function __construct() {
        $this->databaseHandler = null;
    }

    public function setup($databaseHandler) {
        $this->databaseHandler = $databaseHandler;
        return $this;
    }

    public function selectEntity($entity) {
        // Get table name from entity
        $tableName = $entity->getName();
        $fields = $entity->getFields();
        $conditions = [];

        // Get column value conditions, for every column with a known value
        foreach ($fields as $name => $field) {
            if ($field->isUnknown()) {
                continue;
            }

            // Get column value
            $columnName = $field->getName();
            $conditions[$columnName] = $field->getValue();
        }

        // Select all
        if (empty($conditions)) {
            $query = "SELECT * FROM $tableName";
        }

        // Generate query with condition
        else {
            $query = "SELECT * FROM $tableName WHERE ".$this->generateCondtionQuery('', $conditions);
        }

        return $this->executeQuery($query, $entity);
    }

    public function insertEntity($entity) {
        // Get table name from entity
        $tableName = $entity->getName();
        $fields = $entity->getFields();

        // Parse insert values
        $columns = [];
        $values = [];
        foreach ($fields as $name => $field) {
            if ($field->isUnknown()) {
                continue;
            }

            $columns[] = $name;
            $values[] = $this->convertToSqlValue($field->getValue());
        }

        $columns = implode(', ', $columns);
        $values = implode(', ', $values);
        $query = "INSERT INTO $tableName ($columns) VALUES ($values)";
        return $this->executeQuery($query, $entity, true);
    }

    public function deleteEntity($entity) {
        // Get table name from entity
        $tableName = $entity->getName();
        $fields = $entity->getFields();

        // Get column value conditions, for every column with a known value
        foreach ($fields as $field) {
            if ($field->isUnknown()) {
                continue;
            }

            // Get column value
            $columnName = $field->getName();
            $conditions[$columnName] = $field->getValue();
        }

        // Delete all
        if (empty($conditions)) {
            if (\HK\Config::get('ood', 'safe_delete')) {
                throw new \Exception('Safe delete mode active, can not delete all data!');
            }

            $query = "DELETE FROM $tableName";
        }

        // Generate query with condition
        else {
            $query = "DELETE FROM $tableName WHERE ".$this->generateCondtionQuery('', $conditions);
        }

        return $this->executeQuery($query, $entity);
    }

    public function updateEntity($from, $to) {
        if (get_class($from) != get_class($to)) {
            throw new \Exception("Error: When update entity. Two entity have different class from ".get_class($from)." to ".get_class($to));
        }

        // Get table name from entity
        $tableName = $from->getName();
        $fields = $from->getFields();

        // Get column value conditions, for every column with a known value
        foreach ($fields as $field) {
            if ($field->isUnknown()) {
                continue;
            }

            // Get column value
            $columnName = $field->getName();
            $conditions[$columnName] = $field->getValue();
        }

        // Get update value
        $fields = $to->getFields();
        $values = [];
        foreach ($fields as $name => $field) {
            if ($field->isUnknown()) {
                continue;
            }

            $values[] = "$name = ".$this->convertToSqlValue($field->getValue());
        }

        // Update all
        if (empty($conditions)) {
            if (\HK\Config::get('ood', 'safe_update')) {
                throw new \Exception('Safe update mode active, can not update all data!');
            }

            $query = "UPDATE $tableName SET ".implode(', ', $values)." WHERE true";
        }

        // Generate query with condition
        else {
            $query = "UPDATE $tableName SET ".implode(', ', $values)." WHERE ".$this->generateCondtionQuery('', $conditions);
        }

        return $this->executeQuery($query, $to, true);
    }

    public function reloadEntity($entity) {
        $result = $this->clearEntityToOnlyPrimaryKey($entity);

        // Select all with primary key
        $result = $result->select();
        if (gettype($result) == 'array') {
            return $result[0];
        } else {
            return false;
        }
    }

    public function removeEntity($entity) {
        $result = $this->clearEntityToOnlyPrimaryKey($entity);
        return $result->delete();
    }

    public function saveEntity($entity) {
        // Clone the original entity
        $from = $this->clearEntityToOnlyPrimaryKey($entity);
        return ($from->update($entity) ? $entity->reload() : false);
    }

    protected function clearEntityToOnlyPrimaryKey($entity) {
        // Clone the original entity
        $result = $entity->clone();
        $fields = $result->getFields();
        foreach ($fields as $name => $field) {
            // Clear all field which is not primary key
            if (!$field->getOption('primary-key')) {
                $field->unknown();
            } else if ($field->isUnknown()) {
                throw new \Exception('Error: Call update method on unknown primary key entity. '.get_class($entity));
            }
        }

        return $result;
    }

    protected function convertToSqlValue($value) {
        if ($value instanceof \DateTime) {
            return "'".$value->format('Y-m-d H:i:s')."'";
        } if (gettype($value) == 'array' || gettype($value) == 'object' || gettype($value) == 'resource') {
            return json_encode($value);
        } else if (gettype($value) == 'NULL') {
            return "NULL";
        } else if (gettype($value) == 'string') {
            return "'$value'";
        } else if (gettype($value) == 'boolean') {
            return $value ? '1' : '0';
        } else {
            return $value;
        }
    }

    protected function generateCondtionQuery($name, $conditions, $isOr = true, $comp = '=') {
        $query = [];
        foreach ($conditions as $key => $value) {
            // Get compare key (Or new column name)
            $newComp = $comp;
            $newName = $name;
            if (in_array($key, self::COMP_LIST)) {
                $newComp = $key;
            } else {
                $newName = $key;
            }

            if (gettype($value) == 'array') {
                // Continue to generate sql query using new value get from array
                $query[] = $this->generateCondtionQuery($newName, $value, !$isOr, $newComp);
            } else if ($value instanceof \DateTime) {
                return "$newName $newComp ".$this->convertToSqlValue($value);
            } else {
                return "$newName $newComp ".$this->convertToSqlValue($value);
            }
        }

        return '('.implode($isOr ? ' OR ' : ' AND ', $query).')';
    }

    protected function executeQuery($query, $entity, $reloadEntity = false) {
        // Execute query and get result
        $result = $this->databaseHandler->execute($query);
        if (!$result->isSuccess()) {
            throw new \Exception($result->getError());
        } else if (gettype($result->value()) == 'integer' && $reloadEntity) {
            $primaryKey = $entity->getFields('primary-key');
            foreach ($primaryKey as $key => $field) {
                if ($field->isUnknown()) {
                    $field->setValue($result->value());
                }
            }

            return $entity->reload();
        } else if ($result->value() === true || gettype($result->value()) == 'integer') {
            return true;
        } else {
            $result = $result->value()->fetch_all(MYSQLI_ASSOC);
        }

        // Create a list of entity base on query result
        $entityResultList = [];
        $fieldList = $entity->getFields();
        $className = get_class($entity);
        foreach ($result as $row) {
            // Create a copy of entity
            $newEntity = new $className();
            $newEntity->copy($entity);
            foreach ($fieldList as $fieldName => $field) {
                // Set new value
                $columnName = $field->getName();
                if (array_key_exists($columnName, $row)) {
                    // Get data and project it in clone entity
                    $newEntity->$fieldName = $row[$columnName];
                }
            }

            // Add new entity to result list
            $entityResultList[] = $newEntity;
        }

        return $entityResultList;
    }
}