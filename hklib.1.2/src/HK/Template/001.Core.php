<?php
namespace HK\Template;

function render($url, $context = []) {
    if (empty($url)) {
        return;
    }

    // Update current context with global context (Saved in HK\Main\Context)
    $context = \HK\Util::deepMergeArray(\HK\Context::get('hk.template.global_context', []), $context);

    // Context stack save previous page variable context (Before the template render)
    // After finished rendering template, it will return the varible to it's previous context
    $contextStack = [];
    if ($context instanceof \HK\Main\Param) {
        $context = $context->all();
    }

    // Save parameter before set context
    foreach ($context as $key => $value) {
        // Save previous variable to stack
        if (isset($$key)) {
            $contextStack[$key] = $$key;
        }

        // Translate template context to variable
        $$key = $value;
    }

    // Import template
    $result = \HK\Util::getPackagesUrl($url);
    foreach ($result as $url) {
        require($url);
    }

    // Reset value
    foreach ($context as $key => $value) {
        if (isset($contextStack[$key])) {
            $$key = $contextStack[$key];
        } else {
            unset($$key);
        }
    }
}