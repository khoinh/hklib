<?php
namespace HK\Template;

class Component {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    protected $template;
    protected $context;


    ////////////
    // METHOD
    function __construct($template = null, $context = []) {
        $this->template = $template;
        $this->context = $context;
    }

    public function render() {
        $this->beforeRender();
        \HK\Template\render($this->template, $this->context);
        $this->afterRender();
    }

    public function setTemplate($template) {
        $this->template = $template;
        return $this;
    }

    public function setContext($context) {
        $this->context = $context;
        return $this;
    }

    public function getTemplate() {
        return $this->template;
    }

    public function getContext() {
        return $this->context;
    }

    public function updateContext($newContext) {
        $this->context = \HK\Util::deepMergeArray($this->context, $newContext);
        return $this;
    }

    protected function beforeRender() {}
    protected function afterRender() {}
}