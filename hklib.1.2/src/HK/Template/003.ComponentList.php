<?php
namespace HK\Template;

class ComponentList {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    protected $components;


    ////////////
    // METHOD
    function __construct() {
        $this->components = [];
    }

    public function render() {
        $this->beforeRender();
        foreach ($this->components as $component) {
            $component->render();
        }

        $this->afterRender();
    }

    public function add(...$args) {
        if (count($args) == 1) {
            $this->components[] = $args[0];
            return $args[0];
        } else if (count($args) == 2) {
            $name = $args[0];
            $component = $args[1];
            $this->components[$name] = $component;
            return $component;
        }

        throw new \Exception("Error in ComponentList function: Required one or two arguments");
    }

    public function addList($data) {
        foreach ($data as $key => $value) {
            $this->components[$key] = $value;
        }

        return $this;
    }

    public function get($name) {
        if (isset($this->components[$name])) {
            return $this->components[$name];
        } else {
            return null;
        }
    }

    public function delete($name) {
        unset($this->components[$name]);
        return $this;
    }

    public function clear() {
        $this->components = [];
        return $this;
    }

    public function getAll() {
        return $this->components;
    }

    protected function beforeRender() {}
    protected function afterRender() {}
}