<?php
namespace HK\UI\Common;

class Tag extends \HK\Template\Component {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct($tagName = 'div', $content = '', $attrs = [], $class = [], $hasClosingTag = true) {
        parent::__construct('hk-template:UI/Common/Tag.temp.php', [
            'tagName' =>        $tagName,
            'attrs' =>          $this->getDefaultAttrs() + $attrs,
            'content' =>        $content,
            'class' =>          $class,
            'hasClosingTag' =>  $hasClosingTag
        ]);
    }

    protected function getDefaultAttrs() {
        return [];
    }

    public function setTagName($tagName) {
        $this->context['tagName'] = $tagName;
        return $this;
    }

    public function getTagName() {
        return $this->context['tagName'];
    }

    public function setContent($content) {
        $this->context['content'] = $content;
        return $this;
    }

    public function getContent() {
        return $this->context['content'];
    }

    public function addAttributes(...$attrs) {
        foreach ($attrs as $name => $value) {
            $this->context['attrs'][$name] = $value;
        }

        return $this;
    }

    public function getAttributes() {
        return $this->context['attrs'];
    }

    public function setAttribute($name, $value) {
        $this->context['attrs'][$name] = $value;
        return $this;
    }

    public function getAttribute($name = null) {
        if (isset($this->context['attrs'][$name])) {
            return $this->context['attrs'][$name];
        }

        return null;
    }

    public function getAllAttributes() {
        return $this->context['attrs'];
    }

    public function getClass() {
        return $this->context['class'];
    }

    public function addClass(...$classes) {
        $this->context['class'] += $classes;
        return $this;
    }

    public function clearClass() {
        $this->context['class'] = [];
    }

    protected function beforeRender() {
        if (isset($this->context['attrs']['class'])) {
            unset($this->context['attrs']['class']);
        }
    }
}