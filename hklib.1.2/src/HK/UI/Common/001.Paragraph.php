<?php
namespace HK\UI\Common;

class Paragraph extends Tag {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct($content = '', $attrs = [], $class = []) {
        parent::__construct('p', $content, $attrs, $class);
    }
}