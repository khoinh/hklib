<?php
namespace HK\UI\Common;

class Input extends Tag {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct($attrs = [], $class = []) {
        parent::__construct('input', null, $attrs, $class, false);
    }
}