<?php
namespace HK\UI\Common;

class Option extends Tag {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct($value, $name) {
        parent::__construct('option', $name, ['value' => $value]);
    }

    public function select() {
        $this->setAttribute('selected', 'selected');
        return $this;
    }

    public function isSelected() {
        return $this->getAttribute('selected') == 'selected';
    }

    public function getValue() {
        return $this->getAttribute('value');
    }

    public function setValue($value) {
        $this->setAttribute('value', $value);
        return $this;
    }
}

class Select extends Tag {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct($attrs = [], $class = []) {
        parent::__construct('select', new \HK\Template\ComponentList(), $attrs, $class);
    }

    public function addOptions($options) {
        $content = $this->getContent();
        foreach ($options as $value => $name) {
            $option = new Option($value, $name);
            $content->add($value, $option);
        }
    }

    public function getOption($value) {
        return $this->getContent()->get($value);
    }

    public function getSelectedOption() {
        $options = $this->getContent()->getAll();
        foreach ($options as $option) {
            if ($option->isSelected()) {
                return $option;
            }
        }

        return null;
    }

    public function clearOptions() {
        $this->getContent()->clear();
        return $this;
    }
}