<?php
namespace HK\UI\Common;

class Div extends Tag {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct($content = '', $attrs = [], $class = []) {
        parent::__construct('div', $content, $attrs, $class);
    }
}