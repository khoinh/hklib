<?php
namespace HK\UI\Forms;

class CommonForm extends \HK\UI\Common\Tag {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct($attrs = [], $class = []) {
        parent::__construct('form', new \HK\Template\ComponentList());
    }

    protected function getDefaultAttrs() {
        return [
            'method' => 'POST',
            'action' => '#',
            'target' => '_self'
        ];
    }

    public function setMethod($method) {
        $this->context['method'] = $method;
        return $this;
    }

    public function getMethod() {
        return $this->context['method'];
    }

    public function setAction($action) {
        $this->context['action'] = $action;
        return $this;
    }

    public function getAction() {
        return $this->context['action'];
    }

    public function setTarget($target) {
        $this->context['target'] = $target;
        return $this;
    }

    public function getTarget() {
        return $this->context['target'];
    }

    public function setBody($body) {
        $this->context['content'] = $body;
        return $this;
    }

    public function getBody() {
        return $this->context['content'];
    }
}