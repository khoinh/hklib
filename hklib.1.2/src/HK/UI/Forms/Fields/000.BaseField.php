<?php
namespace HK\UI\Forms\Fields;

class BaseField extends \HK\Template\Component {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct() {
        parent::__construct('hk-template:UI/Forms/Fields/BaseField.temp.php', [
            'label' =>          '',
            'input' =>          $this->generateInput(),
            'errorMessage' =>   null
        ]);
    }

    protected function generateInput() {
        return null;
    }

    public function setLabel($label) {
        $this->context['label'] = $label;
        return $this;
    }

    public function getLabel() {
        return $this->context['label'];
    }

    public function setError($errorMessage) {
        $this->context['errorMessage'] = $errorMessage;
        return $this;
    }

    public function getError() {
        return $this->context['errorMessage'];
    }

    public function setInput($input) {
        $this->context['input'] = $input;
        return $this;
    }

    public function getInput() {
        return $this->context['input'];
    }

    public function setName($name) {
        if ($this->getInput()) {
            $this->getInput()->setAttribute('name', $name);
        }

        return $this;
    }

    public function getName() {
        if ($this->getInput()) {
            return $this->getInput()->getAttribute('name');
        }

        return null;
    }

    public function setValue($value) {
        if ($this->getInput()) {
            $this->getInput()->setContent($value);
        }

        return $this;
    }

    public function getValue() {
        if ($this->getInput()) {
            return $this->getInput()->getContent();
        }

        return null;
    }
}