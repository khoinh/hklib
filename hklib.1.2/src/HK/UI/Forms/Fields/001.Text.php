<?php
namespace HK\UI\Forms\Fields;

class Text extends BaseField {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    protected function generateInput() {
        $input = new \HK\UI\Common\Input(['type' => 'text']);
        return $input;
    }

    public function setValue($value) {
        if ($this->getInput()) {
            $this->getInput()->setAttribute('value', $value);
        }

        return $this;
    }

    public function getValue() {
        if ($this->getInput()) {
            return $this->getInput()->getAttribute('value');
        }

        return null;
    }
}