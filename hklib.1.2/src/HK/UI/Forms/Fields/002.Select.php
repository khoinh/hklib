<?php
namespace HK\UI\Forms\Fields;

class Select extends BaseField {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct($options = []) {
        parent::__construct();
        $this->getInput()->addOptions($options);
    }

    protected function generateInput() {
        $input = new \HK\UI\Common\Select();
        return $input;
    }

    public function addOptions($options) {
        $this->getInput()->addOptions($options);
        return $this;
    }

    public function clearOptions() {
        $this->getInput()->clearOptions();
        return $this;
    }

    public function setOptions($options) {
        $this->clearOptions()->addOptions($options);
    }

    public function setValue($value) {
        if ($this->getInput()) {
            $option = $this->getInput()->getOption($value);
            if ($option) {
                $option->select();
            }
        }

        return $this;
    }

    public function getValue() {
        if ($this->getInput()) {
            $option = $this->getInput()->getSelectedOption();
            if ($option) {
                return $option->getValue();
            }
        }

        return null;
    }
}