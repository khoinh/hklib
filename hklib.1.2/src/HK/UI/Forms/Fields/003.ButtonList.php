<?php
namespace HK\UI\Forms\Fields;

class ButtonList extends \HK\Template\Component {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE

    ////////////
    // METHOD
    function __construct() {
        parent::__construct('hk-template:UI/Forms/Fields/ButtonList.temp.php', [
            'buttons' => []
        ]);

        $this->name = '';
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function addButtons($names) {
        foreach ($names as $name) {
            $button = new \HK\UI\Common\Input([
                'type' => 'submit',
                'value' => $name,
                'name' => $this->getName().'['.$name.']'
            ]);

            $this->context['buttons'][$name] = $button;
        }

        return $this;
    }

    public function clearAll() {
        $this->context['buttons'] = [];
    }

    public function removeButton($name) {
        unset($this->context['buttons'][$name]);
    }
}