<?php
namespace HK\UI\Forms\Fields;

class Password extends Text {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    protected function generateInput() {
        $input = new \HK\UI\Common\Input(['type' => 'password']);
        return $input;
    }
}