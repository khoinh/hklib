<?php
namespace HK\UI\Pages;

class Common extends \HK\Template\Component {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    protected $body;


    ////////////
    // METHOD
    function __construct() {
        parent::__construct('hk-template:UI/Pages/Common.temp.php', [
            'title' =>          "Common page",
            'contentType' =>    "text/html; charset=utf-8",
            'styleSheetList' => [],
            'scriptList' =>     [],
            'body' =>           null
        ]);
    }

    public function setTitle($title) {
        $this->context['title'] = $title;
        return $this;
    }

    public function getTitle() {
        return $this->context['title'];
    }

    public function setContentType($contentType) {
        $this->context['contentType'] = $contentType;
        return $this;
    }

    public function getContentType() {
        return $this->context['contentType'];
    }

    public function addStyleSheet(...$styleSheetList) {
        foreach ($styleSheetList as $styleSheet) {
            $this->context['styleSheetList'][] = $styleSheet;
        }

        return $this;
    }

    public function getStyleSheetList() {
        return $this->context['styleSheetList'];
    }

    public function addScriptList(...$scriptList) {
        foreach ($scriptList as $script) {
            $this->context['scriptList'][] = $script;
        }

        return $this;
    }

    public function getScriptList() {
        return $this->context['scriptList'];
    }

    public function setBody($body) {
        $this->context['body'] = $body;
        return $this;
    }

    public function getBody() {
        return $this->context['body'];
    }
}