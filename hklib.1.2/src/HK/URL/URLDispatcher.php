<?php
namespace HK\URL;

\HK\import('hk:Http');

class URLDispatcher {
    // STATIC ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    public static function dispatch($configs) {
        $url = \HK\Http\Request::uri();
        foreach ($configs as $pattern => $detail) {
            // Example pattern: "^/(?<module>[a-zA-Z0-9_\.]*)/(?<id>[a-zA-Z0-9_\.]*)"
            $pattern = str_replace('/', '\/', $pattern);
            if (preg_match_all("/$pattern/", $url, $matches)) {
                // Prepare options
                $options = [];
                foreach ($matches as $key => $value) {
                    if (is_int($key)) {
                        continue;
                    }

                    $options[$key] = $value[0];
                }

                // Call dispatch function (or file)
                if (gettype($detail) == 'array') {
                    if (count($detail) == 1) {
                        call_user_func_array($detail[0], ['options' => $options]);
                    } else if (count($detail) == 2 && gettype($detail[0]) == 'object') {
                        call_user_func_array([$detail[0], $detail[1]],  ['options' => $options]);
                    }
                } else {
                    // Try to import file.
                    \HK\import($detail);
                }

                // Only match one first pattern
                break;
            }
        }
    }
}