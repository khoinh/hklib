<?php
namespace HK;

class Util {
    public static function deepMergeArray($array1, $array2, $appendIntegerIndex = true) {
        foreach ($array2 as $key => $value) {
            if (is_int($key) && $appendIntegerIndex) {
                $array1[] = $value;
            } else if (gettype($value) == 'array' && isset($array1[$key]) && gettype($array1[$key]) == 'array') {
                $array1[$key] = self::deepMergeArray($array1[$key], $value);
            } else {
                $array1[$key] = $value;
            }
        }

        return $array1;
    }

    public static function getClassName($object) {
        $result = explode('\\', get_class($object));
        return end($result);
    }

    public static function getNamespace($object) {
        $fullClassName = get_class($object);
        return substr($fullClassName, 0, strrpos($fullClassName, '\\'));
    }

    public static function getPackagesUrl($packages) {
        $result = [];

        // Get all file using packages
        if (is_file($packages)) {
            $result[] = $packages;
        } if (is_dir($packages)) {
            // Include all file in directory
            $items = scandir($packages);
            $result = [];
            foreach($items as $item) {
                if (is_file("$packages/$item")) {
                    $result[] = "$packages/$item";
                }
            }
        } else {
            // Package with format "name:link"
            $packages = explode(':', $packages);
            if (count($packages) == 2) {
                $packageRoot = \HK\Config::get('packages', $packages[0]);
                $dir = $packages[1];
                if ($packageRoot) {
                    $result = self::getPackagesUrl("$packageRoot/$dir");
                }
            }
        }

        return $result;
    }

    public static function removeFromArray($array, $value) {
        foreach (array_keys($array, $value) as $key) {
            unset($array[$key]);
        }

        return $array;
    }
}