<!DOCTYPE html>
<html>
<head>
    <title>ERROR PAGE</title>
    <style type="text/css">
        .title {
            font-weight: bold;
        }

        .debug-table {
            background: #fdf6e3;
            min-width: 70%;

            margin: 20px auto 20px auto;
            padding: 20px 20px 10px 20px;
        }

        .stack-trace table {
            background: #f7f7f7;
            width: 100%;

            margin: 10px auto 10px auto;
            padding: 20px;
        }
    </style>
</head>
<body>
    <table class="debug-table" align="center">
        <th colspan="2"><span class='header'><?= $header ?></span></th>
        <tr>
            <td><span class="title">Error number</span></td>
            <td><span class="content"><?= $no ?></span></td>
        </tr>
        <tr>
            <td><span class="title">Line</span></td>
            <td><span class="content"><?= $line ?></span></td>
        </tr>
        <tr>
            <td><span class="title">File</span></td>
            <td><span class="content"><?= $file ?></span></td>
        </tr>
        <tr>
            <td><span class="title">Error message</span></td>
            <td><span class="content"><?= $message ?></span></td>
        </tr>
        <?php if (!empty($stackTrace)): ?>
        <th colspan="2"><span class='header'>STACK TRACE</span></th>
        <?php endif ?>
        <?php foreach($stackTrace as $trace): ?>
        <tr>
            <td colspan="2">
                <span class="stack-trace">
                    <table>
                        <?php foreach($trace as $key => $value): ?>
                        <tr>
                            <td><span class="title"><?= ucfirst($key) ?></span></td>
                            <td><span class="content"><?= json_encode($value, JSON_PRETTY_PRINT)?></span></td>
                        </tr>
                        <?php endforeach ?>
                    </table>
                </span>
            </td>
        </tr>
        <?php endforeach ?>
    </table>
</body>
</html>