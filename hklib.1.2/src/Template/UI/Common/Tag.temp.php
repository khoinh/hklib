<<?= $tagName ?> class="<?= implode(' ', $class) ?>" <?php foreach($attrs as $name => $value): ?><?= $name ?>="<?= $value ?>"<?php endforeach ?>>
<?php if($hasClosingTag): ?>
    <?php if(gettype($content) == 'object'): ?>
        <?= $content->render() ?>
    <?php else: ?>
        <?= $content ?>
    <?php endif ?>
</<?= $tagName ?>>
<?php endif ?>