<div class="field-wrapper">
    <div class="field-body">
        <span class="label"><?= $label ?></span>
        <span class="input-wrapper">
            <?php if($input) $input->render() ?>
        </span>
    </div>
    <div class="field-message">
        <?php if($errorMessage): ?>
            <span class="error-message"><?= $errorMessage ?></span>
        <?php endif ?>
    </div>
</div>