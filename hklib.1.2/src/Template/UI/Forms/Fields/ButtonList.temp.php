<div class="button-list">
    <?php foreach($buttons as $button): ?>
    <span class="button-wrapper">
        <?php $button->render() ?>
    </span>
    <?php endforeach ?>
</div>