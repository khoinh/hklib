<!DOCTYPE html>
<html>
<head>
    <title><?= $title ?></title>
    <meta http-equiv="Content-Type" content="<?= $contentType ?>">

    <?php foreach ($styleSheetList as $styleSheet): ?>
    <link rel="stylesheet" href="<?= "$assetRoot$styleSheet?$assetVersion" ?>" type="text/css">
    <?php endforeach ?>

    <?php foreach ($scriptList as $script): ?>
    <script type="text/javascript" src="<?= "$assetRoot$script?$assetVersion" ?>"></script>
    <?php endforeach ?>
</head>
<body>
    <?php if($body) $body->render() ?>
</body>
</html>