<?php
return [
    'packages' => [
        'app' =>        __DIR__.'/..',
        'template' =>   __DIR__.'/../template'
    ],

    'autoload' => [
        'startup' => ['hk:Debug', 'hk:JOD', 'hk:Template']
    ],

    'database' => [
        'handlers' => ['hk:DB/MySQL'],
        'configs' => [
            'TestDB' => [
                'type' =>       'mysql',
                'server' =>     'localhost',
                'username' =>   'khoinh',
                'password' =>   'root',
                'database' =>   'TestDB',
                'port' =>       3306,
                'socket' =>     null
            ]
        ]
    ],

    'jod' => [
        'configs' => [
            'TestJOD' => [
                'database_handler' => 'TestDB'
            ]
        ]
    ],
];