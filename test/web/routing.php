<?php
require_once __DIR__.'/../App/src/HKLoader.php';

// \HK\import('hk:URL/URLDispatcher.php');

// \HK\URL\URLDispatcher::dispatch([
//     "^/(?<module>[a-zA-Z0-9_\.]*)/(?<id>[a-zA-Z0-9_\.]*)" => 'abc'
// ]);

\HK\import('hk:UI/Pages/Common.php');

(new \HK\UI\Pages\Common())
    ->setBody(new \HK\UI\Common\Paragraph("Hello world!", ['class' => 'demo-test']))
    ->render();