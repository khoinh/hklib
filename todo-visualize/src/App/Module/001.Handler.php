<?php
namespace App\Module;

class Handler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    protected $param;
    protected $module;
    protected $action;


    ////////////
    // METHOD
    function __construct($module, $action, $param = []) {
        $this->param = $this->getDefaultParameter();
        $this->module = $module;
        $this->action = $action;
        $this->updateParamter($param);
    }

    protected function getDefaultParameter() {
        return [];
    }

    protected function updateParamter($param) {
        $index = 0;
        foreach ($this->param as $key => $value) {
            if (isset($param[$index]) && trim($param[$index]) != "") {
                $this->param[$key] = $param[$index];
            }

            // Next
            $index++;
        }
    }

    public function get($key, $default = null) {
        if (isset($this->param[$key])) {
            return $this->param[$key];
        }

        return $default;
    }

    public function getModule() {
        return $this->module;
    }

    public function getAction() {
        return $this->action;
    }

    public function execute() {}
}