<?php
namespace App\Module;

class DeleteHandler extends Handler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    protected function getDefaultParameter() {
        return ['id' => 0];
    }

    public function execute() {
        $id = $this->get('id');
        $query = \HK\JOD\QueryBuilder::get('TodoVisualize');
        $result = $query->from($this->module)->delete(['id' => $id]);
        if ($result === false) {
            echo "Failed";
        } else {
            echo "Success";
        }
    }
}