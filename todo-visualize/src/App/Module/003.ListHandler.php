<?php
namespace App\Module;

class ListHandler extends Handler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    protected function getDefaultParameter() {
        return ['pageIndex' => 0, 'numView' => 30];
    }

    public function execute() {
        $pageIndex = $this->get('pageIndex');
        $numView = $this->get('numView');
        $query = \HK\JOD\QueryBuilder::get('TodoVisualize');
        $result = $query->from($this->module)->find();
        var_dump($result);
    }
}