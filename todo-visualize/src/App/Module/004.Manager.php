<?php
namespace App\Module;

\HK\import('app:Module/CRUD');

class Manager {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    function __construct() {}

    public function execute($options) {
        $module = ucfirst($options['module']);
        $action = ucfirst($options['action'])."Handler";
        $param = explode('/', trim($options['param']));

        // Import module
        \HK\import("app:Module/$module");

        // Execute module
        $moduleClass = "\App\Module\\$module\\$action";
        if (class_exists($moduleClass)) {
            (new $moduleClass($module, $action, $param))->execute();
        } else if ($action == 'DeleteHandler' || $action == 'ListHandler') {
            $moduleClass = "\App\Module\\$action";
            (new $moduleClass($module, $action, $param))->execute();
        } else {
            \HK\import(\HK\Context::get('app.default.template.404'));
        }
    }
}