<?php
namespace App\Module\CRUD;

\HK\import('hk:UI');

class Handler extends \App\Module\Handler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    protected function parseData() {
        return [];
    }

    protected function validate($data) {
        return null;
    }

    protected function process($data) {
        return null;
    }

    protected function display($data, $errors) {
    }

    public function execute() {
        $data = $this->parseData();
        $errors = $this->validate($data);
        if (empty($errors)) {
            $errors = $this->process($data);
        }

        $this->display($data, $errors);
    }
}