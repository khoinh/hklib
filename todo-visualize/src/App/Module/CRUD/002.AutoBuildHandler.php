<?php
namespace App\Module\CRUD;

use HK\UI\Common as CommonUI;
use HK\UI\Forms\Fields as FormFields;

class ErrorList {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    //////////////
    // VARIABLE
    private $errors = [];
    private $fieldErrors = [];


    ////////////
    // METHOD
    public function add($message, $fieldKey = null, $fieldMessage = null) {
        $this->errors[] = $message;
        if ($fieldKey && $fieldMessage) {
            $this->fieldErrors[$fieldKey] = $fieldMessage;
        }

        return $this;
    }

    public function total() {
        return count($this->errors);
    }

    public function getFieldError($key) {
        if (isset($this->fieldErrors[$key])) {
            return $this->fieldErrors[$key];
        }

        return null;
    }

    public function getMessages() {
        return $this->errors;
    }
}

class AutoBuildHandler extends Handler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    protected function getModuleDescription() {
        return [];
    }

    protected function parseData() {
        $data = \HK\Http\Request::all()['args'];
        return $data;
    }

    protected function parseDefaultDisplayData() {
        return [];
    }

    protected function getProcessList() {
        return ['Submit'];
    }

    protected function validate($data, $errorList) {
        $description = $this->getModuleDescription();
        foreach ($description as $key => $option) {
            if (!isset($option['validation'])) {
                continue;
            }

            $validateMethod = 'validate'.ucfirst($key);
            if (method_exists($this, $validateMethod)) {
                $this->$validateMethod($data[$key], $errorList);
            } else {
                $label = isset($option['label']) ? $option['label'] : ucfirst($key);
                $option = $option['validation'];
                foreach ($option as $condition => $value) {
                    if (is_int($condition)) {
                        $condition = $value;
                    }

                    if ($condition == 'required' && (!isset($data[$key]) || $data[$key] == null || trim($data[$key]) == "")) {
                        $errorList->add("Empty required field $label",
                            $key, "This field must contain value!");
                    } else if ($condition == 'maxSize' && isset($data[$key]) && strlen($data[$key]) > $value) {
                        $errorList->add("Field $label value exceed maximum size ($value)",
                            $key, "This field have maximum size of $value!");
                    } else if ($condition == 'minSize' && isset($data[$key]) && strlen($data[$key]) < $value) {
                        $errorList->add("Field $label value not exceed minimum size ($value)",
                            $key, "This field have minum size of $value!");
                    } else if ($condition == 'max' && isset($data[$key]) && $value < $data[$key]) {
                        $errorList->add("Field $label value exceed maximum value ($value)",
                            $key, "This field have maximum value of $value!");
                    } else if ($condition == 'min' && isset($data[$key]) && $data[$key] < $value) {
                        $errorList->add("Field $label value not exceed minimum value ($value)",
                            $key, "This field have minum value of $value!");
                    } else if ($condition == 'not-duplicate') {

                    }
                }
            }
        }
    }

    protected function process($data, $errorList) {
        return null;
    }

    protected function buildView($data, $errorList, $result) {
        $view = (new \HK\UI\Pages\Common())
            ->setTitle($this->getAction()." ".$this->getModule())
            ->setBody(new \HK\Template\ComponentList());
        $view->getBody()->add('main-form', $this->buildForm($data, $errorList, $result));
        return $view;
    }

    protected function buildForm($data, $errorList, $result) {
        $form = new \HK\UI\Forms\CommonForm();
        $formBody = $form->getBody();

        if ($result && gettype($result) == 'string') {
            $messageBlock = new CommonUI\Div($result, [], ['form-message']);
            $formBody->add('form-message-block', $messageBlock);
        }

        if ($errorList->total() > 0) {
            $errorMessageList = new \HK\Template\ComponentList();
            $errorMessageBlock = new CommonUI\Div($errorMessageList, [], ['error-message-block']);
            $formBody->add('form-error-message-block', $errorMessageBlock);
            $errorMessageList->add('--', new CommonUI\Div("Total error found: ".$errorList->total(), [], ['summary-error-message']));
            $messages = $errorList->getMessages();
            foreach ($messages as $index => $error) {
                $errorMessageList->add($index, new CommonUI\Div($error, [], ['error-message']));
            }
        }

        $description = $this->getModuleDescription();
        foreach ($description as $key => $option) {
            if (!isset($option['display'])) {
                continue;
            }

            $label = isset($option['label']) ? $option['label'] : ucfirst($key);
            $option = $option['display'];
            $type = $option['type'];
            $field = null;
            switch ($type) {
                case 'text':
                    $field = $formBody->add($key, new FormFields\Text());
                    break;

                case 'choice':
                    $field = $formBody->add($key, new FormFields\Select($option['choices']));
                    break;

                case 'password':
                    $field = $formBody->add($key, new FormFields\Password());

                default:
                    break;
            }

            if ($field && isset($data[$key])) {
                $field->setValue($data[$key]);
            } else if ($field && isset($option['default-value'])) {
                $field->setValue($option['default-value']);
            }

            if ($field) {
                $field->setLabel($label);
                $field->setName($key);
                if ($errorList->total() > 0 && $errorList->getFieldError($key)) {
                    $field->setError($errorList->getFieldError($key));
                }
            }
        }

        // Add submit button
        $formBody->add('submitButtonList', (new FormFields\ButtonList())->setName('submitButtonList')->addButtons($this->getProcessList()));
        return $form;
    }

    public function display($view) {
        $view->render();
    }

    public function execute() {
        // Get data
        $data = $this->parseData();
        $result = null;
        $errors = new ErrorList();

        // Validate and process data
        if (!empty($data)) {
            $this->validate($data, $errors);
            if ($errors->total() <= 0) {
                $result = $this->process($data, $errors);
            }
        }

        // Build and render view
        if (empty($data)) {
            $data = $this->parseDefaultDisplayData();
        }

        $view = $this->buildView($data, $errors, $result);
        $this->display($view);
    }
}