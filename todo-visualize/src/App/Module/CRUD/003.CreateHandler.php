<?php
namespace App\Module\CRUD;

class CreateHandler extends AutoBuildHandler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    protected function getProcessList() {
        return ['Create'];
    }

    protected function process($data, &$errorList) {
        $submittedProcess = isset($data['submitButtonList']) ? $data['submitButtonList'] : [];

        try {
            if (in_array('Create', $submittedProcess)) {
                $query = \HK\JOD\QueryBuilder::get('TodoVisualize');
                $description = $this->getModuleDescription();
                $insertData = [];
                foreach($description as $key => $des) {
                    if (isset($data[$key])) {
                        $insertData[$key] = $data[$key];
                    }
                }

                $result = $query->from($this->getModule())->insert($insertData);
                if ($result === false) {
                    $errorList->add("Unable to create new data!");
                } else {
                    return "Success";
                }
            }
        } catch (\Exception $e) {
            $errorList->add($e->getMessage());
        }

        return null;
    }
}