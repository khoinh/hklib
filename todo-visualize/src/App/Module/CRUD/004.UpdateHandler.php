<?php
namespace App\Module\CRUD;

class UpdateHandler extends AutoBuildHandler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    protected function getDefaultParameter() {
        return ['id' => 0];
    }

    protected function parseDefaultDisplayData() {
        if (!$this->get('id')) {
            return [];
        } else {
            $query = \HK\JOD\QueryBuilder::get('TodoVisualize');
            $result = $query->from($this->getModule())->first(['id' => $this->get('id')]);
            if ($result === false) {
                $errorList->add("Unable to create new data!");
            } else {
                return $result;
            }
        }

        return [];
    }

    protected function getProcessList() {
        return ['Save'];
    }

    protected function process($data, &$errorList) {
        $submittedProcess = isset($data['submitButtonList']) ? $data['submitButtonList'] : [];

        try {
            if (!$this->get('id')) {
                $errorList->add("Empty entry id!");
            } else if (in_array('Save', $submittedProcess)) {
                $query = \HK\JOD\QueryBuilder::get('TodoVisualize');
                $description = $this->getModuleDescription();
                $updateData = [];
                foreach($description as $key => $des) {
                    if (isset($data[$key])) {
                        $updateData[$key] = $data[$key];
                    }
                }

                $result = $query->from($this->getModule())->update(['id' => $this->get('id')], $updateData);
                if ($result === false) {
                    $errorList->add("Unable to create new data!");
                } else {
                    return "Success";
                }
            }
        } catch (\Exception $e) {
            $errorList->add($e->getMessage());
        }

        return null;
    }
}