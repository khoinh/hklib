<?php
namespace App\Module\User;

class DeleteHandler extends \App\Module\DeleteHandler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    public function execute() {
        try {
            $id = $this->get('id');
            $query = \HK\JOD\QueryBuilder::get('TodoVisualize');
            $result = $query->from($this->module)->update(['id' => $id], ['is_deleted' => 1]);
            if ($result === false) {
                echo "Failed";
            } else {
                echo "Success";
            }
        } catch (\Exception $e) {
            echo "Failed";
        }
    }
}