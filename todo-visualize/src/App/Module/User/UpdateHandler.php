<?php
namespace App\Module\User;

class UpdateHandler extends \App\Module\CRUD\UpdateHandler {
    // MEMBER ----------------------------------------------------------------------------------------------------------------------------------------

    ////////////
    // METHOD
    protected function getModuleDescription() {
        return [
            'username' => [
                'display' => [
                    'type' => 'text',
                    'maxSize' => 255
                ],
                'validation' => [
                    'required',
                    'notDuplicate',
                ]
            ],

            'password' => [
                'display' => [
                    'type' => 'text',
                    'maxSize => 255'
                ],
                'validation' => [
                    'required',
                    'maxSize' => 255,
                    'minSize' => 5
                ]
            ],

            'role' => [
                'display' => [
                    'type' => 'choice',
                    'choices' => [1 => 'Admin', 2 => 'Guest']
                ]
            ]
        ];
    }
}