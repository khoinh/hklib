<?php
return [
    'packages' => [
        'app' =>        __DIR__.'/../App',
        'template' =>   __DIR__.'/../Template'
    ],

    'autoload' => [
        'startup' => ['hk:Debug', 'hk:JOD', 'hk:Template']
    ],

    'database' => [
        'handlers' => ['hk:DB/MySQL'],
        'configs' => [
            'TodoVisualize' => [
                'type' =>       'mysql',
                'server' =>     'localhost',
                'username' =>   'khoinh',
                'password' =>   'root',
                'database' =>   'TodoVisualize',
                'port' =>       3306,
                'socket' =>     null
            ]
        ]
    ],

    'jod' => [
        'configs' => [
            'TodoVisualize' => [
                'database_handler' => 'TodoVisualize'
            ]
        ]
    ],
];