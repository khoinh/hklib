<?php
require_once __DIR__.'/../src/App/HKLoader.php';

\HK\import('hk:URL/URLDispatcher.php');
\HK\import('app:Module');

\HK\Context::set('app.default.template.404', 'template:Common/404.temp.php');

// URL Dispatcher
\HK\URL\URLDispatcher::dispatch([
    "^/(?<module>[a-zA-Z0-9_\.]*)/(?<action>[a-zA-Z0-9_\.]*)(/(?<param>[a-zA-Z0-9_\./]*))?" => [new \App\Module\Manager(), 'execute'],
    '.*' => \HK\Context::get('app.default.template.404')
]);